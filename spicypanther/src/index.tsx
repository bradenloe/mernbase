import React from 'react';
import ReactDOM from 'react-dom';


import reportWebVitals from './reportWebVitals';

import { Web3 } from 'web3react'
import { UI , useUser } from 'ui'
import { Apollo, useQuery, useApollo } from 'apollohooks'
import { getCookie} from 'localstorage'


import { Layout } from './pages/theme'
import { Modal } from './pages/theme'
import { Router } from './router'

import { ThemeLight, ThemeDark } from './theme'

//////////////////////////////////////////////
const themes = { light: ThemeLight, dark: ThemeDark }

//////////////////////////////////////////////
const schema = require ('./schema.json')
const server = {
  http: 'https://ql.darkmoon.directblu.com/ql',
  ws: 'wss://ql.darkmoon.directblu.com/ws'
}

//////////////////////////////////////////////
//const networks = ['https://polygon-rpc.com']    
const networks = ['https://data-seed-prebsc-1-s1.binance.org:8545']

const AuthUserProvider = (params: any)=>{
  const { children } = params;
  const { login, userQuery} = useUser()
  const { loading, error, data: userData} = useQuery ('customer', { request : userQuery, variables: { } }  )

  React.useEffect (()=>{
    if (userData && userData._id){
      login (userData)
    }

  },[userData])
  if (loading)
    return <React.Fragment/>
  return (<React.Fragment>{children}</React.Fragment> )
}


const AuthCookieProvider = (params: any) =>{
  const {children} = params
  const { login: loginApollo } = useApollo()
  
  const [ {auth, loading}, setState] = React.useState ( {
    auth: null,    
    loading : true,    
  }  as any)

  const { user } = useUser()

  React.useEffect (()=>{

    const authkey = getCookie ('auth')

    if (authkey && user ==null ){
      loginApollo (authkey)
      setState ({
        auth : authkey,
        loading: false,        
      })
    }

    if ( authkey == null || user !=null){
      setState ({
        auth : null,
        loading: false        
      })
    }
      
  },[])

  if ( loading == true)
    return (<React.Fragment/>)

  if ( auth == null ){
    return (<React.Fragment>{children}</React.Fragment>)    
  }

  return (
    <AuthUserProvider>
      <React.Fragment>{children}</React.Fragment> 
    </AuthUserProvider>
  ) 
  
  
}

const AuthRouterProvider = (params: any)=>{
  const {children: Children} = params
  const {user, login, userQuery} = useUser()

    return (
      <React.Fragment>
        <AuthCookieProvider>
          <Router auth={user} layout={Layout} />
        </AuthCookieProvider>
      </React.Fragment>
    )  
}

//////////////////////////////////////////////
ReactDOM.render(
  <React.StrictMode>
    <Web3 networks={networks}>
      <UI themes = { themes } theme='dark' language='en' alertTimeout={2} userQuery={'_id auth'} modal= {Modal}>
        <Apollo server= {server} schema={schema}>     
            <AuthRouterProvider/>            
        </Apollo>      
      </UI>
    </Web3>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
//reportWebVitals();
