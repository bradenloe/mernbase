var lineReader = require('readline').createInterface({
  input: require('fs').createReadStream('schema.txt')
});

const lookingnone = null
const lookingquery = 'queries'
const lookingmutation = 'mutations'
const lookingsubscription = 'subscriptions'

let lookingFor= lookingnone
const schema = {    
}

const main =async () =>{
    lineReader.on('line', (line) =>{
        
        const querymatches = line.match(/\s*type\s*Query/)
        
        if (querymatches){
            lookingFor = lookingquery        
        }
        
        const mutationMatches = line.match(/\s*type\s*Mutation/g)
        if (mutationMatches)
            lookingFor = lookingmutation
        
        const subscriptionmatches = line.match(/\s*type\s*Subscription/g)
        if (subscriptionmatches)
            lookingFor = lookingsubscription    

        const endmatches = line.match(/\}/g)
            if (endmatches)
                lookingFor = lookingnone

        
        if (!lookingFor)
            return
                
                
        const functionmatches =  line.match(/\s*(.*)\s*\((.*)\):/)                

        if (functionmatches) {  //found name( params:type) style
            const name=functionmatches[1]
            const params=functionmatches[2]
            if (schema[lookingFor] == null)
                schema[lookingFor] = {}

            if (schema[lookingFor][name] == null)
                schema[lookingFor][name] = { parameters : {}}

            const func = schema[lookingFor][name]            
            
            const paramsArray = params.split(',')
            paramsArray.map ((value)=>{
                
                const varmatch = value.match(/\s*(.*)\s*:\s*(.*)\s*/)
                if (varmatch){
                    const varname = varmatch[1]
                    const vartype = varmatch[2]                    
                    func.parameters[varname]=vartype
                }
            })        
        }

        if (!functionmatches){
            const functionnakedmatches =  line.match(/\s*(.*)\s*:/)
            if (functionnakedmatches){                
                const name = functionnakedmatches[1]

                if (schema[lookingFor] == null)
                schema[lookingFor] = {}

                if (schema[lookingFor][name] == null)
                schema[lookingFor][name] = { parameters : {}}
            }
        }        
        
        
        
    })

    lineReader.on ('close',()=>{        
        require('fs').writeFileSync('src/schema.json', JSON.stringify(schema))        
    })
}

main ()
