import * as React from 'react'
import { useIFrame, HtmlView} from 'ui'
import { Avatar, Button, Paper, Box, Typography, Modal} from '@material-ui/core'
import { useQuery, useSubscription } from 'apollohooks'
import { useStateArray } from 'usestateobject'
import { hashmap } from 'utils'

import {CreatePost} from './createPost'

const nullfunction = (params:any) =>{
    return <React.Fragment/>
}


const ChatBubbleOutlineIcon = nullfunction
const AddIcon = nullfunction

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const sortPosts = (postarray: any) => {
	return postarray
	const postsSorted = {} as any

	postarray.map((p: any) => {
		if (p.parentEventPost == null) {
			postsSorted[p._id] = p
			postsSorted[p._id].children = []
			return
		}
		if (postsSorted[p.parentEventPost])
			postsSorted[p.parentEventPost].children.push(p)
		else console.log(`failed ${p}`)
		console.log(p)
	})
	return postsSorted
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


export const MediaPosts = (params: any) => {	
    const [{event, guest, user}] = useIFrame({event: {_id: null}, guest: null, user: null})

	const [createModal, e] = React.useState(false)
	const displayCreate = ()=>{e(true)}
	const hideCreate = ()=>{e(false)}

	const {data: result, loading} = useQuery('eventPublic', {
		variables: {_id: event._id},
		$: ` childrenEventPosts{_id name message _createdTime parentEventPost authorName authorID}`,
	})

	const {data: subscription } = useSubscription ('subscribeEventPosts',
	{
		variables : {event: event._id},
		$: '_action _id message _createdTime parentEventPost authorName authorID',
	})

	const [eventPostsArray, eventPostsArraySet] = useStateArray(
		(result && result.childrenEventPosts) || [],
		[result]
	)
	const [postText, postSet] = React.useState('')
	const [selected, selectedSet] = React.useState(-1)
	
	React.useEffect(() => {		
			if (subscription == null) return null

			const action = subscription._action
			if ('create'.localeCompare(action) == 0) {
				return eventPostsArraySet([...eventPostsArray, subscription])
			}

			if ('delete'.localeCompare(action) == 0) {
				const updates = eventPostsArray.filter((item: any) => {
					return item._id.localeCompare(subscription._id) != 0
				})
				return eventPostsArraySet(updates)
			}

			if ('update'.localeCompare(action) == 0) {
				const updates = eventPostsArray.map((item: any) => {
					return item._id.localeCompare(subscription._id) == 0 ? subscription : item
				})
				return eventPostsArraySet(updates)
			}
		}, [subscription])

	const addComments = (children: any, post: any) => {

/*		displayPopup(CreatePost, {
			event: event,
			comments: children,
			post: post,
			guest: guest,
			refetch: () => {},
		})*/
	}
	const create = () => {
		displayCreate ()
		//displayPopup(CreatePost, {event: event, guest: guest, refetch: () => {}})
	}
	if (loading) return <React.Fragment />

    if (result && result.lenght == 0)
        return <div>nope</div>

	const sorted = eventPostsArray

	return (
		<>
			<Modal open={createModal}><CreatePost event={event} guest={guest} close={hideCreate}/></Modal>
			<Paper elevation={2}>
				<Box
					display='flex'
					flexDirection='row-reverse'
					alignItems='center'
					justifyContent='space-between'
					px={2}
					p={1}>
					{user && <Button
						size='small'
						startIcon={<AddIcon color='primary' />}
						onClick={create}>
						Create New Post
					</Button>}
					{guest && (
						<Box display='flex' flexDirection='row'>
							<Avatar
								
								alt={guest.displayName}
								src={
									guest.image
										? guest.image
										: 'https://stock.adobe.com/sk/search/images?k=default+profile+picture&asset_id=391192211'
								}
							/>
							<Typography variant='h6' align='left' color='textPrimary'>
								&nbsp;&nbsp;{guest.displayName}
							</Typography>
						</Box>
					)}
				</Box>
			</Paper>

			{sorted.map( (post: any, index: any) => {
				const update = (action: string) => {
					/*selectedSet(-1)
					if (action == 'save')
						mutation(
							'updateEventPost',
							{_id: post._id, updates: {message: postText}, $: '_id'},
							() => {}
						)*/
				}

				const onclickedit = () => {
					selectedSet(index)
					postSet(post.message)
				}

				const remove = () => {
			//		mutation('deleteEventPost', {_id: post._id}, () => {})
				}

				const children = post.children || []

				console.log (post.message)

				return (
					<Box key={index} mt={1}>
						<Paper>
							<Box>
								<Box
									display='flex'
									flexDirection='row'
									justifyContent='space-between'
									alignItems='center'
									p={2}
									pl={2}>
									<Box display='flex' flexDirection='row' alignItems='center'>
										<Avatar											
											alt={post.authorName}
											src={
												post.authorImage
													? post.authorImage
													: 'https://stock.adobe.com/sk/search/images?k=default+profile+picture&asset_id=391192211'
											}
										/>

										<Typography variant='h6' align='left' color='textPrimary'>
											&nbsp;&nbsp;{post.authorName}
										</Typography>
									</Box>
									<Box display='flex' flexDirection='row' alignItems='center'>
										<Typography variant='h3' align='left' color='secondary'>
											&nbsp;&nbsp;&nbsp;created@
											
										</Typography>
									</Box>
								</Box>

								<Box
									display='flex'
									justifyContent='center'
									flexDirection='column'
									p={1}
									pl={2}>
									<Typography variant='h5' align='left' color='initial'>
										{post.name}
									</Typography>
									<HtmlView value={post.message} />
								</Box>
							</Box>
							<Box justifyContent='center' pl={2}>
								<Button
									color='secondary'
									onClick={() => addComments(children, post)}
									startIcon={<ChatBubbleOutlineIcon />}>
									{children.length}&nbsp;Comments
								</Button>
							</Box>

							{children.map((post: any, key: any) => {
									return (
										<Box
											display='flex'
											flexDirection='row'
											justifyContent='left'
											alignItems='center'
											key={key}>
											<Typography variant='h6' color='inherit'>
												&nbsp; &nbsp;@{post.authorName}: &nbsp;												
											</Typography>
											<HtmlView value={post.message} />
											
											<Button aria-label='delete' onClick={remove}>
												<Typography variant='h3' align='right' color='error'>
													Remove
												</Typography>
											</Button>
										</Box>
									)
								})} 
							{/* <CreatePost event={event} post={post} refetch={() => {}} /> */}
						</Paper>
					</Box>
				)
			})}
		</>
	)
}
