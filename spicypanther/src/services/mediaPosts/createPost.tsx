import * as React from 'react'
import { useIFrame, HtmlView } from 'ui'
import { Avatar, Button, Paper, Box, Typography} from '@material-ui/core'
import { useQuery, useSubscription, useMutation } from 'apollohooks'
import { useStateHash } from 'usestateobject'
import { hashmap } from 'utils'

import RichTextEditor from 'react-rte'

export const CreatePost = (params: any) => {
	const {event, comments, guest, post, close} = params
	
	const [text, textSet] = React.useState(RichTextEditor.createEmptyValue())
    const [createEventPost ] = useMutation ('createEventPost', {$: '_id',})

	const {register, changes: postChanges} = useStateHash(
		{
			name: '',
		},
		[]
	)
	const postID = post ? post._id : null
	const guestID = guest ? guest._id : null
	console.log(postID)
	console.log(guestID)

	const add = () => {
		const finish = () => {
			//refetch()
			//dismissPopup()
		}
		const eventpost = postChanges()

		textSet(RichTextEditor.createEmptyValue())

		const postID = post ? post._id : null
		const guestID = guest ? guest._id : null
		console.log(postID)

        createEventPost ( {
            variables: { 
                event: event._id,
                guest: guestID,
                parentPost: postID,
                create: {
                    name: eventpost.name, message: text.toString('html')
                } 
            } 
        })

	
	}

	return (
		<Box p={2} display='flex' flexDirection='column'>
			{post ? (
				<>
					<Typography variant='h5' align='center'>
						Post
					</Typography>
					<Box>
						<Box
							display='flex'
							flexDirection='row'
							justifyContent='space-between'
							alignItems='center'
							p={2}
							pl={2}>
							<Box display='flex' flexDirection='row' alignItems='center'>
								<Avatar
									
									alt={post.authorName}
									src={
										post.authorImage
											? post.authorImage
											: 'https://stock.adobe.com/sk/search/images?k=default+profile+picture&asset_id=391192211'
									}
								/>

								<Typography variant='h6' align='left' color='textPrimary'>
									&nbsp;&nbsp;{post.authorName}
								</Typography>
							</Box>
							<Box display='flex' flexDirection='row' alignItems='center'>
								<Typography variant='h3' align='left' color='secondary'>
									&nbsp;&nbsp;&nbsp;created@
									
								</Typography>
							</Box>
						</Box>

						<Box
							display='flex'
							justifyContent='center'
							flexDirection='column'
							p={1}
							pl={2}>
							<Typography variant='h5' align='left' color='initial'>
								{post.name}
							</Typography>
							<HtmlView value={post.message} />
						</Box>
					</Box>
					<RichTextEditor
						value={text}
						onChange={textSet}
						placeholder='Add Comment here'
					/>
					<Typography variant='h6' color='textSecondary'>
						Comments
					</Typography>
					<Box
						minHeight='35vh'
						maxHeight='35vh'
						display='flex'
						overflow='auto'
						flexDirection='column'>
						{comments &&
							comments.map((post: any, key: any) => {
								return (
									<Box
										display='flex'
										alignItems='center'
										flexDirection='row'
										key={key}>
										<Typography variant='h6' color='initial'>
											&nbsp; &nbsp;@{post.authorName}: &nbsp;
										</Typography>
										<HtmlView value={post.message} />
									</Box>
								)
							})}
					</Box>
				</>
			) : (
				<>
					<Typography variant='h5' align='center'>
						Create Post
					</Typography>
					<Box
						minHeight='65vh'
						maxHeight='65vh'
						pt={5}
						display='flex'
						flexDirection='column'
						alignItems='center'>
						<Box m={1}>
							
						</Box>
						<RichTextEditor
							value={text}
							onChange={textSet}
							placeholder='say something about event'
						/>
					</Box>
				</>
			)}

			<Box
				display='flex'
				flexDirection='row'
				alignItems='center'
				justifyContent='center'
				pb={2}
				m={2}>
				<Box mx={1}>
					<Button
						data-testid='change-password-button'
						variant='contained'
						color='primary'
						onClick={add}>
						{post ? 'Add Comment' : 'Post'}
					</Button>
				</Box>
				<Box mx={1}>
					<Button
						data-testid='email-templates-button'
						variant='contained'
						color='default'
						onClick={close}>
						Cancel
					</Button>
				</Box>
			</Box>
		</Box>
	)
}
