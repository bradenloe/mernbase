import * as React from 'react'
import { BrowserRouter as ReactRouter, Routes, Route } from 'react-router-dom';

import { Home } from './pages/home';
import { Header } from './pages/theme';

import { CreateEvent, EventEdit, EventList} from './pages/eventDashboard'
import { ViewEvent} from './pages/externalViewEvent'
import { ViewCustomer } from './pages/externalViewCustomer'

import { BlackJack, MediaBlog, MediaFollowing,MediaItem, MediaList } from './pages/games';
import { Profile } from './pages/user'
//import { Home as HomeDes, Header as HeaderDes } from './pages/shrineofdes'

import { MediaPosts } from './services'

import { Auth } from './pages/auth';
import { Box } from '@material-ui/core';

const Fragment = (params: any)=> {return (<React.Fragment/>)}

const Pages = [
  { path: '/home', header: Header, body: Home },
  { path: '/blackjack', header: Header, body: BlackJack },
  { path: '/mediapost', header: Fragment, body: MediaPosts },
  { path: '/mediablog/:blog', header: Header, body: MediaBlog },
  { path: '/mediafollowing', header: Header, body: MediaFollowing },
  { path: '/medialist', header: Header, body: MediaList },
  { path: '/mediaitem/:itemID', header: Header, body: MediaItem },
  { path: '/createevent', header: Header, body: CreateEvent},
  { path: '/eventedit/:eventID', header: Header, body: EventEdit},
  { path: '/event/:eventID', header: Header, body: ViewEvent},
  { path: '/customer/:_id', header: Header, body: ViewCustomer},

  //default routes **************************************************
  { path: '/login', header: Header, body: Auth.Login },
  { path: '/register', header: Header, body: Auth.Register },
  { path: '/dashboard', header: Header, body: EventList },

  { path: '/profile', header: Header, body: Profile} ,

  { path: '/', header: Header, body: Home, home: true, exact: true },
  { path: '*', header: Header, body: Home, home: true },
];




export const Router = (params: any) => {
  const { auth, layout: Layout } = params;

  const AuthRoute = (params: any) => {
    const { route, element } = params;    
    const header = route.header || Fragment

    let page = route.body;
    

    if (auth != null){      
      if (route.admin && auth.admin !=true)
        page = Fragment
    }

    if (auth == null) {
      if (route.auth || route.admin)
        page = Fragment
    }    

    return (<Layout header={header} body={page} />)
  };

  return (
    <Box>
      <ReactRouter>
        <Routes>
          {Pages.map((route: any, index: any) => {
            return (
              <Route
                key={index}
                path={route.path}
                element={<AuthRoute route={route} />}
              />
            );
          })}
        </Routes>
      </ReactRouter>
    </Box>
  );
};
