import * as React from 'react'
import {useState, Fragment, useEffect} from 'react'
import { useMutation} from 'apollohooks'
import {
	Box,
	Paper,
	Typography,
	Button,
	IconButton,	
	makeStyles,
} from '@material-ui/core'


import { Link, useNavigate } from 'react-router-dom'
import {useAlert, LabeledInput} from 'ui'
import {useStateHash} from 'usestateobject'


const ImageUploader = ()=> {return <React.Fragment/>}
const GoogleMapsAutoCompleteInput= ()=> {return <React.Fragment/>}
const CloseIcon = ()=> {return <React.Fragment/>}


const eventValidateMask = {
	name: {field: 'Name', validate: 'string minlength:3 maxlength:32'},
} as any

const useStyles = makeStyles(() => ({
	root: {
		outline: 'none',
	},
}))

export const CreateEvent = (params: any) => {
	//const dismissPopup = useUI((store) => store.dismissPopup)
	//const displayPopup = useUI((store) => store.displayPopup)
	
	const {alert: displayNotification} = useAlert()		
	const classes = useStyles()
	
	const navigate = useNavigate()

	const now = new Date().toISOString().slice(0, 16)
	const {register, changes: eventChanges} = useStateHash(
		{
			name: '',
			description: '',
			image: '',
			startTime: now,
			endTime: now,
			notes: '',
		},
		[]
	)

	const [createEvent] = useMutation ('createEvent',{ $: '_id'})

	const save = () => {
		const event = eventChanges()
		//if (displayNotification(validate(event, eventValidateMask))) return

		createEvent( { variables: {create: event, $: '_id'}, onCompleted : (data: any) => {
			navigate(`/event/${data._id}`)
		}} )
			
			


//		dismissPopup()
//		displayNotification('valid')*/
	}

	return (
		<Box>
			<Typography variant='h4' align='center'>
				Create Event
			</Typography>

			<Box
				display='flex'
				flexDirection='row'
				p={2}
				alignItems='center'
				justifyContent='center'>
				<Box mr={2}>
					<ImageUploader
						{...register('image')}
						label='Event Cover'></ImageUploader>
				</Box>
				<Box>
					<Box mt={1}>
						<LabeledInput {...register('name')} label='Event Name*' />
					</Box>
					<Box mt={1}>
						<GoogleMapsAutoCompleteInput {...register('address')} label='Event Address' />
					</Box>
					<Box mt={1}>
						<LabeledInput {...register('description')} label='Description*' />
					</Box>
					<Box mt={1}>
						<LabeledInput
							{...register('startTime')}
							type='datetime-local'
							label='Event Starts*'
						/>
					</Box>
					<Box mt={1}>
						<LabeledInput
							{...register('endTime')}
							type='datetime-local'
							label='Event Ends*'
						/>
					</Box>
					
				</Box>
			</Box>

			<Box
				display='flex'
				flexDirection='row'
				alignItems='center'
				justifyContent='center'
				pb={2}>
				<Box mx={1}>
					<Button
						data-testid='change-password-button'
						variant='contained'
						color='primary'
						onClick={save}>
						Create Event
					</Button>
				</Box>
				<Box mx={1}>
					<Button
						data-testid='email-templates-button'
						variant='contained'
						color='default'
						onClick={()=>{}}>
						Cancel
					</Button>
				</Box>
			</Box>
		</Box>
	)
}
