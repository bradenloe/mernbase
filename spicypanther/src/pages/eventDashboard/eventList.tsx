import * as React from 'react'
import {useState, Fragment, useEffect} from 'react'


import { useAlert} from 'ui'
import { Link } from 'react-router-dom'
import { useQuery } from 'apollohooks'
/*import {muiFormatDate} from '../../lib/utils'
import {useUI} from '../../lib/hook'
import {useQuery} from '../../lib/ql'
import {CreateEvent} from '../popups/createEvent'*/




import {
	Typography,
	Grid,
	Button,
	makeStyles,
	Paper,
	Box,
	Card,
	CardActionArea,
	CardActions,
	CardContent,
	CardMedia,
} from '@material-ui/core'

const CreateEvent = ()=>{ return <React.Fragment></React.Fragment>}
const DateTimeInput = ()=>{ return <React.Fragment></React.Fragment>}


const useStyles = makeStyles({
	root: {
		maxWidth: 380,
	},
	container: {
		display: 'flex',
		maxWidth: '100vw',
		alignItems: 'center',
		marginTop: '1rem',
		marginLeft: '1rem',
	},
})

const EmptyList = () => {  	
  
	return (
	  <Box>
		<Typography>
		  You have no events.              
		</Typography>
	  </Box>
	)
  }

export const EventList = () => {	
	const classes = useStyles()
	const {data: user} = useQuery('customer', {
		$: '_id displayName childrenEvents{ _id name image description startTime endTime}',
	})

	if (!user) return <React.Fragment />

	if (user.childrenEvents.length == 0) {		
		return <EmptyList />
	}

	return (
		<>
			
			<Grid className={classes.container} container spacing={1}>
				{user.childrenEvents.map((event: any, index: number) => {
					return (
						<Grid item xs={12} sm={4}>
							<Card className={classes.root}>
								<CardActionArea>
									<CardMedia
										component='img'
										alt={event.name}
										height='140'
										image={event.image}
										title={event.name}
									/>
									<CardContent>
										<Typography gutterBottom variant='h5' component='h2'>
											{event.name}
										</Typography>
										<Typography
											variant='body2'
											color='textSecondary'
											component='p'>
											{event.description}
										</Typography>
										<Typography
											variant='body2'
											color='textSecondary'
											component='p'>
											Starts: {''}
										</Typography>
										<Typography
											variant='body2'
											color='textSecondary'
											component='p'>
											Ends: {''}
										</Typography>
									</CardContent>
								</CardActionArea>
								<CardActions>
									<Link key={index} to={`/eventedit/${event._id}`}>
										<Button size='small' color='primary'>
											View Event
										</Button>
									</Link>
								</CardActions>
							</Card>
						</Grid>
					)
				})}
			</Grid>
		</>
	)
}
