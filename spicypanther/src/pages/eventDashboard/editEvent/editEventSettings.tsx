import * as React from 'react'
import {createTheme} from '@material-ui/core/styles'

import {Box, Typography, Button} from '@material-ui/core'

import ColorPicker from 'material-ui-color-picker'
import Switch from '@material-ui/core/Switch'

//import {EditEventPopup} from './popups/editEvent'


//import {Spinner, Image, HtmlEditor, PopupYesNo, Link} from '../../lib/ui'


//import {SelectTemplate} from './settings/selectTemplate'
//import {ManageEmailTemplates} from '../popups/manageEmailTemplates'
//import {useHistory} from 'react-router-dom'

import {ImageUploader, LabeledInput} from 'ui'
//import {useUI} from '../../lib/hook'
//import {useTheme} from '../../lib/changeThemeHook'

//import {mutation} from '../../lib/ql'
import {useStateHash} from 'usestateobject'

//import {GoogleMapsAutoCompleteInput} from '../../lib/mui/googleMapsAutoCompleteInput'

const GoogleMapsAutoCompleteInput = (params:any)=>{
	return <React.Fragment/>
}

const Link = GoogleMapsAutoCompleteInput 


const parse = (value: string) =>{	
	try {
		const hash = JSON.parse (value)
		return hash
	}
	catch(e: any) {
		return null
	}	
}



export const Settings = (params: any) => {	
	const {event, onChange} = params
	

	const {theme } = event || {} 
	const { pallettePrimaryMain } =parse (theme) || {"pallettePrimaryMain":"#5F2EEA"}
	
	
	const {register, state: item, update, changes} = useStateHash({
		...event,
		pallettePrimaryMain: pallettePrimaryMain
	}, [event])

	const onsave = () => {
		/*const {pallettePrimaryMain, ...updates} = changes()

		if (pallettePrimaryMain){
			updates.theme= JSON.stringify ({
				pallettePrimaryMain: pallettePrimaryMain
			})
		}		

		if (updates) mutation('updateEvent', {_id: event._id, updates: updates})
		dismissPopup()*/
	}

	const tooglePublic = () => {
		/*8mutation('updateEvent', {
			_id: event._id,
			updates: {restricted: !item.restricted},
		})
		update('restricted')(!item.restricted)
		onsave()*/
	}

	const tooglePublished = () => {
		/*mutation('updateEvent', {
			_id: event._id,
			updates: {published: !item.published},
		})
		update('published')(!item.published)
		onsave()*/
	}

	const tooglePrivacyPolicy = () => {
		/*mutation('updateEvent', {
			_id: event._id,
			updates: {privacyPolicyRequired: !item.privacyPolicyRequired},
		})
		update('privacyPolicyRequired')(!item.privacyPolicyRequired)
		onsave()*/
	}

	const manageEmailTemplates = () => {
		//displayPopup(ManageEmailTemplates)
	}

	const deleteEvent = () => {
		/*const remove = () => {
			mutation('deleteEvent', {
				_id: event._id,
			})
			history.replace('/')
		}

		displayPopup(PopupYesNo, {title: 'Are you sure', onClick: remove})*/
	}
	const editRSVP = () => {
		/*displayPopup(SelectTemplate, {
			value : register('emailRSVP').value,
			onChange : (v:any)=>{ register('emailRSVP').onChange(v) ; onsave()},
			onCancel: dismissPopup,
		})*/
	}

	const editPrivacy = () => {
		/*displayPopup(SelectTemplate, {			
			value : register('privacyPolicy').value,
			onChange : (v:any)=>{ register('privacyPolicy').onChange(v) ; onsave()},
			onSave: onsave,
			onCancel: dismissPopup,
		})*/
	}

	const editRSVPDecline = () => {
		/*displayPopup(SelectTemplate, {			
			value : register('emailRSVPDecline').value,
			onChange : (v:any)=>{ register('emailRSVPDecline').onChange(v) ; onsave()},
			onSave: onsave,
			onCancel: dismissPopup,
		})*/
	}

	const editTicket = () => {
		/*const mergeTags = {
			qrCode: '{!qrCode!}',
		}

		displayPopup(SelectTemplate, {			
			value : register('emailTicket').value,
			onChange : (v:any)=>{ register('emailTicket').onChange(v) ; onsave()},
			
			onCancel: dismissPopup,
		})*/
	}

	const editEvent = () => {
		//displayPopup(EditEventPopup, params)
	}

	const idk = (layout: number) =>{
		/*const updates = {
			layout : JSON.stringify ({
				externalEvent : layout
			})
		}
		mutation('updateEvent', {_id: event._id, updates: updates})		*/
	}

	const one = ()=>{idk(1)}
	const two = ()=>{idk(2)}

	if (!event) return <React.Fragment/>

	return (
		<React.Fragment>
			<Typography variant='h4' align='center'>
				Settings
			</Typography>
			<Box
				display='flex'
				flexDirection='row'
				p={2}
				alignItems='center'
				justifyContent='center'>
				<Box>
					<Box mt={1}>
						<LabeledInput {...register('name')} label='Event Name*' />
					</Box>
					<Box mt={1}>
						<GoogleMapsAutoCompleteInput {...register('address')} label='Event Address' />
					</Box>
					<Box mt={1}>
						<LabeledInput {...register('description')} label='Description*' />
					</Box>
					<Box mt={1}>
						<LabeledInput
							type='datetime-local'
							{...register('startTime')}
							label='Event Starts*'
						/>
					</Box>
					<Box mt={1}>
						<LabeledInput
							type='datetime-local'
							id='endTime'
							{...register('endTime')}
							label='Event Ends*'
						/>
					</Box>
					
				</Box>
				<Box ml={2}>
					{/*<ImageUploader
						{...register('image')}
					label='Event Cover'></ImageUploader>*/}
					<Box mt={1}>
						<ColorPicker
							defaultValue='Event Primary Theme Color'
							{...register('pallettePrimaryMain')}
						/>
					</Box>
					<Box mt={1}>
						{item.restricted ? 'Invitation Only' : 'Tickets Sold Online'}
						<Switch checked={item.restricted} onClick={tooglePublic} />
					</Box>
					<Box mt={1}>
						{item.published ? 'Event Is Published' : 'Event Is Hidden'}
						<Switch checked={item.published} onClick={tooglePublished} />
					</Box>

					<Box mt={1}>
						{item.privacyPolicyRequired
							? 'Privacy Policy is Required'
							: 'Privacy Policy is Not Required'}
						<Switch
							checked={item.privacyPolicyRequired}
							onClick={tooglePrivacyPolicy}
						/>
					</Box>

					<Box mt={1}>
						<React.Fragment>
								<Button onClick= {one}>Layout One</Button>
								<Button onClick={two}>Layout Two</Button>
						</React.Fragment>
					</Box>


				</Box>
			</Box>

			<Box
				display='flex'
				flexDirection='row'
				alignItems='center'
				justifyContent='center'
				pb={2}>
				<Box mx={1}>
					<Button
						data-testid='change-password-button'
						variant='contained'
						color='primary'
						onClick={onsave}>
						Update Event
					</Button>
				</Box>
				<Box mx={1}>
					<Link to={`/view/${event._id}`}>
						<Button
							data-testid='view-event-button'
							variant='contained'
							color='default'>
							View Event
						</Button>
					</Link>
				</Box>
			</Box>
			<Box
				m={2}
				display='flex'
				flexDirection='row'
				alignItems='center'
				justifyContent='center'>
				<Button variant='contained' color='primary' onClick={editRSVP}>
					RSVP
				</Button>
				<Box mx={1}>
					<Button variant='contained' color='primary' onClick={editRSVPDecline}>
						RSVP Decline
					</Button>
				</Box>
				<Button variant='contained' color='primary' onClick={editTicket}>
					Ticket
				</Button>
				<Box mx={1}>
					<Button variant='contained' color='primary' onClick={editPrivacy}>
						Privacy
					</Button>
				</Box>
				<Button
					data-testid='email-templates-button'
					variant='contained'
					color='primary'
					onClick={manageEmailTemplates}>
					Email Templates
				</Button>
				<Button onClick={deleteEvent}>Delete Event</Button>
			</Box>
		</React.Fragment>
	)
}
