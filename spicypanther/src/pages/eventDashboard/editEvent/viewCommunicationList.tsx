import * as React from 'react'

import {useState, Fragment, useEffect} from 'react'
import {
	Box,
	Grid,
	Typography,
	Button,	

} from '@material-ui/core'

import {useStyles, HtmlView} from 'ui'


import {useQuery} from 'apollohooks'

//import {CommunicationDisplay} from './popups/communicationDisplay'

const deliveryStatus = (status: number) => {
	const statusString = ['Pending', 'Delivery', 'Bounce', 'Complaint', 'Open']
	return statusString[status]
}

const pageStyles = {
	modalbox: {
		outline: 'none',
	},
	image: {
		borderRadius: '50%',
		height: '30px',
		width: '30px',
	},
	root: {
		borderBottom: `1px solid`,		
		fontWeight: 600,
		overFlow: 'auto',
	},
	fieldBox: {
		width: '205px',
		display: 'flex',
		height: '50px',
		alignItems: 'center',
		justifyContent: 'center',
	},
}

export const CommunicationDisplay= (params: any)=> {
    const {communication, close} = params
  
    
    return (
      <Box>
        <Typography>Details</Typography>
        <HtmlView value={communication.message}/>
        <Button onClick={close}>Cancel</Button>
      </Box>
    )
  }


export const CommunicationList = (params: any) => {
	const {event: unsafe, onChange} = params
	
	const {styles} = useStyles(pageStyles)
	const {data: event, loading, error} = useQuery( 'event', { 
		variables: {
			_id: unsafe._id
		}, 
		request: 'childrenGuestCommunications {_id subject _createdTime message}'
	})
	const communications = (event && event.childrenGuestCommunications) || []
	
	const formatDate = (value: string) => {
		const r = value.match(/(\d+)-(\d+)-(\d+)T(\d+):(\d+):/)
		if (!r) return value
		const am = parseInt(r[4]) > 12 ? 'pm' : 'am'
		return r ? `${r[1]}-${r[2]}-${r[3]} ${r[4]}:${r[5]}${am}` : value
	}

	if ( loading)
		return  (<React.Fragment/>)

	return (
		<>
		<Typography variant='h4' align='center'>
				Invitation Log
			</Typography>
			<Grid container direction='row' alignItems='center' justify='center'>
				<Box >
					<Typography>Mail Subject</Typography>
				</Box>
				<Box >
					<Typography>View Email</Typography>
				</Box>
				<Box >
					<Typography>Created</Typography>
				</Box>
			</Grid>

			{communications.map((item: any, index: number) => {
				const showMessage = (message: any) => {
//					displayPopup(CommunicationDisplay, {communication: item})
				}

				return (
					<Grid
						key={index}
						container					
						direction='row'
						alignItems='center'
						justify='center'>
						<Box >
							<Typography>{item.subject}</Typography>
						</Box>
						<Box >
							<Button color='primary' onClick={showMessage}>
								View
							</Button>
						</Box>
						<Box >
							<Typography>{formatDate(item._createdTime)}</Typography>
						</Box>
					</Grid>
				)
			})}
		</>
	)
}