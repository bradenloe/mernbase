import * as React from 'react'
import {Button, Typography, Grid, makeStyles, Paper} from '@material-ui/core'

import {useQuery} from 'apollohooks'
import {useParams} from 'react-router-dom'

import { TabContainer, Tab, useStyles} from 'ui'
/*import {Div, Spinner, Input} from '../../lib/ui'

import {useUser, useUI} from '../../lib/hook'
import {useQuery, mutation} from '../../lib/ql'
import {guestListPageLimit} from '../../settings/settings'
import {_TextField as TextField} from '../../lib/mui/textField'*/

//import {EditEvent} from './editEvent'
//import {GuestList} from './guestList'
//import {Tickets} from './tickets'


//import {Settings} from './settings'
//import {CommunicationList} from './communicationList'
//import {CommunicationDetails} from './communicationDetails'

import { ViewPosts } from './viewPosts'

import { ViewEventDetails} from './viewEventDetails'
import { EditEventDetails} from './editEventDetails'
import { ViewGuestList} from './viewGuestList'
import { CommunicationDetails} from './viewCommunicationDetails'
import { CommunicationList} from './viewCommunicationList'
import { Tickets} from './viewTickets'
import { Settings } from './editEventSettings'



const GuestList = (params:any)=>{return <React.Fragment/>}





const pageStyles = makeStyles({
	container: {
		padding: '1rem',
	},
})

export const EventEdit = () => {	
	const {eventID}: any = useParams()
	const {styles} = useStyles(pageStyles)
	const eventQuery = '_id image name theme address description startTime endTime emailRSVP emailRSVPDecline emailTicket privacyPolicy privacyPolicyRequired published restricted _countChildrenEventGuests  _countChildrenEventGuests'

	const { loading, error, data : event } = useQuery ('event', {variables:{_id: eventID}, request: eventQuery})

	const save = (id: any, updates: any) => {
	//	mutation('updateEvent', {_id: _id, updates: updates}, mutate)
	}

	if (loading) 
		return <React.Fragment />

	if (error) 
		return (
			<React.Fragment>
				{error.message}
			</React.Fragment>
		)

	return (
		<Paper >
			<ViewEventDetails event={event} />
			<TabContainer>
				<Tab label={'Guest List'}>
				
					<ViewGuestList event={event} onChange={()=>{}} />
				</Tab>
				<Tab label={'Invitation Log'}>
					<CommunicationList event={event} />
				</Tab>
				<Tab label={'Invitation Delivery Details'}>
					<CommunicationDetails event={event} />
				</Tab>
				<Tab label='Tickets'>
					<Tickets event={event} />
				</Tab>
				<Tab label='Posts'>
					<ViewPosts event={event} />
				</Tab>
				<Tab label='Settings'>
					<Settings event={event} onChange={save} />
				</Tab>
			</TabContainer>
		</Paper>
	)
}
