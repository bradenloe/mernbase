import { useUser, useStyles, Modal, IFrame } from 'ui'

export const ViewPosts = (params: any)=>{
    const {event} = params

    const {user} = useUser()

    const value = {
        event : event,
        guest : null,
        user: user
    }

    return (
        <IFrame width={'100%'} height={400} value={value} onChange={()=>{}} src='/mediapost' />
    )
}