import * as React from 'react';

import { LabeledInput } from 'ui';

import {
  Box,  
  Typography,
  Button,    
} from '@material-ui/core';

import { useStateHash } from 'usestateobject';
import { useMutation } from 'apollohooks'

const ticketTypes = [
    'Paid',
    'Free'
  ];
  
export const EditEventTicket = (params: any) => {
  const { ticket, close } = params;

  //const { render, changes: changes } = reactHash(ticket);
  const {state: item, update, changes, register} = useStateHash(ticket,[ticket])
  const [updateTicketType, {data, loading, error}] = useMutation ('updateTicketType', {request : '_id'})

  const save = () => {    
      updateTicketType ( {
        variables: { 
          _id: ticket._id, 
          updates: changes()
        }, 
        request: '_id' } ) 
      close ()   

  };

  return (
    <Box>
      <Typography variant='h4' align='center'>
        Edit Ticket
      </Typography>

      
      
        <Box
        display='flex'
        flexDirection='column'
        p={2}
        alignItems='center'
        justifyContent='center'>
        <Box display='flex' flexDirection='row' mt={1}>
          <Box mx={1}>
            <LabeledInput
              {...register('name')}
              data-testid='profile-input-state'              
              label='Ticket Name'
            />
          </Box>
          
        </Box>
        <Box display='flex' flexDirection='row' mt={1}>          
          <Box mx={1}>
            <LabeledInput
              data-testid='profile-input-zipcode'
              value={item.type === 1 ? '0' : item.price }
              onChange={update('price')}
              type='number'
              label='Price'
              disabled={item.type=== 1 ? true : false}
            />
          </Box>
        </Box>
        <Box display='flex' flexDirection='row' mt={1}>
          <Box mx={1}>
          <LabeledInput
              data-testid='profile-input-state'
              {...register('quantityTotal')}              
              type='number'
              label='Quantity'
            />
          </Box>
         
        </Box>
        
      </Box>
        

      <Box
        display='flex'
        flexDirection='row'
        alignItems='center'
        justifyContent='center'
        pb={2}>
        <Box mx={1}>
          <Button
            data-testid='change-password-button'
            variant='contained'
            color='primary'
            onClick={save}>
            Edit Ticket
          </Button>
        </Box>
        <Box mx={1}>
          <Button
            data-testid='email-templates-button'
            variant='contained'
            color='primary'
            onClick={close}>
            Cancel
          </Button>
        </Box>
      </Box>
    </Box>
  );
};
