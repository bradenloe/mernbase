import * as React from 'react';


import { useStateHash } from 'usestateobject';
import { useMutation } from 'apollohooks'

import {
  Box,
  Paper,
  Typography,
  Button,
  IconButton,    
} from '@material-ui/core';

import { LabeledInput } from 'ui'


const ticketTypes = [
    'Paid',
    'Free'
  ];

export const CreateEventTicket = (params: any) => {
  
  const { event, close } = params;
  const eventID = event._id
  const [ createTicketType, {data, loading, error} ] = useMutation ('createTicketType',{request :'_id'})

  const { register, changes: ticketChanges } = useStateHash({
    name: '',
    price: '',
    quantityTotal: '',
  },[]);

  const save =  () => {
    const ticket = ticketChanges();  
    const newTicket = createTicketType ( {variables:{event: eventID, create : ticket }} )              
    close();              
  }

  const buttonpress = ()=>{
    close()
    console.log ('presss')
  }

  return (
    <Box>
      <Typography variant='h4' align='center'>
        Create Ticket
      </Typography>

  
  
          <Box
            display='flex'
            flexDirection='column'
            p={2}
            alignItems='center'
            justifyContent='center'>
            <Box display='flex' flexDirection='row' mt={1}>
              <Box mx={1}>
                <LabeledInput
                  data-testid='profile-input-state'
                  {...register('name')}
                  label='Ticket Name'
                />
              </Box>
              
            </Box>
            <Box display='flex' flexDirection='row' mt={1}>
              
              <Box mx={1}>
                <LabeledInput
                  {...register('price')}
                  type='number'
                  label='Price'
                />
              </Box>
            </Box>
            <Box display='flex' flexDirection='row' mt={1}>
              <Box mx={1}>
              <LabeledInput
                  data-testid='profile-input-state'
                  {...register('quantityTotal')}
                  type='number'
                  label='Quantity'
                />
              </Box>              
            </Box>
            
          </Box>

      <Box
        display='flex'
        flexDirection='row'
        alignItems='center'
        justifyContent='center'
        pb={2}>
        <Box mx={1}>
          <Button
            data-testid='change-password-button'
            variant='contained'
            color='primary'
            onClick={save}>
            Add Ticket
          </Button>
        </Box>
        <Box mx={1}>
          <Button
            data-testid='email-templates-button'
            variant='contained'
            color='primary'
            onClick={buttonpress}>
            Cancel
          </Button>
        </Box>
      </Box>
    </Box>
  );
};
