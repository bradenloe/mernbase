import * as React from 'react'

import {useState, Fragment, useEffect} from 'react'

import {
	Box,
	Paper,
	Typography,
	IconButton,
	makeStyles,
	Grid,
	Button,	
} from '@material-ui/core'

import {useQuery} from 'apollohooks'
import { Modal, useStyles} from 'ui'

import {CreateEventTicket} from './createEventTicket'
import { EditEventTicket } from './editEventTicket'

import AddIcon from '@material-ui/icons/AddBox'
import EditIcon from '@material-ui/icons/Edit'
import DeleteIcon from '@material-ui/icons/Delete'


export const NoTickets = (params: any) => {

    return (
            <Typography variant='h6' align='center'>
              You haven't created any ticket catagories yet. 
            </Typography>
    )
  }
  

const CheckBox = (params:any)=>React.Fragment

const ticketTypes = ['Paid', 'Free']

const pageStyles = {
	modalbox: {
		outline: 'none',
	},
	image: {
		borderRadius: '50%',
		height: '30px',
		width: '30px',
	},
	root: {
		borderBottom: `1px solid`,
		
		fontWeight: 600,
		overFlow: 'auto',
	},
	fieldBox: {
		width: '170px',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
	},
}

const closed = { create : false, edit : false}

	

export const Tickets = (params: any) => {
	const {event} = params
	const {styles} = useStyles(pageStyles)

	
	const [ createVisible, createToggle  ] = useState(false)
	const displayCreate = ()=> {createToggle(true)}
	const dismissCreate = ()=> {createToggle(false)}

	const [ [editVisible, editParams],  editToggle ] = useState([false, { ticket: false}])
	const displayEdit = (data:any)=> {editToggle( [ true, data ])}
	const dismissEdit = ()=> {editToggle([false, { ticket: false}])}

	const {loading, data: result} = useQuery('event', {
		variables : {_id: event._id},
		$: 'childrenTicketTypes {_id name type price description quantityAvailable quantityTotal}',
	})
	const ticketArray = (result && result.childrenTicketTypes) || []

	

	/////////////////////////////////////////////////////////////////////////

	
	const create = () => {		
		displayCreate()
		//display (CreateEventTicket, {event: event, close : dismiss})
	}

	const edit = (ticket: any) => {
		displayEdit ({ticket : ticket})
		
	}
    if (loading)    return <React.Fragment/>

	return (
		<>
		
			{ createVisible && 
				<Modal>
					<CreateEventTicket event={event} onChange={()=>{}} close={dismissCreate}/>					
				</Modal>
			}	

			{ editVisible && 
				<Modal>
					<EditEventTicket ticket={editParams.ticket} onChange={()=>{}} close={dismissEdit}/>					
				</Modal>
			}	

			<Typography variant='h4' align='center'>
				Tickets
			</Typography>

			<Paper elevation={2}>
				<Box
					display='flex'
					flexDirection='row-reverse'
					alignItems='center'
					justifyContent='space-between'
					px={2}
					p={1}>
					<Button
						size='small'
						startIcon={<AddIcon color='primary' />}
						onClick={create}>
						Create New Ticket Category
					</Button>
				</Box>
			</Paper>
			<Box mt={3}>
				{ticketArray.length == 0 && <NoTickets />}

				{ticketArray.length != 0 && (
					<Grid container direction='row' alignItems='center' justify='center'>
						<Box>
							<Typography>Name</Typography>
						</Box>

						<Box >
							<Typography>Price</Typography>
						</Box>

						<Box >
							<Typography>Available</Typography>
						</Box>
						<Box >
							<Typography>Total</Typography>
						</Box>
						<Box >
							<Typography>Edit/Delete</Typography>
						</Box>
					</Grid>
				)}

				{ticketArray.map((ticket: any, index: any) => {
					const onclickedit = () => {
						edit(ticket)
					}

					const remove = () => {
	//					mutation('deleteTicketType', {_id: ticket._id})
						//refetch()
					}

					return (
						<Grid
							key={index}
							container
							
							direction='row'
							alignItems='center'
							justify='center'>
							<Box >
								<Typography>{ticket.name}</Typography>
							</Box>

							<Box >
								<Typography>
									{ticket.type === 0 ? ticket.price : 'Free'}
								</Typography>
							</Box>

							<Box >
								<Typography>{ticket.quantityAvailable}</Typography>
							</Box>
							<Box >
								<Typography>{ticket.quantityTotal}</Typography>
							</Box>
							<Box >
								<IconButton aria-label='delete' onClick={onclickedit}>
									<EditIcon color='primary' />
								</IconButton>
								<IconButton aria-label='delete' onClick={remove}>
									<DeleteIcon color='error' />
								</IconButton>
							</Box>
						</Grid>
					)
				})}
			</Box>
		</>
	)
}
