import * as React from 'react';


import { LabeledInput } from 'ui';


import { useStateHash } from 'usestateobject';

import {
  Box,
  Paper,
  Typography,
  Button,
  IconButton,
  Modal,
  makeStyles,
} from '@material-ui/core';

import { useMutation } from 'apollohooks'

export const CreateEventGuest = (params: any) => {
//  const dismissPopup = useUI((store) => store.dismissPopup);
  const { event, close, onChange } = params;

  const { register, changes: guestChanges } = useStateHash({
    firstName: '',
    lastName: '',
    email: '',
    telephone: '',
  },[]);

  const [createEventGuest, {loading, error, data} ] = useMutation ('createEventGuest', { request : '_id'})

  const save = () => {
    const guest = guestChanges();
    createEventGuest({variables: { _id: event._id, create: guest, $: '_id' }, onCompleted: ()=>{
      onChange()
      close()
    } })
    
  };

  return (
    <Box>
      <Typography variant='h4' align='center'>
        Add Guest
      </Typography>
      
          <Box
            display='flex'
            flexDirection='column'
            p={2}
            alignItems='center'
            justifyContent='center'>
            <Box display='flex' flexDirection='row' mt={1}>
              <Box mx={1}>
                <LabeledInput
                  {...register('firstName')}
                  id='profile-input-state'                  
                  label='First Name'                  
                />
              </Box>
              <Box mx={1}>
                <LabeledInput
                  id='profile-input-zipcode'
                  {...register('lastName')}
                  label='Last Name'
                />
              </Box>
            </Box>
            <Box display='flex' flexDirection='row' mt={1}>
              <Box mx={1}>
                <LabeledInput
                  id='profile-input-state'
                  {...register('email')}
                  label='Email'
                />
              </Box>
              <Box mx={1}>
                <LabeledInput
                  id='profile-input-zipcode'
                  {...register('telephone')}
                  label='Telephone'
                />
              </Box>
            </Box>
          </Box>
        
      <Box
        display='flex'
        flexDirection='row'
        alignItems='center'
        justifyContent='center'
        pb={2}>
        <Box mx={1}>
          <Button
            data-testid='change-password-button'
            variant='contained'
            color='primary'
            onClick={save}>
            Add Guest
          </Button>
        </Box>
        <Box mx={1}>
          <Button
            data-testid='email-templates-button'
            variant='contained'
            color='primary'
            onClick={close}>
            Cancel
          </Button>
        </Box>
      </Box>
    </Box>
  );
};
