export const FileUploader = (params :any ) => {
	const {value, onChange, onTextfile, onBinaryfile, onArrayBuffer, ...more}= params
	
  const onfileupload = ( e:any ) => {
    var reader = new FileReader();
    
     if (onBinaryfile){
      reader.onloadend =  () => {       
        onBinaryfile (reader.result)        
      }
      reader.readAsBinaryString(e.target.files[0])
    } 

    if (onTextfile){
      reader.onloadend =  () => {       
        onTextfile (reader.result)        
      }
      reader.readAsText(e.target.files[0])
    } 

    if (onArrayBuffer){
      reader.onloadend =  () => {       
        onArrayBuffer (reader.result)        
      }
      reader.readAsArrayBuffer(e.target.files[0])
    } 

  }

	return (		
		<input type="file" onChange= { onfileupload }/>
	)
}