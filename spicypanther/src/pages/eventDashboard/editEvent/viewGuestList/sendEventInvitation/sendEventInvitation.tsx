import * as React from 'react'

import {useState} from 'react'
//import {Div, HtmlEditor, HtmlView, PopupTextInput, Spinner} from '../../../lib/ui'
//import {TextFieldSelectHashArray} from '../../../lib/mui/selectTextField'

import {HtmlView} from 'ui'
//import {useUI} from '../../../lib/hook'
import {Box, Typography, Button, Grid} from '@material-ui/core'

import { useMutation, useQuery} from 'apollohooks'
import {useStateArray} from 'usestateobject'
import {withKeys} from 'utils'

const CheckBox = (params: any)=> {return <React.Fragment/>}

export const SendEventInvitation = (params: any) => {
	//const dismissPopup = useUI((store) => store.dismissPopup)
	//const displayPopup = useUI((store) => store.displayPopup)
	const {event, guestlist, close: dismissPopup} = params
	
	
	const [html, htmlSet] = useState('')

	const [guestArray] = useStateArray(withKeys(guestlist, {$selected: true}),[])
	const [sendEventInvitation] = useMutation ('sendEventInvitation', {$: '_id senderName childrenGuestCommunicationDetails{ recipientEmail deliveryStatus }'})
	
	const message= html
	/*const {html: message} =
		html != null && html != '' ? JSON.parse(html) : {html: ''}*/

	const {loading, data: customer} = useQuery('customer', {
		$: '_id childrenEmailTemplates {_id name subject message }',
	})

	if (loading) return <React.Fragment/>

	const templates =
		customer && customer.childrenEmailTemplates
			? customer.childrenEmailTemplates
			: []

	const send = () => {
		// if (message == null || message =='')
		// return displayNotification ('Email is empty')

		const guestIDs = guestArray
			.filter((guest: any) => {
				return guest.$selected == true
			})
			.map((guest: any) => {
				return guest._id
			})

		const message = JSON.parse(html).html
		sendEventInvitation({ 
			variables: {
				parentEvent: event._id,
				guests: guestIDs,
				message: {
					message: message,
					//senderName : senderName,
					senderEmail: 'bradenloe@gmail.com',
					subject: 'test',
				}
			},							
		})
	}

	const onchange = (value: any) => {
		htmlSet(value)
		dismissPopup()
	}

	const onsavetemplate = (value: any) => {
		const savetemplate = (name: string) => {
			/*mutation(
				'createEmailTemplate',
				{
					create: {
						name: name,
						message: value,
					},
					$: '_id',
				},
				dismissPopup
			)*/
		}

		htmlSet(value)
		/*displayPopup(PopupTextInput, {
			title: 'Template Name',
			onClick: savetemplate,
			onCancel: dismissPopup,
		})*/
	}

	const showEditor = () => {
		/*displayPopup(HtmlEditor, {
			value: html,
			onSaveTemplate: onsavetemplate,
			onChange: onchange,
			onCancel: dismissPopup,
		})*/
	}

	const selectTemplate = (template: any) => {
		htmlSet(template.message)
	}

	return (
		<Box m={2} display='flex' flexDirection='column' alignItems='center'>
			<Typography variant='h4' align='center'>
				Guest Communications
			</Typography>

			<Grid container spacing={1}>
				<Grid item xs={12} md={9} justifyContent='center'>
					<Box display='flex' alignItems='center' justifyContent='center'>
						{/*<TextFieldSelectHashArray
							label='Select Email Template'
							_key='name'
							options={templates}
							onChange={selectTemplate}
						/>*/}
					</Box>

					<Box
						m={2}
						minHeight='55vh'
						minWidth='65vw'
						display='flex'
						flexDirection='column'
						alignItems='center'>
						{html && (
							<>
								<Typography variant='h6' align='center'>
									Message:
								</Typography>
								<Box maxHeight='55vh' overflow='auto'>
									<HtmlView value={html} />
								</Box>

								<Box mt={1}>
									<Button
										variant='contained'
										color='primary'
										onClick={showEditor}>
										Edit Message
									</Button>
								</Box>
							</>
						)}

						{!html && (
							<>						
								<Box mt={1}>
									<Button
										variant='contained'
										color='primary'
										onClick={showEditor}>
										Create Message
									</Button>
								</Box>
							</>
						)}

							


					</Box>
				</Grid>
				<Grid item xs={12} md={3}>
					<Box maxHeight='70vh' overflow='auto'>
						{guestArray.length > 0 ? (
							<>
								<Typography variant='h6' align='center'>
									Selected Guests
								</Typography>
								{guestArray.render(({element: item, register}: any) => {
									return (
										<Box key='item._id' display='flex' flexDirection='row' alignItems='center'>
											<CheckBox {...register('$selected')} />
											<Typography>{item.firstName}</Typography>
											<Typography>{item.lastName}</Typography>
										</Box>
									)
								})}
							</>
						) : (
							<Typography align='center'>Select Guests to Send mail</Typography>
						)}
					</Box>
				</Grid>
			</Grid>

			<Box flexDirection='row' display='flex' justifyContent='space-between'>
				<Box mx={1}>
					<Button
						data-testid='profile-password-change-button'
						variant='contained'
						color='primary'
						onClick={send}>
						Send Email
					</Button>
				</Box>

				<Box mx={1}>
					<Button
						data-testid='profile-password-change-button'
						variant='contained'
						color='default'
						onClick={dismissPopup}>
						Cancel
					</Button>
				</Box>
			</Box>
		</Box>
	)
}
