import * as React from 'react';

import { useState, Fragment, useEffect } from 'react';

import {
  Box,
  Paper,
  Typography,
  Button,
  IconButton,
  Modal,  
} from '@material-ui/core';

import { useStyles, LabeledInput} from 'ui'


import { useMutation } from 'apollohooks';
import { useStateHash } from 'usestateobject';

const ArrayText = (params: any) => {
	const {values, value, ...more} = params
	return <Typography {...more} >{values[value]}</Typography>
}


const statusTypes = [
    'pending',
    'rsvp',
    'declined',
    'checked-in',
    'checked-out',
  ];

export const EditEventGuest = (params: any) => {  
  const { guest, close } = params;
  const { register, changes: changes } = useStateHash(guest, guest);

  const [updateEventGuest ]= useMutation ( 'updateEventGuest', {  $: '_id'  })

  const save = () => {
    updateEventGuest ({variables: { _id: guest._id, updates: changes() }, onCompleted: close })
  };

  return (
    <Box>
      <Typography variant='h4' align='center'>
        Edit Guest Details
      </Typography>
      
          <Box
            display='flex'
            flexDirection='column'
            p={2}
            alignItems='center'
            justifyContent='center'>
            <Box display='flex' flexDirection='row' mt={1}>
              <Box mx={1}>
                <LabeledInput
                  id='profile-input-state'
                  {...register('firstName')}                  
                  label='First Name'
                />
              </Box>
              <Box mx={1}>
                <LabeledInput
                  id='profile-input-zipcode'
                  {...register('lastName')}
                  label='Last Name'
                />
              </Box>
            </Box>
            <Box display='flex' flexDirection='row' mt={1}>
              <Box mx={1}>
                <LabeledInput
                  id='profile-input-state'
                  {...register('email')}                  
                  label='Email'
                />
              </Box>
              <Box mx={1}>
                <LabeledInput
                  id='profile-input-zipcode'
                  {...register('telephone')}
                  label='Telephone'
                />
              </Box>
            </Box>
            <Box display='flex' flexDirection='row' mt={1}>
                <ArrayText
                label='Status'
                {...register('status')}
                values={statusTypes}/>
            </Box>
          </Box>
              

      <Box
        display='flex'
        flexDirection='row'
        alignItems='center'
        justifyContent='center'
        pb={2}>
        <Box mx={1}>
          <Button
            data-testid='change-password-button'
            variant='contained'
            color='primary'
            onClick={save}>
            Edit Guest
          </Button>
        </Box>
        <Box mx={1}>
          <Button
            data-testid='email-templates-button'
            variant='contained'
            color='primary'
            onClick={close}>
            Cancel
          </Button>
        </Box>
      </Box>
    </Box>
  );
};
