import * as React from 'react'

import { useState, Fragment, useEffect  } from 'react';
import { Box, Button, Typography  } from '@material-ui/core'


import { useMutation } from 'apollohooks'


import * as XLSX from 'xlsx';

import {FileUploader} from './fileUploader'

const formatCSV= (data: string)=>{	
    
    const workbook = XLSX.read(data, {
        type: 'binary',
        bookVBA: true 
      });
      

    const sheetNames = workbook.SheetNames;
    const currentPage = workbook.Sheets[sheetNames[0]]
            
    const XL_row_object = XLSX.utils.sheet_to_json(currentPage)
         
    return (XL_row_object)    
}

export const UploadSpreadSheet = (params: any) => {
    
    const {eventID, onChange, close} = params

    const [sheetData, sheetDataSet] = useState (null as any)

    const [createEventGuests, {loading, data, error}]= useMutation ('createEventsGuests', {request : '_id'} )

    const save = ()=>{            
        const events = sheetData.map  ((item: any)=>{
        

            return ({
                firstName : item.firstName,
                lastName : item.lastName,
                email : item.email,
                telephone: item.phone,
                notes: item.note,
            })
         })

        

        createEventGuests ( {
            variables : {_id: eventID, create : events, $ :'_id'}, 

            onCompleted : ()=>{
                onChange ()
                close()
            }
        })                    
        
    }    

    const upload = (data: any) =>{
        const sheet = formatCSV(data)
        sheetDataSet (sheet)
    }
    

    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    if (sheetData == null)
        return (
            <>
                <Box className='popuptitle'>
                    <Box>Upload Spreadsheet</Box>
                </Box>
                <FileUploader onBinaryfile={upload}>Upload CSV</FileUploader>
                <Button onClick={close}>Cancel</Button>
                
                <a href='/sample.xls'>Sample Guest List</a>
            </>
        )

    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////

    return (
        <Box>  
            <Typography>Upload SpreadSheet</Typography> 
            {sheetData.map ((item: any, index: number)=>{
                return (
                <Box key={index} className='columns4'>         
                    <Typography>{item.firstName}</Typography> 
                    <Typography>{item.lastName}</Typography> 
                    <Typography>{item.email}</Typography>
                    <Typography>{item.phone}</Typography>
                </Box>)
            })}

            <Button onClick={save}>Save</Button>
            <Button onClick={close}>Cancel</Button>
        </Box>
  )
}
