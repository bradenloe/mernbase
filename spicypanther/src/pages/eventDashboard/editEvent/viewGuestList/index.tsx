import * as React from 'react'

import {useState, Fragment, useEffect} from 'react'
import {green} from '@material-ui/core/colors'

import { Modal, useStyles} from 'ui'
import { excelDownload } from './excelDownload'

import {
	Typography,
	Grid,
	Button,
	makeStyles,
	Paper,
	Box,
	Card,
	CardActionArea,
	IconButton,
	CardActions,
	CardContent,	
	CardMedia,
} from '@material-ui/core'



import {useStateArray} from 'usestateobject'
//import {Pagination} from '../../lib/mui/pagination'

import {CreateEventGuest} from './createEventGuest'
import {EditEventGuest} from './editEventGuest'
import {UploadSpreadSheet} from './uploadSpreadSheet'
import {SendEventInvitation} from './sendEventInvitation'

import {withKeys, withoutKeys } from 'utils'

import BackupIcon from '@material-ui/icons/Backup'
import AddIcon from '@material-ui/icons/AddBox'
import EditIcon from '@material-ui/icons/Edit'
import DeleteIcon from '@material-ui/icons/Delete'
import CloudDownloadIcon from '@material-ui/icons/CloudDownload'
import MailIcon from '@material-ui/icons/Email'

import {useQuery, useSubscription} from 'apollohooks'


const gustListPageLimit = 8
const CheckBox = (params: any) =>{return (<Fragment/>)}


const ArrayText = (params: any) => {
	const {values, value, ...more} = params
	return <Typography {...more} >{values[value]}</Typography>
}

const statusTypes = ['pending', 'rsvp', 'declined', 'checked-in', 'checked-out']


 
const pageStyles = {
	modal: {
		outline: 'none',
		background: 'white'
	},
	image: {
		borderRadius: '50%',
		height: '30px',
		width: '30px',
	},
	root: {
		borderBottom: `1px solid`,		
		fontWeight: 600,
		overFlow: 'auto',
	},
	fieldBox: {
		width: '143px',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
	},
	emailBox: {
		width: '190px',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
	},
	checkBox: {
		width: '10px',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
	},
}

export const ViewGuestList = (params: any) => {
	const {event} = params
	const {styles} = useStyles(pageStyles)

	//const displayNotification = useUI((store) => store.displayNotification)
	const [selectAll, selectAllSet] = useState(false)
	const [currentPage, currentPageSet] = useState(0)

	const [modalCreate , c]  = useState (false)
	const displayCreate = ()=>{ c (true)}
	const dismissCreate = ()=> { c (false)}

	const [modalImport , d ]  = useState (false)
	const displayImport = ()=>{ d (true)}
	const dismissImport = ()=> { d (false)}


	const [modalEdit , e ]  = useState (null)
	const displayEdit = (id: any)=>{ e (id) }
	const dismissEdit = ()=> { e (null)}

	const [modalEmail , f ]  = useState (null)
	const displayEmail = (id: any)=>{ f (id) }
	const dismissEmail = ()=> { f (null)}


	

	const {data: result, loading, error} = useQuery( 'event', { variables : { _id: event._id},	$: ` _countChildrenEventGuests childrenEventGuests(_skip: ${15*currentPage}, _limit: 15 ){_id firstName lastName email telephone status typeDescription privacyPolicyAccepted}`,
		},		
	)

	const array= [] as any
	const [guestArray, guestArraySet] = useStateArray(
		array,[result]
	)


	const [totalRecords, totalRecordsSet] = useState(0)

	React.useEffect(() => {
		if (result && result.childrenEventGuests)
			guestArraySet(result.childrenEventGuests)
	}, [result])

	React.useEffect(() => {
		guestArraySet(withKeys(guestArray, {$selected: selectAll}) || [])
	}, [selectAll])

	const {data : subscription,  loading : cat1} = useSubscription ('subscribeEventGuests', { $: '_id _action status firstName lastName email telephone typeDescription privacyPolicyAccepted' })

	React.useEffect (()=>{
		if (!subscription)	return

		const action = subscription._action
		if (action == 'create') {
			return guestArraySet([...guestArray, subscription])
		}

		if (action.localeCompare('delete') == 0) {
			const updates = guestArray.filter((item: any) => {
				return item._id.localeCompare(subscription._id) != 0
			})
			return guestArraySet(updates)
		}

		if (action.localeCompare('update') == 0) {
			const updates = guestArray.map((item: any) => {
				return item._id.localeCompare(subscription._id) == 0 ? subscription : item
			})
			return guestArraySet(updates)
		}	
		
	},[subscription])

	/////////////////////////////////////////////////////////////////////////
	const create = () => {
		displayCreate ()		
	}

	/////////////////////////////////////////////////////////////////////////
	const upload = (data: any) => {
		displayImport ()
		
	}

	/////////////////////////////////////////////////////////////////////////
	const edit = (guest: any) => {
		displayEdit (guest)
	}

	
	/////////////////////////////////////////////////////////////////////////
	const download = () => {
		const displayArray = withoutKeys(guestArray, ['_id', '$selected', 'action']).map ((item :any )=> {
			return (
				{...item, status: statusTypes[item.status]}
			)
		})


		if (guestArray.length != 0)
			return excelDownload(
				displayArray,
				'guestlist.xls'
			)
		//displayNotification('Your guestlist is empty')
	}

	/*const handlePageChange = (pagenumber: any) => {
		const skip = (pagenumber - 1) * guestListPageLimit
		console.log(skip)
		currentPageSet(pagenumber)
		query(
			'event',
			{
				_id: event._id,
				$: ` _countChildrenEventGuests childrenEventGuests(_limit: ${guestListPageLimit}, _skip: ${skip} ){_id firstName typeDescription lastName email telephone status}`,
			},
			(data: any, error: string) => {
				guestArraySet(
					withKeys(data.childrenEventGuests, {$selected: selectAll}) || []
				)
				totalRecordsSet(data._countChildrenEventGuests)
			}
		)
	}*/

	/////////////////////////////////////////////////////////////////////////
	if (!event || loading) return <React.Fragment/>

	return (
		<>
			{ modalCreate && 
				<Modal classes={styles.modal} open={true}>
					<CreateEventGuest event = {event} close = {dismissCreate} onChange={()=>{}}/>
				</Modal>
			}
			{ modalImport && 
				<Modal open={true}>
					<UploadSpreadSheet event = {event} close = {dismissImport} onChange={()=>{}}/>
				</Modal>
			}

			{ modalEdit && 
				<Modal open={true}>
					<EditEventGuest  guest = {modalEdit} close = {dismissEdit} onChange={()=>{}}/>
				</Modal>
			}

			{ modalEmail && 
				<Modal open={true}>
					<SendEventInvitation 
						event = {event} 
						close = {dismissEmail} onChange={()=>{}}
						guestlist=  { guestArray }						
					/>
				</Modal>
			}

			<Typography variant='h4' align='center'>
				Guest List
			</Typography>
			<Box >
				<Box
					display='flex'
					flexDirection='row-reverse'
					justifyContent='space-between'
					alignItems='center'
					p={1}
					px={2}>
					<Button
						size='small'
						startIcon={<MailIcon style={{color: green[500]}} />}
						onClick={displayEmail}>
						Send Invitation Email
					</Button>
					<Button
						size='small'
						startIcon={<BackupIcon color='action' />}
						onClick={upload}>
						Import Guest List
					</Button>
					<Button
						size='small'
						startIcon={<CloudDownloadIcon color='secondary' />}
						onClick={download}>
						Download Guest List
					</Button>
					<Button
						size='small'
						startIcon={<AddIcon color='primary' />}
						onClick={create}>
						Create New Guest
					</Button>
				</Box>
			</Box>
			<Box mt={3}>
				<Grid container direction='row' alignItems='center' justify='center'>
					<Box >
						<CheckBox value={selectAll} onChange={selectAllSet} />
					</Box>

					<Box >
						<Typography>First Name</Typography>
					</Box>
					<Box >
						<Typography>Last Name</Typography>
					</Box>
					<Box >
						<Typography>Telephone</Typography>
					</Box>
					<Box >
						<Typography>Email</Typography>
					</Box>
					<Box >
						<Typography>Ticket Type</Typography>
					</Box>
					<Box >
						<Typography>Status</Typography>
					</Box>
					<Box >
						<Typography>Privacy Policy</Typography>
					</Box>
					<Box >
						<Typography>Edit/Delete</Typography>
					</Box>
				</Grid>

				{guestArray.render(({element: guest, index, register}: any) => {
					const onclickedit = () => {
						edit(guest)
					}

					const remove = () => {
						
						
					}

					return (
						<Grid
							key={index}
							container							
							direction='row'
							alignItems='center'
							justify='center'>
							<Box> 
								<CheckBox {...register('$selected')} />
							</Box>
							<Box >
								<Typography>{guest.firstName}</Typography>
							</Box>
							<Box >
								<Typography>{guest.lastName}</Typography>
							</Box>
							<Box >
								<Typography>{guest.telephone}</Typography>
							</Box>
							<Box >
								<Typography>{guest.email}</Typography>
							</Box>
							<Box >
								<Typography>{guest.typeDescription}</Typography>
							</Box>
							<Box >
								<ArrayText value={guest.status} values={statusTypes} />
							</Box>

							<Box >
								<Typography>{guest.privacyPolicyAccepted}</Typography>
							</Box>

							<Box >
								<IconButton aria-label='delete' onClick={onclickedit}>
									<EditIcon color='primary' />
								</IconButton>
								<IconButton aria-label='delete' onClick={remove}>
									<DeleteIcon color='error' />
								</IconButton>
							</Box>
						</Grid>
					)
				})}
				<Box display='flex' flexDirection='row-reverse'>
					{/*<Pagination
						totalRecords={totalRecords}
						pageLimit={guestListPageLimit}
						pageNeighbours={1}
						onPageChange={handlePageChange}
					/>*/}
				</Box>
				</Box>
		</>
	)
}
