import * as React from 'react'
import {
	Box,
	Paper,
	Typography,
	IconButton,
	makeStyles,
	CardMedia,
	CardContent,
	Modal,
} from '@material-ui/core'

import EditIcon from '@material-ui/icons/Edit'
//import {Image, DateTimeInput} from '../../lib/ui'

//import {muiFormatDate} from '../../lib/utils'
import {EditEventDetails} from './editEventDetails'
const muiFormatDate = (x:any)=> {return (x)}



const useStyles = makeStyles({
	imageStyle: {
		width: '70%',
		height: 'auto',
	},
	box: {
		position: 'relative',
		backgroundColor: '#f0f0f0',
	},
	overlay: {
		position: 'absolute',
		top: '0px',
		right: '20px',
		height: '200px',
		color: 'black',
		opacity: '0.9',		
	},
})

export const ViewEventDetails = (params: any) => {
	//const displayPopup = useUI((store) => store.displayPopup)
	//const dismissPopup = useUI((store) => store.dismissPopup)
	const {event, onChange} = params
	const classes = useStyles()
	const [editor, editorState] = React.useState(false)

	const editEvent = () => {
		editorState ( !editor )
	}

	const close = ()=> editorState(false) 

	return (
		<>
			<Modal open={editor}>
				<Box>
					<EditEventDetails event={event} close={close} onChange={()=>{}}/>
				</Box>
			</Modal>




			<Box display='flex' flexDirection='row-reverse' px={2}>
        		<IconButton aria-label='delete' onClick={editEvent}>
          		<EditIcon color='primary' />
        		</IconButton>
      		</Box> 

			<Box
				display='flex'
				flexDirection='column'
				alignItems='center'
				className={classes.box}>
				<CardMedia
					component='img'
					alt={event.name}
					height='200'
					image={event.image}
				/>
				<Box
					display='flex'
					flexDirection='column'
					justifyContent='space-between'
					p={2}
					className={classes.overlay}>
					<Box display='flex' flexDirection='column' alignItems='center'>
						<Typography variant='h5'>{event.name}</Typography>
						<Typography variant='body2' color='textSecondary' component='p'>
							{event.description}
						</Typography>
					</Box>
					<Box display='flex' flexDirection='column' alignItems='center'>
						<Typography variant='body2' color='textSecondary' component='p'>
							Starts At: {muiFormatDate(event.startTime)}
						</Typography>
						<Typography variant='body2' color='textSecondary' component='p'>
							Ends At: {muiFormatDate(event.endTime)}
						</Typography>
					</Box>
				</Box>
			</Box>
		</>
	)
}
