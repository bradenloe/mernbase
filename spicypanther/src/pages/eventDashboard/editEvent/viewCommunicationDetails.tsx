import * as React from 'react'

import {useState, Fragment, useEffect} from 'react'
import {
	Box,
	Paper,
	Grid,
	Typography,
	Button,
	makeStyles,
} from '@material-ui/core'

import {useStyles} from 'ui'
import {useQuery } from 'apollohooks'

//import {CommunicationDisplay} from './popups/communicationDisplay'
const CommunicationDisplay = (params: any)=> <React.Fragment/>

const deliveryStatus = (status: number) => {
	const statusString = ['Pending', 'Delivery', 'Bounce', 'Complaint', 'Open']

	return statusString[status]
}



/*const useStyles = makeStyles(({palette}) => ({
	modalbox: {
		outline: 'none',
	},
	image: {
		borderRadius: '50%',
		height: '30px',
		width: '30px',
	},
	root: {
		borderBottom: `1px solid ${palette.primary.light}`,
		color: palette.secondary.dark,
		fontWeight: 600,
		overFlow: 'auto',
	},
	fieldBox: {
		width: '205px',
		display: 'flex',
		height: '50px',
		alignItems: 'center',
		justifyContent: 'center',
	},
}))*/

export const CommunicationDetails = (params: any) => {
	const {event: unsafe, onChange} = params
	const {styles} = useStyles({})

	const {data: event, loading, error} = useQuery(
		'event',
		{
			variables : {_id: unsafe._id},
			$: 'childrenGuestCommunicationDetails {_id recipientEmail recipientName deliveryDetails deliveryStatus} ',
		},		
	)
	const communicationDetails = (event && event.childrenGuestCommunicationDetails) || []
	if (loading)	return <React.Fragment/>

	return (
		<>
		<Typography variant='h4' align='center'>
				Invitation Delivery Details
			</Typography>
			<Grid container direction='row' alignItems='center' justify='center'>
				<Box >
					<Typography>Full Name</Typography>
				</Box>
				<Box >
					<Typography>Email</Typography>
				</Box>
				<Box >
					<Typography>Delivery Details</Typography>
				</Box>
				<Box >
					<Typography>Delivery Status</Typography>
				</Box>
			</Grid>

			{communicationDetails.map((item: any, index: number) => {
				return (
					<Grid
						key={index}
						container						
						direction='row'
						alignItems='center'
						justify='center'>
						<Box >
							<Typography>{item.recipientName}</Typography>
						</Box>
						<Box >
							<Typography>{item.recipientEmail}</Typography>
						</Box>
						<Box >
							<Typography>{item.deliveryDetails}</Typography>
						</Box>
						<Box >
							<Typography>{deliveryStatus(item.deliveryStatus)}</Typography>
						</Box>
					</Grid>
				)
			})}
		</>
	)
}
