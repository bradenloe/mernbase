import * as React from 'react'
import { Box, Typography, Button  } from '@material-ui/core'

import {useStyles, LabeledInput, ImageUploader} from 'ui'



import { useStateHash } from 'usestateobject';

const pageStyles = {
    root: {
        
    }
}




const ColorPicker = (params:any)=>{
    return <React.Fragment/>
}

export const EditEventDetails = (params: any) => {    
    const {event, onChange, close} = params  
    const { register, changes: changes} = useStateHash (event, event)  
    const {styles} = useStyles(pageStyles)

    const save = ()=>{
        const updates = changes()
        if(updates)
            onChange( updates )
        close()        
    }


  return (
    <Box sx={styles.root}>
        

            <Typography variant="h4" align='center'>
                Edit Event
            </Typography>
            <Box  display='flex' flexDirection='row' p={2}  alignItems='center' justifyContent='center'>
            <Box mr={2}>
                
                    <ImageUploader {...register('image')} label="Event Cover"></ImageUploader>
                
            </Box>
            <Box>
            <Box mt={1}>
                
                    <LabeledInput {...register('name')} label='Event Name*'/>
                
            </Box>
            <Box mt={1}>
                
                    <LabeledInput {...register('description')} label='Description*'/>
                
            </Box>
            <Box mt={1}>
                
                    <LabeledInput type="datetime-local" {...register('startTime')} label='Event Starts*'/>
                
            </Box>
            <Box mt={1}>
                
                    <LabeledInput type="datetime-local" id='endTime' {...register('endTime')} label='Event Ends*'/>
                
            </Box>
            <Box mt={1}>                    
                    <LabeledInput {...register('notes')} label='Event Notes'/>                    
            </Box>
            <Box mt={1}>                    
                <ColorPicker
                    defaultValue='Event Primary Color'
                    value = {register('primaryColor').value  ? register('primaryColor').value : '#5F2EEA' }
                    onChange = {(color: any) => {{register('primaryColor').onChange(color)}}}
                />                   
            </Box>
        </Box>
    </Box>
    
    <Box display='flex' flexDirection='row' alignItems='center' justifyContent='center' pb={2}>
        <Box mx={1}>
            <Button data-testid="change-password-button" variant='contained' color='primary'   onClick={save}>
                Update Event
            </Button>
            </Box>
            <Box mx={1}>
            <Button data-testid="email-templates-button" variant='contained' color='primary' onClick={close}>
                Cancel
            </Button>
        </Box>
    </Box>

    



    
        </Box>
  )
}
