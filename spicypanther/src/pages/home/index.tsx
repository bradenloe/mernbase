import { useAlert, useStyles } from 'ui';
import { Button, Box } from '@material-ui/core';
import { Link } from 'react-router-dom';

export const Home = ()=>{
    const { alert }= useAlert ()

    const alertX = () =>{
        console.log (alert ('error'))
    }
    
    return (
        <>
        <Box><Link to='/register'>Register</Link></Box>
        
        <Box><Link to='/login'>Login</Link></Box>

        

        <Button variant='contained' onClick={alertX}>Test Alert</Button>
        </>
    )
}