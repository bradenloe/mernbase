import { useEffect, useState } from 'react';
import { useWeb3React } from '@web3-react/core';
import { contractAbi } from '../../contracts/mediaitem';

import { useParams } from 'react-router-dom';
import { Avatar, Box, Divider, Paper, Typography, Button } from '@material-ui/core';

import { useControlState } from 'utils'
import {getValueJson, setValueJson} from 'localstorage'
import { Link } from 'react-router-dom';
import { hashmap } from 'utils'

const errorState = (error: string) => [0, error, {}];
const initState = [
  0,
  null,
  {}  
];



export const MediaList = () => {
  const { active, account, activate, library } = useWeb3React();
  
  const [[loading, error, list], setState] = useState(initState) as any;
  

  useEffect (()=>{
    const followed = getValueJson ('viewed')  || {}
    console.log  (followed)
    setState ([0, null, followed])      
  },[])

  
  

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error: {error}</p>;
  if (!active) return <p>Connect</p>;

  return (
    <Paper>
      <Box p={2} display="flex" flexDirection="column">
                
        {hashmap (list, ({key, value} : any)=>{
          return (
          <Box key={key}>
            <Link to={`/mediaitem/${key}`}>
            <img  width={100} height={100} src={value.content} />
            </Link>
          </Box>)
        })}
        
        


      </Box>
    </Paper>
  );
};
