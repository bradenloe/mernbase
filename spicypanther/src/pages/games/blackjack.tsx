import {useEffect, useState} from 'react'
import { useWeb3React } from '@web3-react/core'
import { useStyles } from 'ui'

import { hashmap, useControlState } from 'utils'

import { contract as blackjack, address as blackjackAddress } from '../../contracts/blackjack'
import { Grid, Input, Avatar, Box, Divider, Paper, Typography, Button } from '@material-ui/core';

export const GameTemplate = (params: any) =>{
    const {title, instructions: A, game: B, options: C} = params

    const [a,b] = useControlState ('')
    return (
        <>
            <Input value={a} onChange={b} />
            <Typography variant='h6'>{title}</Typography>
            <Grid container>
                <Grid item xs={2}><A/></Grid>
                <Grid item xs={8}><B/></Grid>
                <Grid item xs={2}><C/></Grid>                                
            </Grid>
        </>
    )
}


const Instructions = ()=>{
    return <div>
        instructions on how to play blackjack
    </div>
}

const Options = () =>{
    return <div>
        options for creating game
        or game history or something 
        idk
    </div>
}




////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
const pageStyles = (params: any) => {
    const {theme, vw, vh} = params
    return ({   
        cardview :{            
            boxShadow: 1,
            borderRadius: 5,
            p: 2,
            minWidth: 30
        }
    })
}


const CardView = (params: any)=>{
    const {cards}= params

    const { styles } = useStyles (pageStyles)

    return (
        <Box sx= {styles.cardview}>

            {cards.map ((card: any, index: number)=>{
                return (<img key={index} src={`/img/deck/${card}.jpg`}></img>)
            })}
            
    </Box>
    )
}





////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
export const BlackJack= () =>{
    const {active, account, activate, library } = useWeb3React()
    
    

    const Game = () =>{        
        const [game, gameState] = useState ({})
        const [contract, contractSet ] =  useState(null as any)
        const [ refreshstate, refresh] = useState (0)
        console.log (game)

        //load the contract
        useEffect (()=>{
            (async ()=>{
                const contract = await new library.eth.Contract(blackjack, blackjackAddress)
                contractSet (contract)
            })()
        },[account])

        //subscribe to new events
        useEffect (()=>{
            (async ()=>{
            
                const options = {
                    address: blackjackAddress,
                    topics: [
                        '0x640a1937ee806eb73e79556271508da15e4f81612236da73fa30f5b2524ea427'
                    ]
                };

                const callback = (error: any, event: any) => {                    
                    refresh(refreshstate+1)
                }

                library.eth.subscribe( 'logs', options, callback )
            })()
        },[])

        //load or refresh the current game
        useEffect (()=>{
            (async ()=>{
                if (!active)
                    return

                if (!contract)
                    return
                
                const history={} as any
                const moves = await contract.methods.moveCount.call().call()
                
                for (let x=0; x< moves ; x++){
                    
                    const item= await contract.methods.history(x).call()
                    console.log (item)
                    
                    if (item && item.Address){

                        if ( history[item.Address] == null )
                            history[item.Address]=[]

                        history[item.Address].push( parseInt(item.cardSuite)*20 + parseInt(item.cardFace))
                    }
                        
                }       
                gameState(history)                            
                
            })()
        },[contract, refreshstate])
    
        const play= async ()=>{
 /*           if (player0.Address != account || player1.Address != account)
                return*/

            if (contract){                
                const cat = await contract.methods.Play().send({from : account})
                    .on('data', (event :any) => console.log("data",event))
                    .on('changed', (changed :any) => console.log('change',changed))
                    .on('error', (err :any) => console.log ('error',err))
                    .on('connected', (str :any) => console.log('str',str))
            }
        }

        
                
        return (
            <div className='CardGameTwoPlayer'>
                {hashmap (game, (item: any, index:any)=>{
                    
                    return (
                        <div key={index}>
                        <CardView cards={item}></CardView>            
                        </div>
                    )
                })}
                
                {account ? 
                    <div className='gamebar'>
                        <button onClick={play}>Hit</button>
                        <button>Hold</button>
                    </div>:
                    <div>connect metamask to play</div>}

            </div>
        )        
    }

    if (!active)
        return (<div>thats wrong</div>)

    return (
        <GameTemplate title='BlackJack' instructions={Instructions} game={Game} options={Options}/>
    )
}


/*const [[loading, error, data], setState ] = useState([1,0,{ balance: 0, owner: 1}])
    const {active, account, activate, library } = useWeb3React()    
    
    useEffect (()=>{
        (async()=>{
            const owner=1
        
            setState([0,0,{
                owner : owner,
                balance: 0
            }]) 

            if (!active)
                return
            
            const contract = await new library.eth.Contract(lotto1, contractAddr)
            if (contract) {  
                
                const cat = await contract.methods.currentPool.call().call()
                
                setState ([0,0,{
                    owner : 1,
                    balance: cat
                }])
            }
        })()
        
    },[account])

    const play = async ()=>{
        if (!account)
            return
        const contract = await new library.eth.Contract(lotto1, contractAddr)
        const cat = await contract.methods.join('76').send({from: account, value : 1})
        console.log (cat)

    }
*/