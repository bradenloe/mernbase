import { useEffect, useState } from 'react';
import { useWeb3React } from '@web3-react/core';
import { contractAbi } from '../../contracts/mediapost';

import { useParams } from 'react-router-dom';
import { Avatar, Box, Divider, Paper, Typography, Button } from '@material-ui/core';

import { useControlState } from 'utils'
//const contractAddr='0x3F6F83945A2B2F901bDFbB7b60a2AEeCF5f5ADF8'
import {getValue, setValue} from 'localstorage'

const errorState = (error: string) => [0, error, {}];
const initState = [
  1,
  null,
  {
    title: '',
    description: '',
    postCount: 0,
    posts: [],    
  },
  null
];

const CreatePost = (params: any) => {
  const [post, postSet] = useControlState ('')
  const { contract, account } = params;

  const createPost = async ()=>{  
    if (contract) {
      contract.methods.createPost(post).send({'from': account});
    }

  }

  return (
    <>
      <Typography variant="h6" align="center">
        Create Post
      </Typography>
      <Box display="flex" flexDirection="column" alignItems="center">
        <input
          value={post}
          onChange={postSet}
          placeholder="say something about event"
        />
        <Button onClick={createPost}>Click</Button>
      </Box>
    </>
  );
};

export const MediaBlog = () => {
  const { active, account, activate, library } = useWeb3React();
  const { blog: blogID } = useParams() as any

  const [[loading, error, blog, contract], setState] = useState(initState) as any;

  const follow  = () =>{
    const cookie = getValue ('followed') as any    
    const followed = JSON.parse (cookie||"{}") as any
    followed[blogID]={}
    setValue ('followed', JSON.stringify(followed))
  }

  useEffect(() => {
    (async () => {
      if (!active) return;

      try {
        console.log (blogID)

        const contract = (await new library.eth.Contract(
          contractAbi,
          blogID
        )) as any;

        if (contract) {
          const posts = [] as any;

          const blog = await contract.methods.blog.call().call();
          const { description, image, title, postCount } = blog;

          const max = postCount > 5 ? 5 : postCount;

          for (let x = 0; x < max; x++) {
            const post = await contract.methods.posts(postCount - 1 - x).call();
            posts.unshift(post);
          }

          setState([
            0,
            0,
            {
              postCount: postCount,
              posts: posts,
              description: description,
              title: title,
              image: image,
            },
            contract
          ]);
        }
      } catch (e) {
        setState(errorState('blog not found'));
      }
    })();
  }, [account]);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error: {error}</p>;
  if (!active) return <p>Connect</p>;

  const onfileupload = ( e:any ) => {
    const reader = new FileReader();
    
    reader.onloadend =  () => {
       const image = reader.result
       console.log (image)
       //setImageString(image)
       //onChange (image)
    }        
    reader.readAsDataURL(e.target.files[0])
 }    


  return (
    <Paper>
      <Box p={2} display="flex" flexDirection="column">
        <Box>
          <Box
            display="flex"
            flexDirection="row"
            justifyContent="space-between"
            alignItems="center"
            p={2}
            pl={2}
          >
            <Box display="flex" flexDirection="row" alignItems="center">
              <Avatar src={blog.image} />

              <Typography variant="h6" align="left" color="textPrimary">
                &nbsp;&nbsp;{blog.title}
              </Typography>
              <Button variant="contained" onClick={follow}>Follow</Button>
            </Box>
          </Box>

          <Typography variant="h6" align="left" color="textPrimary">
            &nbsp;&nbsp;{blog.description}
          </Typography>

          {blog.posts.length != 0 && <Divider />}
          {blog.posts.map((item: any, index: any) => {
            return (
              <>
                <Box display="flex" p={1} pl={2} key={index}>
                  <Typography variant="h6" align="left" color="textPrimary">
                    {item.content}
                  </Typography>
                </Box>

                <Divider />
              </>
            );
          })}
        </Box>
        {/*<input type='file' onChange={onfileupload}></input>*/}
      </Box>

      {active && account && <CreatePost contract={contract} account= {account}/>}
    </Paper>
  );
};
