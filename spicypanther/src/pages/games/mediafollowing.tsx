import { useEffect, useState, Fragment } from 'react';
import { useWeb3React } from '@web3-react/core';
import { contractAbi } from '../../contracts/mediapost';

import { Avatar, Box, Divider, Paper, Typography, Button } from '@material-ui/core';
import { Link } from 'react-router-dom';

import { hashmap } from 'utils'
import { getValue } from 'localstorage'

const errorState = (error: string) => [0, error, {}];
const initState = [
  1,
  null,
  {}  
];

export const MediaFollowing = () => {
  const { active, account, activate, library } = useWeb3React();

  const [[loading, error, following], setState] = useState(initState) as any;

  useEffect(() => {
    (async () => {
      if (!active) return;

      const cookie = getValue ('followed') as any    
      const followed = JSON.parse (cookie||"{}") 

      if (Object.keys(followed).length === 0)
        setState ([0, 'you are not subscribed to anyone', {}])

      hashmap (followed, async ({ key } : any)=>{        
        
        try {
          const contract = (await new library.eth.Contract(
            contractAbi,
            key
          )) as any;
  
          if (contract) {
            const posts = [] as any;
  
            const blog = await contract.methods.blog.call().call();
            const { description, image, title, postCount } = blog;
  
            const max = postCount > 3 ? 3 : postCount;
  
            for (let x = 0; x < max; x++) {
              const post = await contract.methods.posts(postCount - 1 - x).call();
              posts.unshift(post);
            }

            followed[key] = {
              posts : posts,
              description: description,
              title: title,
              image: image,
              address: key
            }  
            
            setState ([0, null, followed])
          }
        } 
        catch (e) {
          console.log (`blog ${e} not found`)
        }      
      }) 

      
      
    })();
  }, [account]);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error: {error}</p>;
  if (!active) return <p>Connect</p>;

  
  return (
    <Paper>
      
      <Box p={2} display="flex" flexDirection="column">      
      {hashmap (following, ({value : blog, key }: any)=> {
        

        if ( blog == null)
          return (<></>)

        return (
        <Box 
          key={key}
          display="flex"
          flexDirection="row"
          >
        
          <Box
            display="flex"
            flexDirection="row"
            justifyContent="space-between"
            alignItems="center"
            p={2}
            pl={2}
          >
          
          <Link to={`/mediablog/${blog.address}`} >
            <Box display="flex" flexDirection="row" alignItems="center">
              <Avatar src={blog.image} />

              <Typography variant="h6" align="left" color="textPrimary">
                &nbsp;&nbsp;{blog.title}
              </Typography>
              
            </Box>
            </Link>
          </Box>
          

          <Typography variant="h6" align="left" color="textPrimary">
            &nbsp;&nbsp;{blog.description}
          </Typography>

          {blog && blog.posts && blog.posts.length != 0 && <Divider />}
          {blog && blog.posts && blog.posts.map((item: any, index: any) => {
            return (
              <Fragment key = {index}>
                <Box display="flex" p={1} pl={2} key={index}>
                  <Typography variant="h6" align="left" color="textPrimary">
                    {item.content}
                  </Typography>
                </Box>

                <Divider />
              </Fragment>
            );
          })}


        </Box>
          )})}
      </Box>
      
    </Paper>
  );
};
