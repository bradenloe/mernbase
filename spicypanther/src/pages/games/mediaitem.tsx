import { useEffect, useState } from 'react';
import { useWeb3React } from '@web3-react/core';
import { contractAbi } from '../../contracts/mediaitem';

import { useParams } from 'react-router-dom';
import { Avatar, Box, Divider, Paper, Typography, Button } from '@material-ui/core';

import { useControlState } from 'utils'
import {getValueJson, setValueJson} from 'localstorage'

const errorState = (error: string) => [0, error, {}];
const initState = [
  0,
  null,
  {
    description : 'private',
    content : ''
  }  
];



export const MediaItem = () => {
  const { active, account, activate, library } = useWeb3React();
  const { itemID } = useParams() as any

  const [[loading, error, item], setState] = useState(initState) as any;

  useEffect (()=>{
    const followed = getValueJson ('viewed')  || {}

      if (followed[itemID])
        setState ([0, null, followed[itemID]])
  },[])

  const viewItem  = async ()=>{
    
    try {
      const contract = (await new library.eth.Contract(
        contractAbi,
        itemID
      )) as any;

      if (contract) {
        setState ([1, null, {
          description : '',
          content : item
        }])


        const post = await contract.methods.viewContent().send({'from': account, value : 1})        
        .then ( (result : any, a : any, b: any)=>{          

          const content = result.events.ContentReady.returnValues.contents

          const followed = getValueJson ('viewed') || {}
          
          followed[itemID]={
            description : 'cached',
            content: content
          }
          
          setValueJson ('viewed', followed)


          setState ([0, null, {
            description : "live",
            content : content
          }])

        })        
      }
    }
    catch (e){
      console.log (e)
    }
  }
  
  const onfileupload = ( e:any ) => {
    const reader = new FileReader();
    
    reader.onloadend =  async () => {
       const image = reader.result
       
       try {
        const contract = (await new library.eth.Contract(
          contractAbi,
          itemID
        )) as any;
  
        if (contract) {
          const post = await contract.methods.editContent(image,1).send({'from': account})
        }
      }
      catch (e){console.log (e)}
    }        
    reader.readAsDataURL(e.target.files[0])
 }    

  

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error: {error}</p>;
  if (!active) return <p>Connect</p>;

  return (
    <Paper>
      <Box p={2} display="flex" flexDirection="column">
        <Box>{item.description} </Box>
        
        <img width={100} height={100} src={item.content} />
        
      
      { (account) ? <Button onClick= {viewItem}>View</Button> : <>Connect your metamask to view</>}
      <input type="file" onChange={onfileupload}  />
      </Box>
    </Paper>
  );
};
