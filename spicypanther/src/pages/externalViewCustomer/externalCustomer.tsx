import * as React from "react"
import {
  Box,
  Typography,
  Button,
  makeStyles,
  CardMedia,
} from "@material-ui/core"


import { useQuery } from 'apollohooks'

import { useParams } from 'react-router-dom'

/*const useStyles = makeStyles({
  imageStyle: {
    width: "70%",
    height: "auto",
  },
})*/

const Link = (params:any) => { return (<React.Fragment/>) } 


export const ViewCustomer = () => {  
  

  //const { setTheme} = useTheme()
    
  let { _id }: any = useParams()

  const win = window as any
  if (win.customerID){
    _id = win.customerID
  }
  
  //const [customer, customerSet ] = React.useState ({} as any)    

  
    const {loading, data: customer} = useQuery ('customerPublic', {_id: _id, $:'_id firstName lastName displayName theme image childrenEvents {_id name }'} )
    
    /*, {onChange: (result: any, error: string)=>{  
      if ( error )  return displayNotification (error)

      
      setTheme ({
        image: result.image,
        styles: result.theme,
        layout: result.layout,
        displayName: result.displayName
      })
      
    }})*/  
  

  if (loading || customer==null || customer._id ==null ) return <React.Fragment/>  
  
  const events = customer.childrenEvents || []  

  return (
    <>
      <Box display='flex' flexDirection='row-reverse' px={2}>
        
      </Box>

      <Box display='flex' flexDirection='column' alignItems='center'>
        
        <CardMedia
          component='img'
          alt={`${customer.firstName} ${customer.lastName}`}
          height='140'
          image={customer.image}
          title={`${customer.firstName} ${customer.lastName}`}
        />

        <Box
          display='flex'
          flexDirection='row'
          justifyContent='space-between'
          p={2}>
          <Box mr={5}>
            <Typography variant='h5'>{customer.name}</Typography>
            <Typography variant='body2' color='textSecondary' component='p'>
              {customer.description}
            </Typography>
          </Box>
          <Box>
          </Box>
        </Box>
        
        

        
        {events.map ((event: any)=>{
          return (
            <Button color='primary' variant='contained'>
            
            <Link to ={`/view/${event._id}`}>
              <Typography>{event.name}</Typography>
            </Link>
            </Button>
            )            
        })}


        

      </Box>




    </>
  )
}
