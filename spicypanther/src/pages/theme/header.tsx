import { useWeb3React } from '@web3-react/core';
import { InjectedConnector } from '@web3-react/injected-connector';

import { Link } from 'react-router-dom';
import { useStyles } from 'ui';
import { Button, Grid, Typography } from '@material-ui/core';

const pageStyles = (params: any) => {
  const { theme, vw, vh } = params;
  return {
    h1: {
      fontSize: '50px',
    },
  };
};

const injected = new InjectedConnector({
  supportedChainIds: [137, 97],
});

export const Header = () => {
  const { active, account, activate } = useWeb3React();

  const { styles } = useStyles(pageStyles);

  const Connect = () => {
    const onclick = () => {

      activate(injected);
    };

    if (active && account) {
      const accountNumber = `${account.substring(0, 12)}...${account.slice(
        -8
      )}`;
      return <Typography>{accountNumber}</Typography>;
    }
    return (
      <>
        <Typography>Connect your MetaMask Wallet</Typography>
        <Button onClick={onclick}>Connect</Button>
      </>
    );
  };

  return (
    <Grid container>
      <Grid item xs={10}>
        <Typography variant="h6">Test Site</Typography>
        <Link to='/mediafollowing'>following</Link>
      </Grid>
      <Grid item xs={1}>
        <Connect />
      </Grid>
    </Grid>
  );
};
