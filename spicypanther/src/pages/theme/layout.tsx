import { Box, Modal as MuiModal } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import { useAlert, useStyles} from 'ui';

const pageStyles = (params: any) => {
  const { theme, vw, vh } = params;
  return {
    root: {},
    header: {},
    body: {
      overflow: 'auto',
    },
    alert :{
      top : 10,
      left : 10,
      position : 'absolute'
    }
  };
};

export const Layout = (params: any) => {
  const { body: Body, header: Header } = params;
  const { alertMessage } = useAlert()

  const { styles } = useStyles(pageStyles);
  

  return (
    <Box>
      <Box>
        <Header />
      </Box>
      <Box>
        <Body />
      </Box>
      <Box sx={styles.alert}>          
        {alertMessage && <Alert  severity="error">{alertMessage}</Alert>}
      </Box>
    </Box>
  );
};
