import {
	Box,
	Modal as MuiModal,	
} from '@material-ui/core'

import {useStyles} from 'ui'

const pageStyles =  {
    modal :{
        background : 'white',
        width : '50%',
        height: '50%',
        color: 'black',
        borderRadius: '5px'
    }
}

export const Modal = (params:any)=>{
    const {open, children} = params
    const {styles} = useStyles(pageStyles)
    return (
        <MuiModal open={open || true} style={{display:'flex',alignItems:'center',justifyContent:'center'}}>
            <Box sx={styles.modal}>                
                {children}
            </Box>
        </MuiModal>
    )
}