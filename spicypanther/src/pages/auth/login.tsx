import { useEffect, useState , useContext } from 'react'
import { Link, useNavigate } from 'react-router-dom'


import { Grid, Input, Avatar, Box, Divider, Paper, Typography, Button } from '@material-ui/core';
import { useQuery, useMutation } from 'apollohooks'
import { hashmap, useControlState } from 'utils'
import { useAlert, useStyles, useUser } from 'ui';
import { setCookie } from 'localstorage'


import { useApollo } from 'apollohooks'

const LoginController = (params: any)=>{
    const { username, password, updater, redirect, errorController: E, loadingController: L } = params
    const { login: loginApollo } = useApollo()
    const navigate = useNavigate()
    const { alert }= useAlert ()
    const { login: loginUI, userQuery} = useUser()

    const { loading, error, data } = useQuery ('loginUser', { request : userQuery, variables: { email : username, password: password}} );

    useEffect (()=>{        
        if (data && data.auth){
            loginApollo (data.auth)
            loginUI (data)
            setCookie ('auth', data.auth)
            
            navigate(redirect);
        }

        if ( error ){
            alert (error)
            setTimeout (()=>{ updater(0) }, 2000)                
        }

    },[data, error])    

    if (loading) return <L/>;
    if (error) return <E/>;
    return (<></>)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const Login = () => {         
    const [ username, usernameSet ] = useControlState('')
    const [ password, passwordSet ] = useControlState('')
    const [ status, statusSet]  = useState(0)

    const Error = ()=> <p>Error</p>
    const Loading = ()=> <p>Loading</p>

    if (status==1)
        return <LoginController loadingController={Loading} errorController={ Error } updater={statusSet} username={username} password={password} redirect='/dashboard' />
     
    
    return (
        <Box sx={{ flexDirection: 'column' }}>
            <Typography variant='h1'>Login</Typography>
            <Typography>Email Address</Typography>
            <Input id='email' value={username} onChange={usernameSet}></Input>
            <Typography>Password</Typography>
            <Input id='password' value= {password} onChange={passwordSet}></Input>
            <Button id='submit' onClick={()=>{ statusSet(1) }}>Login</Button>
      </Box>
     )
    
  }