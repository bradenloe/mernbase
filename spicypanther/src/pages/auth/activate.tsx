import { useEffect, useState , useContext } from 'react'
import { Link } from 'react-router-dom'
import { useNavigate } from "react-router-dom";

import { ApolloAuthContext, useQuery, useMutation } from 'apollohooks'


import { useParams } from "react-router-dom";

const ActivateController = (params: any)=>{
    const { activation, redirect, errorController: E, loadingController: L } = params    
    const navigate = useNavigate()

    const [ activateUser, { data, loading, error }] = useMutation ('activateUser', { 
       request : '_id',  
       variables: { 
          activation : activation
         }  
      });

    useEffect (()=>{        
        (async ()=>{
            await activateUser ({variables: { activation: activation}})
        })() 
    },[activation])   
    
    useEffect (()=>{
        if (data && data._id)
            navigate(redirect)
    },[data, error])

    if (loading) return <L/>;
    if (error) return <E/>;
    return (<></>)
}


export const Activate = ()=>{
   const Error = ()=> <p>Error</p>
   const Loading = ()=> <p>Loading</p>

   const { activation }: any = useParams()    

   return <ActivateController loadingController={Loading} errorController={ Error } activation={activation} redirect='/dashboard' />
}
