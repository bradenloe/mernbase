import { Login } from './login'
import { Register } from './register'

export const Auth = {
    Login : Login,
    Register : Register
}