import { useEffect, useState , useContext } from 'react'
import { Grid, Input, Avatar, Box, Divider, Paper, Typography, Button } from '@material-ui/core';
import { useQuery, useMutation } from 'apollohooks'
import { hashmap, useControlState } from 'utils'
import { Link } from 'react-router-dom'
import { useNavigate } from "react-router-dom";


const RegisterController = (params: any)=>{
    const { username, password, updater, redirect, errorController: E, loadingController: L } = params    
    const navigate = useNavigate()

    const [ registerUser, { data, loading, error }] = useMutation ('registerUser', { 
        request : '_id',         
    })

    useEffect (()=>{        
        (async ()=>{
            await registerUser ({
                variables: {
                create: { 
                    email : username, 
                    password: password
                }
            }})
        })() 

    },[])   
    
    useEffect (()=>{        
        if (data && data._id)
            navigate(redirect)
        if (error)
            console.log (`ERROR: ${error}`)
    },[data, error])

    if (loading) return <L/>;
    if (error) return <E/>;
    return (<></>)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const Register = () => {         
    const [ username, usernameSet ] = useControlState('')
    const [ password, passwordSet ] = useControlState('')
    const [ status, statusSet]  = useState(0)

    const Error = ()=> <p>Error</p>
    const Loading = ()=> <p>Loading</p>

    if (status==1)
        return <RegisterController loadingController={Loading} errorController={ Error } updater={statusSet} username={username} password={password} redirect='/login' />
     
    
    return (
      <Box >
        <Typography variant='h1'>Register</Typography>
        <Input id='email' onChange={(e)=>{usernameSet(e.target.value)}}></Input>
        <Typography>Password</Typography>
        <Input id='password' onChange={(e)=>{passwordSet(e.target.value)}}></Input>
        <Button id='submit' onClick={()=>{ statusSet(1) }}>Login</Button>
      </Box>
     )
    
  }