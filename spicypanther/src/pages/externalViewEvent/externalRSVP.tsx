import * as React from 'react'


import { useMutation } from 'apollohooks'
import { useParams } from "react-router-dom";
import { Box } from '@material-ui/core'



export const RSVP = () =>{
    const { rsvp }: any = useParams()
    

    const [status, statusSet] = React.useState('loading')

    const [rsvpEvent, {loading, error}] =  useMutation('rsvp', { request : 'status message'})

    

    React.useEffect (
        ()=>{
            rsvpEvent ({
                variables: {
                    rsvp: rsvp
                }, 
                onCompleted: (result:any)=>{
                    statusSet (result.message)
                }
            })    
        },[])        

    
    return (
        <Box>
            {status}
        </Box>
    )
    

}