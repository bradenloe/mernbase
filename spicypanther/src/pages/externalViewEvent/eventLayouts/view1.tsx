import * as React from 'react'
import {
	Box,
	Paper,
	Typography,
	IconButton,
	makeStyles,
	CardMedia,
	CardContent,
	Button,
	Grid,
	Card,
	CardActions,
	Fab,
} from '@material-ui/core'


import {useStyles} from 'ui'
//import {_Tab as Tab, _Tabs as Tabs} from '../../lib/mui/tabs'


//import {ViewPosts} from '../../imports'




//import {muiFormatDate} from '../../lib/utils'
//import {GoogleMapsView} from '../../lib/mui/googleMapsView'

const muiFormatDate = (param:any)=> {return ('now')}
const GoogleMapsView = (params:any)=>{
	return <React.Fragment/>
}
const ViewPosts= GoogleMapsView

const pageStyles = {
	imageStyle: {
		width: '70%',
		height: 'auto',
	},
	root: {
		maxWidth: 380,
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
	container: {
		display: 'flex',
		maxWidth: '100vw',
		alignItems: 'center',
		marginTop: '1rem',
		marginLeft: '1rem',
	},
	messengerIcon: {
		right: 20,
		position: 'fixed',
		bottom: 10,
	},
	messengerBox: {
		right: 80,
		position: 'fixed',
		bottom: 0,
		height: '380px',
		width: '300px',
		borderRadius: 0,
	},
	box: {
		position: 'relative',
	},
	overlay: {
		position: 'absolute',
		top: '0px',
		right: '20px',
		height: '300px',
		color: 'black',
		opacity: '0.9',
		backgroundColor: 'white',
	},
}

export const ViewEvent = (params: any) => {		
	const {styles} = useStyles (pageStyles)
	const classes = styles
	
	const {onTickets, event} = params



	if (!event) return <React.Fragment />

	const ticketTypes = event.childrenTicketTypes || []
	

	return (
		<>
			<Box
				display='flex'
				className={classes.box}
				flexDirection='column'
				alignItems='center'>
				<CardMedia
					component='img'
					alt={event.name}
					height='300'
					image={event.image}
					title={event.name}
				/>
				<Box
					display='flex'
					flexDirection='column'
					alignItems='center'
					justifyContent='space-between'
					className={classes.overlay}
					p={2}>
					<Box display='flex' flexDirection='column' alignItems='center'>
						<Typography variant='h5'>{event.name}</Typography>
						<Typography variant='body2' color='textSecondary' component='p'>
							{event.description}
						</Typography>
					</Box>
					<Box>
						<Typography variant='body2' color='textSecondary' component='p'>
							Starts: {muiFormatDate(event.startTime)}
						</Typography>
						<Typography variant='body2' color='textSecondary' component='p'>
							Ends: {muiFormatDate(event.endTime)}
						</Typography>
					</Box>
					<Box mx={1}>
						<Button
							data-testid='change-password-button'
							variant='contained'
							color='primary'
							onClick={onTickets}>
							Buy Tickets
						</Button>
					</Box>
				</Box>

				<Grid container spacing={1} justifyContent='center'>
					<Grid item xs={12} md={6}>
						<Box mt={2} display='flex' flexDirection='column'>
							<ViewPosts event={event} />
						</Box>
					</Grid>

					<Grid item xs={12} md={3}>
						<Box mt={2} display='flex' flexDirection='column'>
							<Paper>
								<Box p={1}>
									<GoogleMapsView value={event.address} />
								</Box>
							</Paper>
						</Box>
						{/* <Box mt={1} display='flex' flexDirection='column'>
							<Paper>
								<Box p={2}>
									<Typography>
										maybe something like rooms/spakers/faq and other requried
										tabs
									</Typography>
								</Box>
							</Paper>
						</Box> */}
					</Grid>
				</Grid>
				
			</Box>
		</>
	)
}
