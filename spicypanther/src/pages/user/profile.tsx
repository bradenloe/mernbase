import * as React from 'react'
import { Grid, Input, Avatar, Box, Divider, Paper, Typography, Button } from '@material-ui/core';
import { useQuery, useMutation } from 'apollohooks'
import { hashmap, useControlState } from 'utils'
import { useAlert, useStyles, LabeledInput } from 'ui';
import { useStateHash} from 'usestateobject'

const ColorPicker = ()=>{ return <React.Fragment></React.Fragment> }

const ImageUploader = ()=> {
	return (<React.Fragment/>)
}



export const Profile = ()=>{
	const query ='_id image firstName lastName company displayName address city state zip theme'
	const { loading, error, data: customer, updateQuery } = useQuery('customer', { $: query })
	
	const [ updateCustomer ] = useMutation ('updateCustomer', { request: query, onCompleted : (data: any, error:any)=>{
		updateQuery ( data )
	}})

	const {register, string, changes: customerChanges} = useStateHash({
		...customer,		
	},[customer])

	if (error)
		return (<Typography>{error.message}</Typography>)

	const save = async ()=>{
		updateCustomer ({variables: {updates: customerChanges()} })		
	}
	const changepassword = ()=>{}
	const manageEmailTemplates = ()=>{}

	return (
			<Box
				display='flex'
				flexDirection='column'
				alignItems='center'
				mt={5}
				justifyContent='center'
				maxHeight='100'>
				<ImageUploader {...register('image')} label='Event Cover'/>
	
				<Box display='flex' flexDirection='row' mt={1}>
					<Box mx={1}>
						<LabeledInput {...string('firstName')} label='First Name*' />
					</Box>
					<Box mx={1}>
						<LabeledInput
							{...string('lastName')}
							data-testid='profile-input-lastname'
							label='Last Name*'
						/>
					</Box>
				</Box>
				<Box display='flex' flexDirection='row' mt={1}>
					<Box mx={1}>
						<LabeledInput
							{...string('displayName')}
							data-testid='profile-input-lastname'
							label='Display Name*'
						/>
					</Box>
					<Box mx={1}>
						<LabeledInput
							{...string('company')}
							data-testid='profile-input-company'
							label='Company'
						/>
					</Box>
				</Box>
				<Box display='flex' flexDirection='row' mt={1}>
					<Box mx={1}>
						<LabeledInput
							{...string('city')}
							data-testid='profile-input-city'
							label='City'
						/>
					</Box>
					<Box mx={1}>
						<LabeledInput
							{...string('state')}
							data-testid='profile-input-state'
							label='State'
						/>
					</Box>
				</Box>
				<Box display='flex' flexDirection='row' mt={1}>
					<Box mx={1}>
						<LabeledInput
							{...string('zip')}
							data-testid='profile-input-zipcode'
							label='Zip Code'
						/>
					</Box>
				</Box>
				<Box mt={1}>
					<ColorPicker
						defaultValue='Primary Theme Color'
							{...register('pallettePrimaryMain')}					
						/>
						</Box>
				<Box my={1}>
					<Button
						data-testid='update-profile-button'
						variant='contained'
						color='primary'
						onClick={save}>
						Update
					</Button>
				</Box>
				<Box display='flex' flexDirection='row'>
					<Box mx={1}>
						<Button
							data-testid='change-password-button'
							variant='contained'
							color='primary'
							onClick={changepassword}>
							Change Password
						</Button>
					</Box>
					<Box mx={1}>
						<Button
							data-testid='email-templates-button'
							variant='contained'
							color='primary'
							onClick={manageEmailTemplates}>
							Email Templates
						</Button>
					</Box>
				</Box>
			</Box>
		)
}
/*import {useState, Fragment, useEffect} from 'react'
import {Input, Spinner, Button, Box, Typography} from '@material-ui/core'
import {Div, Input, _LabledInput as LabledInput, Spinner} from '../../lib/ui'
import {ImageUploader} from '../../lib/mui/imageUploader'
import {useUI} from '../../lib/hook'
import {useQuery, mutation} from '../../lib/ql'
import ColorPicker from 'material-ui-color-picker'

import { useStateHash} from 'usestateobject'

//import {ChangePassword} from '../../imports'
//import {ManageEmailTemplates} from '../popups/manageEmailTemplates'

export const Profile = () => {
	

	const {
		result,
		mutate,
		refetch,
	} = useQuery('customer', {
		$: '_id image firstName lastName company displayName address city state zip theme',
	})

	const {theme, ...customer} = result || {} 
	const { pallettePrimaryMain } =JSON.parse (theme || '{"pallettlePrimaryMain":"#5F2EEA"}')

	const {register, changes: customerChanges} = useStateHash({
		...customer,
		pallettePrimaryMain: pallettePrimaryMain
	},[result])

	const manageEmailTemplates = () => {
		displayPopup(ManageEmailTemplates)
	}

	const save = () => {
		const {pallettePrimaryMain, ...updates} = customerChanges()

		if (pallettePrimaryMain){
			updates.theme= JSON.stringify ({
				pallettePrimaryMain: pallettePrimaryMain
			})
		}
		mutation('updateCustomer', {updates: updates}, mutate)
	}

	const changepassword = () => {
		displayPopup(ChangePassword)
	}


	if (!customer) return <Spinner />

	return (
		<Box
			display='flex'
			flexDirection='column'
			alignItems='center'
			mt={5}
			justifyContent='center'
			maxHeight='100'>
			<ImageUploader {...register('image')} label='Event Cover'/>

			<Box display='flex' flexDirection='row' mt={1}>
				<Box mx={1}>
					<LabledInput {...register('firstName')} label='First Name*' />
				</Box>
				<Box mx={1}>
					<LabledInput
						{...register('lastName')}
						data-testid='profile-input-lastname'
						label='Last Name*'
					/>
				</Box>
			</Box>
			<Box display='flex' flexDirection='row' mt={1}>
				<Box mx={1}>
					<LabledInput
						{...register('displayName')}
						data-testid='profile-input-lastname'
						label='Display Name*'
					/>
				</Box>
				<Box mx={1}>
					<LabledInput
						{...register('company')}
						data-testid='profile-input-company'
						label='Company'
					/>
				</Box>
			</Box>
			<Box display='flex' flexDirection='row' mt={1}>
				<Box mx={1}>
					<LabledInput
						{...register('city')}
						data-testid='profile-input-city'
						label='City'
					/>
				</Box>
				<Box mx={1}>
					<LabledInput
						{...register('state')}
						data-testid='profile-input-state'
						label='State'
					/>
				</Box>
			</Box>
			<Box display='flex' flexDirection='row' mt={1}>
				<Box mx={1}>
					<LabledInput
						{...register('zip')}
						data-testid='profile-input-zipcode'
						label='Zip Code'
					/>
				</Box>
			</Box>
			<Box mt={1}>
				<ColorPicker
					defaultValue='Primary Theme Color'
						{...register('pallettePrimaryMain')}					
					/>
					</Box>
			<Box my={1}>
				<Button
					data-testid='update-profile-button'
					variant='contained'
					color='primary'
					onClick={save}>
					Update
				</Button>
			</Box>
			<Box display='flex' flexDirection='row'>
				<Box mx={1}>
					<Button
						data-testid='change-password-button'
						variant='contained'
						color='primary'
						onClick={changepassword}>
						Change Password
					</Button>
				</Box>
				<Box mx={1}>
					<Button
						data-testid='email-templates-button'
						variant='contained'
						color='primary'
						onClick={manageEmailTemplates}>
						Email Templates
					</Button>
				</Box>
			</Box>
		</Box>
	)
}
*/