import { ThemeOptions } from '@material-ui/core';

export const defaultTheme: ThemeOptions = {
  palette: {
    error: {
      light: '#e57373',
      main: '#f50057',
      dark: '#c51162',
      contrastText: '#fff',
    },
    warning: {
      light: '#ffb74d',
      main: '#ff9800',
      dark: '#f57c00',
      contrastText: 'rgba(0, 0, 0, 0.87)',
    },
  },
  typography: {
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Arial',
      'sans-serif',
    ].join(','),
    subtitle1: {
      fontWeight: 'bolder',
    },
  },
  overrides: {
    MuiAppBar: {
      root: {
        'box-shadow': 'none',
      },
    },
    MuiPaper: {
      root: {
        'box-shadow': 'none',
      },
    },
    MuiCard: {
      root: {
        'box-shadow': 'none',
      },
    },
    MuiTableCell: {
      sizeSmall: {
        'padding-top': '10px',
        'padding-bottom': '10px',
      },
    },
  },
};

export const ThemeLight: ThemeOptions = {
  ...defaultTheme,
  palette: {
    type: 'light',
    primary: {
      main: '#427fba',
    },
    secondary: {
      main: '#3B365F',
      light: '#ede7f6',
    },
    background: {
      default: '#F3F3F4',
      paper: '#ffffff',
    },
    ...defaultTheme.palette,
  },
};

export const ThemeDark: ThemeOptions = {
  ...defaultTheme,
  palette: {
    type: 'dark',
    primary: {
      main: '#427fba',
    },
    secondary: {
      main: '#3B365F',
      light: '#a5a1ac',
    },
    background: {
      default: '#1E272E',
      paper: '#2b343b',
    },
    text: {
      primary: 'rgba(255, 255, 255, 0.87)',
      secondary: 'rgba(255, 255, 255, 0.6)',
    },
    ...defaultTheme.palette,
  },
};
