// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

/**
 * @title Storage
 * @dev Store & retrieve value in a variable
 */
contract BlackJack {    
    struct History {
        address Address;
        string Description;
        uint cardFace;
        uint cardSuite;
    }  

    struct PlayerData{
        uint hold;
        uint sum;
        uint ace;
    }

    History[] public history;
    uint public ended;
    uint public moveCount;


    mapping ( uint => uint ) private cardState;
    
    uint private playerCount;
    uint private draw;
    
    mapping (address => PlayerData) private game;

    constructor (){
        playerCount=0;
        ended=0;
        draw=0;        
    }
    
    function payout (address _address) internal{

    }

    function checkWinCondtions() internal {        

        /*for (uint x= draw; x< moveCount; x++){
            address _address= history[x].Address;        

            uint value = history[x].cardFace;
            if (value > 10)
                value = 10;
            if (value == 1){
                value = 11;
                game[_address].ace++;
            }
            game[_address].sum++;
        }

        
        if ( game[history[1].Address].sum > 21 && game[history[0].Address].sum < 22){
            if (game[history[1].Address].sum-11 < 22 && game[history[1].Address].ace ==1)
                return;

            history.push (History({
                Address : history[0].Address,
                Description:"WIN", 
                cardFace: 0, 
                cardSuite: 0 
            }));
            payout(history[0].Address); return;
        }

        if ( game[history[0].Address].sum > 21 && game[history[1].Address].sum < 22){
            if (game[history[0].Address].sum-11 < 22 && game[history[1].Address].ace ==1)
                return;

            history.push (History({
                Address : history[1].Address,
                Description:"WIN", 
                cardFace: 0, 
                cardSuite: 0 
            }));
            payout(history[1].Address); return;
        }

        if ( game[history[0].Address].sum == 21 && game[history[1].Address].sum == 22){
            draw=moveCount+1;

            history.push (History({
                Address : address(this),
                Description:"DRAW", 
                cardFace: 0, 
                cardSuite: 0 
            }));
            
            return;
        }        

        if ( game[history[0].Address].sum < game[history[1].Address].sum && game[history[0].Address].hold==1){
            history.push (History({
                Address : history[1].Address,
                Description:"WIN", 
                cardFace: 0, 
                cardSuite: 0 
            }));
            payout(history[1].Address); return;
        }


        if ( game[history[1].Address].sum < game[history[0].Address].sum && game[history[1].Address].hold==1){
            history.push (History({
                Address : history[0].Address,
                Description:"WIN", 
                cardFace: 0, 
                cardSuite: 0 
            }));
            payout(history[0].Address); return;
        } */     

        
    }
    
    /*function verifyPlayer (address _address) view internal {
        require (ended == 0, "Game has ended");
        if (playerCount < 2 ){
            return;       
        }        

        if ( game[history[0].Address].hold ==1 ){
            require (history[1].Address == _address,"no");
            return;
        }

        if ( game[history[1].Address].hold  ==1 ){
            require (history[0].Address == _address,"no");
            return;
        }
        
        require (history[moveCount % 2].Address == _address,"no");        
    }*/

    function Play() external {
        //verifyPlayer(msg.sender);
        
       
        uint suite=2;
        uint face=2;
        
        uint card = (suite * 20)+ face;
        cardState[card]=1;

        history.push ( 
            History({
                Address : msg.sender, 
                Description:"HIT", 
                cardFace: face, 
                cardSuite: suite 
            }
        ));
        moveCount++;
        checkWinCondtions();
    }        

    function Hold() external {
        //verifyPlayer(msg.sender);
        game[msg.sender].hold++;
        history.push ( 
            History({
                Address : msg.sender, 
                Description:"HOLD", 
                cardFace:0, 
                cardSuite:0 
            }
        ));
        moveCount++;
        checkWinCondtions();
    }
    
    
}