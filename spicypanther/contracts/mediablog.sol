pragma solidity >=0.7.0 <0.9.0;

/**
 * @title Owner
 * @dev Set & change owner
 */
contract MediaPost {    
    struct Post {        
        uint version;
        string content;     
        uint time;    
    }  

    struct Blog{
        string title;
        string description;
        string image;
        uint postCount;
        uint stickyCount;
    }

    mapping ( uint => Post ) public posts;
    mapping ( uint => uint ) public sticky;

    Blog public blog;
    
    address public owner;    
       
    // modifier to check if caller is owner
    modifier isOwner() {    
        require(msg.sender == owner, "Caller is not owner");
        _;
    }
    
    
    constructor( string memory title, string memory description, string memory image) {
        owner = msg.sender; // 'msg.sender' is sender of current call, contract deployer for a constructor
        blog = Blog ({
            title : title,
            description : description,
            image : image,
            postCount: 0,
            stickyCount: 0
        });
    }

    function updateBlog ( string calldata title, string calldata description, string calldata image) external isOwner {
        blog.title = title;
        blog.description = description;
        blog.image = image;
    }
    
    function createPost(string calldata postBody) external isOwner {
        posts[blog.postCount]=Post({
            version : 1,
            content: postBody,
            time: block.timestamp
        }); 
        blog.postCount++;
    }

    function editPost(string calldata postBody, uint post) external isOwner {
        posts[post]=Post({
            version : 1,
            content: postBody,
            time: block.timestamp
        });         
    }

    


    // SPDX-License-Identifier: GPL-3.0

    
}