# AdminSettings
type AdminSettings {
  _id: ID
  _createdTime: DateTime
  _updatedTime: DateTime
  sendLookMessage: String
  recruiterVerifyMessage: String
  recruiterRequestMessage: String
}

input AdminSettingsInput {
  sendLookMessage: String
  recruiterVerifyMessage: String
  recruiterRequestMessage: String
}

# CustomerPublic
type CustomerPublic {
  _id: ID
  _createdTime: DateTime
  _updatedTime: DateTime
  firstName: String
  lastName: String
  displayName: String
  image: String
  company: String
  address: String
  city: String
  state: String
  zip: String
  theme: String
  childrenEvents(_limit: Int, _skip: Int, searchLastName: String): [EventsPublic]
}

input CustomerRegistrationInput {
  email: String
  password: String
  provider: String
}

# Customers
type Customers {
  _id: ID
  _createdTime: DateTime
  _updatedTime: DateTime
  unsubscribed: Boolean
  banned: Boolean
  activated: Boolean
  accountStatus: Int
  administrator: Boolean
  auth: String
  email: String
  encryptedID: String
  password: String
  displayName: String
  firstName: String
  lastName: String
  image: String
  company: String
  domain: String
  address: String
  city: String
  state: String
  zip: String
  theme: String
  layout: String
  childrenEvents(_limit: Int, _skip: Int, searchLastName: String): [Events]
  childrenGuestCommunications: [GuestCommunications]
  childrenEmailTemplates: [EmailTemplates]
  childrenEventPosts: [EventPosts]
  childrenMyTickets(searchEvent: String): [EventGuests]
}

input Customers_AdministratorViewInput {
  email: String
  serviceNewsLetter: Int
  serviceEvents: Int
  serviceStore: Int
  banned: Boolean
  unsubscribed: Boolean
}

input CustomersInput {
  activated: Boolean
  accountStatus: Int
  auth: String
  password: String
  displayName: String
  firstName: String
  lastName: String
  image: String
  company: String
  address: String
  city: String
  state: String
  zip: String
  theme: String
  layout: String
}

# A date-time string at UTC, such as 2007-12-03T10:15:30Z, compliant with the
# `date-time` format outlined in section 5.6 of the RFC 3339 profile of the ISO
# 8601 standard for representation of dates and times using the Gregorian calendar.
scalar DateTime

# EmailTemplates
type EmailTemplates {
  _id: ID
  _createdTime: DateTime
  _updatedTime: DateTime
  message: String
  json: String
  name: String
  subject: String
  sender: String
}

input EmailTemplatesInput {
  message: String
  json: String
  name: String
  subject: String
  sender: String
}

# EventGuests
type EventGuests {
  _id: ID
  _createdTime: DateTime
  _updatedTime: DateTime
  notes: String
  email: String
  firstName: String
  image: String
  lastName: String
  displayName: String
  status: Int
  telephone: String
  typeDescription: String
  additionalGuests: Int
  deliveryStatus: Int
  amountPaid: Int
  privacyPolicyAccepted: String
  parentOwner: CustomerPublic
  type: TicketTypes
}

input EventGuestsInput {
  notes: String
  email: String
  firstName: String
  image: String
  lastName: String
  displayName: String
  status: Int
  telephone: String
  type: ID
  typeDescription: String
  additionalGuests: Int
  privacyPolicyAccepted: String
}

# EventGuestsPublic
type EventGuestsPublic {
  _id: ID
  _createdTime: DateTime
  _updatedTime: DateTime
  email: String
  firstName: String
  image: String
  lastName: String
  status: Int
  displayName: String
  telephone: String
  parentEvent: EventsPublic
}

input EventGuestsPublicInput {
  email: String
  firstName: String
  image: String
  lastName: String
  status: Int
  displayName: String
  telephone: String
}

# EventGuestsSubscription
type EventGuestsSubscription {
  _id: ID
  _createdTime: DateTime
  _updatedTime: DateTime
  _action: String
  notes: String
  email: String
  firstName: String
  image: String
  lastName: String
  status: Int
  telephone: String
  additionalGuests: Int
  deliveryStatus: Int
  typeDescription: String
  privacyPolicyAccepted: String
}

# EventPosts
type EventPosts {
  _id: ID
  _createdTime: DateTime
  _updatedTime: DateTime
  message: String
  name: String
}

input EventPostsInput {
  message: String
  name: String
}

# EventPostsPublic
type EventPostsPublic {
  _id: ID
  _createdTime: DateTime
  _updatedTime: DateTime
  name: String
  message: String
  parentEventPost: ID
  authorName: String
  authorID: String
}

# EventPostsSubscription
type EventPostsSubscription {
  _id: ID
  _createdTime: DateTime
  _updatedTime: DateTime
  _action: String
  message: String
  name: String
  parentEventPost: ID
  authorName: String
  authorID: String
}

# Events
type Events {
  _id: ID
  _createdTime: DateTime
  _updatedTime: DateTime
  _countChildrenEventGuests: Int
  address: String
  description: String
  endTime: String
  image: String
  name: String
  notes: String
  startTime: String
  published: Boolean
  restricted: Boolean
  emailRSVP: String
  emailRSVPDecline: String
  emailTicket: String
  privacyPolicy: String
  privacyPolicyRequired: Boolean
  theme: String
  layout: String
  childrenEventGuests(_limit: Int, _skip: Int, searchLastName: String): [EventGuests]
  childrenGuestCommunications: [GuestCommunications]
  childrenGuestCommunicationDetails: [GuestCommunicationDetails]
  childrenTicketTypes: [TicketTypes]
  childrenEventPosts: [EventPosts]
}

input EventsInput {
  address: String
  description: String
  endTime: String
  image: String
  name: String
  notes: String
  startTime: String
  published: Boolean
  restricted: Boolean
  emailRSVP: String
  emailRSVPDecline: String
  emailTicket: String
  privacyPolicy: String
  privacyPolicyRequired: Boolean
  theme: String
  layout: String
}

# EventsPublic
type EventsPublic {
  _id: ID
  _createdTime: DateTime
  _updatedTime: DateTime
  address: String
  description: String
  endTime: String
  image: String
  name: String
  notes: String
  startTime: String
  privacyPolicy: String
  privacyPolicyRequired: Boolean
  primaryColor: String
  published: Boolean
  layout: String
  theme: String
  childrenTicketTypes: [TicketTypes]
  childrenEventPosts: [EventPostsPublic]
  parentCustomer: CustomerPublic
}

input EventTicketInput {
  event: ID
  lastName: String
  firstName: String
  email: String
  type: ID
  location: Int
  privacyPolicyAccepted: String
}

# GuestCommunicationDetails
type GuestCommunicationDetails {
  _id: ID
  _createdTime: DateTime
  _updatedTime: DateTime
  deliveryDetails: String
  deliveryStatus: Int
  deliveryMessage: String
  recipientEmail: String
  recipientName: String
  parentEventGuest: ID
  childrenGuestCommunicationDetailsActivities: [GuestCommunicationDetailsActivities]
}

# GuestCommunicationDetailsActivities
type GuestCommunicationDetailsActivities {
  _id: ID
  _createdTime: DateTime
  _updatedTime: DateTime
  activity: Int
}

# GuestCommunications
type GuestCommunications {
  _id: ID
  _createdTime: DateTime
  _updatedTime: DateTime
  message: String
  senderEmail: String
  senderName: String
  subject: String
  childrenGuestCommunicationDetails: [GuestCommunicationDetails]
}

input GuestCommunicationsInput {
  message: String
  senderEmail: String
  senderName: String
  subject: String
}

type Mutation {
  activateUser(activation: String!): Customers
  registerUser(create: CustomerRegistrationInput!, provider: String): Customers
  resetPassword(email: String!, provider: String): Response
  updateCustomer(updates: CustomersInput): Customers
  createEvent(create: EventsInput): Events
  updateEvent(_id: ID!, updates: EventsInput): Events
  deleteEvent(_id: ID!): Events
  createEventGuest(_id: ID!, create: EventGuestsInput): EventGuests
  createEventGuests(_id: ID!, create: [EventGuestsInput]): [EventGuests]
  deleteEventGuest(_id: ID!): EventGuests
  updateEventGuest(_id: ID!, updates: EventGuestsInput): EventGuests
  updateEventGuestPublic(_id: ID!, updates: EventGuestsPublicInput): EventGuestsPublic
  rsvp(rsvp: String!): Response
  sendEventInvitation(parentEvent: ID!, guests: [ID], message: GuestCommunicationsInput): GuestCommunications
  createEmailTemplate(create: EmailTemplatesInput!): EmailTemplates
  updateEmailTemplate(_id: ID!, updates: EmailTemplatesInput!): EmailTemplates
  deleteEmailTemplate(_id: ID!): EmailTemplates
  purchaseTicket(event: ID!, create: EventTicketInput): EventGuests
  purchaseTickets(event: ID!, create: [EventTicketInput]): EventGuests
  createTicketType(event: ID!, create: TicketTypesInput): TicketTypes
  updateTicketType(_id: ID!, updates: TicketTypesInput): TicketTypes
  deleteTicketType(_id: ID!): TicketTypes
  createEventPost(guest: ID, parentPost: ID, event: ID!, create: EventPostsInput): EventPosts
  updateEventPost(_id: ID!, updates: EventPostsInput): EventPosts
  deleteEventPost(_id: ID!): EventPosts
}

type Query {
  Admin_fetchSettings: AdminSettings
  Admin_updateSettings(updates: AdminSettingsInput!): AdminSettings
  Admin_ActivateCustomer(_id: ID!): Customers
  Admin_ListCustomers: [Customers]
  Admin_ListCustomerDetails(_id: ID!): Customers
  Admin_UpdateCustomer(_id: ID!, updates: Customers_AdministratorViewInput!): Customers
  loginUser(email: String, password: String, provider: String, oauth: String, oauthProvider: String): Customers
  loginWithToken(token: String!): Customers
  customer(_id: ID): Customers
  customerPublic(_id: ID, domain: String): CustomerPublic
  event(_id: ID): Events
  eventPublic(_id: ID): EventsPublic
  eventGuest(_id: ID!): EventGuests
  eventGuestPublic(_id: String!): EventGuestsPublic
  ticketType(_id: ID!): TicketTypes
  eventPost(_id: ID!): EventPostsPublic
}

# Response
type Response {
  _id: ID
  _createdTime: DateTime
  _updatedTime: DateTime
  status: Int
  message: String
}

type Subscription {
  subscribeEventGuests: EventGuestsSubscription
  subscribeEventPosts(event: ID!): EventPostsSubscription
}

# TicketTypes
type TicketTypes {
  _id: ID
  _createdTime: DateTime
  _updatedTime: DateTime
  name: String
  type: Int
  description: String
  price: Int
  quantityTotal: Int
  quantityAvailable: Int
}

input TicketTypesInput {
  name: String
  type: Int
  description: String
  price: Int
  quantityTotal: Int
  quantityAvailable: Int
}
