const userAdministrator = 2

export const users = {
    const : {
        userAdministrator  : userAdministrator
    },
    types: {
        Users_AdministratorView: {
            _$table :'users',        
            email: { type : 'string'},
            serviceNewsLetter: {type : 'int'},
            serviceEvents: {type : 'int'},
            serviceStore: {type : 'int'},            
            banned: {type : 'boolean'},
            unsubscribed: {type : 'boolean'}
        },        
        Users : {
            _$table : 'users',
            
            _$children : {
                //childrenMyTickets: {type: 'EventGuests', parameters : { searchEvent: 'string' }}
            },            

            unsubscribed : {type: 'boolean', readOnly: true, default: false},
            banned:         {type : 'boolean', readOnly: true, default: false},
            activated:      { type: 'boolean', default: false },
            accountStatus:  { type: 'int',     default: 0 },
            administrator:  { type: 'boolean', default: false , readOnly : true},
            auth :          { type: 'string',  default: null},                                                
            email :         { type: 'string',   default: '', readOnly : true},
            encryptedID:    { type: 'string', default: '', readOnly: true},                        
            password:       { type: 'string',   default: ''},
                      
            ///////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////            
            displayName :     { type: 'string',   default: ''},
            firstName :     { type: 'string',   default: ''},
            lastName :      { type: 'string',   default: ''},
            image :         { type: 'string',   default: ''},
            company :         { type: 'string',   default: ''},

            domain : { type: 'string',   default: '', readOnly: true},

            address :         { type: 'string',   default: ''},
            city :         { type: 'string',   default: ''},
            state :         { type: 'string',   default: ''},
            zip :         { type: 'string',   default: ''},

            theme:    { type: 'string',   default: ''},
            layout:    { type: 'string',   default: ''},
        },

        UserRegistration : {
            _$table :'users',
            email : { type: 'string',   default: ''},
            password:    { type: 'string',   default: ''},
            provider:   { type: 'string',   default: null}
        },            
        
 

        
    },

    mutations:{
        activateUser: { authenticated: false, returnType :'Users', parameters: {  activation : 'String!'} },
        registerUser: { authenticated: false,  returnType :'Users', parameters: {  create : 'UserRegistrationInput!' , provider: 'string'} },
        resetPassword: { authenticated: false,  returnType :'Response', parameters: {  email : 'String!', provider: 'string'} },

        updateUser  : { returnType: 'Users' , parameters: { updates : 'UsersInput'}, },
    },

    queries:{
        
        loginUser: { authenticated: false,  returnType :'Users', parameters: {  email : 'String', password: 'String', provider: 'String', oauth: 'String', oauthProvider :'String'} },
        loginWithToken: { authenticated: false,  returnType :'Users', parameters: {  token : 'String!' }},
        
        user  : { parameters: { _id : 'ID'  },returnType: 'Users'  },
    }

}