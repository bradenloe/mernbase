import { db, unauthenticated, ObjectId} from '../lib/mongo'

import { graphqlFunctions as users } from './users'


const sortbyIDArray = (unsorted: any, searchIDs: any) =>  {
  const sorted = []


  for (let x = 0; x  < searchIDs.length; x++)
            for (let y = 0; y  < unsorted.length; y++){
              
              if ( searchIDs[x].toString() == unsorted[y]._id.toString() ){                
                sorted.push (unsorted[y])
              }
            }

          return sorted
}

const mksort = (_sort : any)=>{
  if (!_sort) return null

  const sort= {} as any
  const [sortName, sortDirection] = (_sort[0] == '-') ? [_sort.substring(1), -1] : [_sort, 1]
  sort[sortName] = sortDirection
  return sort
}



export const graphqlFunctions = {       
    
  ...users,
        
    __childQuery: async (params: any) => {                
        const { _id, args, search, table, root} = params 
        const {_sort, _skip, _limit} = args        
        
                
        if (!Array.isArray( _id )){                  
          const waited = await db.collection(unauthenticated, table).findOneByID(_id)
          return waited || {}
        }

        
        const sort = mksort (_sort)
        const result = await db.collection(unauthenticated, table).findManyByUnsafeSearch( search , _skip, _limit, sort )
        return result || []
      }
}


    