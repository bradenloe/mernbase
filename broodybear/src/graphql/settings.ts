import { unsafe, _db as db, ObjectId, unauthenticated } from '../lib/mongo'
import { encryptString, decryptString } from '../lib/authenticationTools'
import { SendUnformatedMessage } from '../lib/mailTools'
import {  emailFromRecruiter , emailFromSeeker, emailVerifyRecruiter} from '../settings/settings'
import { templateLook, templateRecruiter, templateRecruiterVerify} from '../lib/templateTools'


export const graphqlFunctions = {
    Admin_fetchSettings : () =>{
        return db.collection(unauthenticated, 'AdminSettings').findOneByUnsafeSearch ({ __id : 'settings'})
    },

    Admin_updateSettings : async (params: any)=>{
        const updates=params.args.updates

        const settings = await db.collection(unauthenticated, 'AdminSettings').updateOneByUnsafeSearch ({ __id : 'settings'}, {$set : updates})
        if (settings == null)
            return db.collection(unauthenticated, 'AdminSettings').insertOne ({...updates, __id : 'settings' })
        return settings
    },   

}
