import { db, ObjectId, unauthenticated } from '../lib/mongo'


import { activateionMessage, resetPasswordExpireTime, userQRPrefix, resetPasswordMessage} from '../settings/settings'
import { authUser, comparePassword, decryptString, encryptString, hashPassword, qrGeneratorBase64 } from '../lib/authenticationTools'

import { templateActivationEmail, templatePasswordResetEmail } from '../lib/templateTools'
import { sendEmail } from '../lib/mailTools'

import axios from 'axios'
import { access } from 'fs'



const newCustomerDefaults = {
  administrator : false,
  activated: false,
  branded : 0,
  type : 1,
  serviceNewsLetter : 0,
  serviceEvents : 0,
  serviceStore : 0,  
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const activateCustomer = async (_id: any, encryptedID: any, provider: any)=> {
  const result = await db.collection( unauthenticated, 'Users',{identifier: provider}).updateOneByID ( 
    ObjectId (_id), { $set: {
      activated : true, 
      encryptedID: encryptedID,  
      publicURL : encryptedID, 
      qrCode : await qrGeneratorBase64 (encryptString (userQRPrefix+ ':'+ _id))
    },
  })
  return result.value
}
///////////////////////////////////////////////////////////////////////

export const graphqlFunctions = {    

  activateUser: async (params: any) =>{    
    const { activation } = params.args

    const decrypted = await decryptString(activation)    

    const [_id, provider] = decrypted.split (':')

        
    return activateCustomer (_id, activation, provider)    
  },

   
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  loginUser: async (params: any) =>{  
    const { args } = params
    const {email, password, provider, oauth, oauthProvider} = args  


    if (oauthProvider == 'facebook'){
      const facebook_url = 'https://graph.facebook.com/v6.0/me?access_token='

      const result = await axios.get (facebook_url+oauth)
      let customer = await db.collection(unauthenticated, 'Users').findOneByUnsafeSearch({oauth : result.data.id})    

      if (customer == null){
        const [fname, lname] = result.data.name.split(' ')
        customer = await db.collection(unauthenticated, 'Users', {identifier: provider}).insertOne({...newCustomerDefaults, activated: true, oauth: result.data.id, firstName: fname, lastName: lname  })
        
        
      }

      return authUser (customer, provider)
    }

    const client = '86w1070fw6v3w8'
    const key= 'UIWcrodLEHQtnGSO'
    const url = "https://interviewlook.com/loginLinkedIn"
    

    if (oauthProvider == 'linkedin'){
      const oauth_url = `https://www.linkedin.com/oauth/v2/accessToken?grant_type=authorization_code&code=${oauth}&redirect_uri=${url}&client_id=${client}&client_secret=${key}`

      
      const result = await axios.get (oauth_url)

      const access_token = result.data. access_token
      
      

      const user  = await axios.get ('https://api.linkedin.com/v2/me?projection=(id,firstName,lastName,profilePicture(displayImage~:playableStreams))', {headers: {
          "Authorization" : `Bearer ${access_token}`
        }})
        

      const {id, lastName, firstName} = user.data
      const image = user.data.profilePicture['displayImage~'].elements[2].identifiers[0].identifier
      
      let customer = await db.collection(unauthenticated, 'Users').findOneByUnsafeSearch({oauth : `linkedin${id}`})    


      

      if (customer == null){      
        const lastname = lastName.localized.en_US
        const firstname = firstName.localized.en_US
        
        customer = await db.collection(unauthenticated, 'Users').insertOne({...newCustomerDefaults, activated: true, oauth: `linkedin${id}`, firstName: firstname, lastName: lastname, profileImage : image  })        
      }

      
      return authUser (customer, provider)      
    }




        
    if (!email || !password) throw new Error('Email or password not set in the request')    
            
    
    const customer = await db.collection( unauthenticated, 'Users', {identifier: provider}).findOneByUnsafeSearch( { email : email, activated: true} )

    if (!customer) throw new Error('error logging in')
    if (customer && customer.activated==false) throw new Error('error logging in')

    if (!await comparePassword(password, customer.password)) {
        throw new Error('error logging in')        
    }        
    return authUser (customer, provider)    
  },



  
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  loginWithToken: async (params: any) => {
    const token = params.args.token
    const [email, tokenTime, provider] =  decryptString (token).split (':')

    const customer = await db.collection(unauthenticated, 'Users', {identifier: provider}).findOneByUnsafeSearch({email : email, activated: true})

    if (token.localeCompare ( customer.recoveryToken) != 0) 
        throw new Error('error logging in')

    if (parseInt(tokenTime) <  Date.now() - (resetPasswordExpireTime ) )
        throw new Error('error logging in')       
    
    return authUser (customer)    
  },


  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  registerUser :  async  ( params:any ) => {
    const  user = {...params.args.create, ...newCustomerDefaults}
    
    const { name, password, email, provider } = user
    
    if (!password) throw new Error('Password not set in the request')    
    if (!email) throw new Error('Email not set in the request');
        

    console.log (`creating new user ${name} ${email} ${provider}`)

    if (provider){
      const parent = await db.collection(unauthenticated, 'Users').findOneByID( ObjectId(provider)) 
      if (!parent)
        throw new Error('Invalid Provider')
    }

    const customer = await db.collection(unauthenticated, 'Users', {identifier: provider}).findOneByUnsafeSearch({email : email})    
    if (customer != null){        
        throw new Error('Error creating account')   
    }
        
    
    user.password = await hashPassword(user.password)


    const userCreate = await db.collection(unauthenticated, 'Users', {identifier: provider}).insertOne(user)
        
    user.password= 'hidden'

    const message = templateActivationEmail (activateionMessage, userCreate, provider)
    sendEmail ({
        to : user.email,
        message: message,
        subject: 'Activate your account'
    })
    return (user)
  },    

  



  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  resetPassword: async (params: any) => {
    const { args } = params
    const {email, provider} = args  

    const recoveryToken = encryptString ( email + ':' + Date.now().toString () + ':' + provider)

    const customer = await db.collection(unauthenticated, 'Users').updateOneByUnsafeSearch({email : email, activated: true}, {$set : {recoveryToken : recoveryToken} })
    
    if (customer){
      console.log (`*************************************: Resetting password for ${customer.email}`)
      const message = templatePasswordResetEmail (resetPasswordMessage, recoveryToken)
      sendEmail ({
        to : customer.email,
        message : message, 
        subject : 'Password Reset'
      })
    }
    return ({status: 0, message: 'done'})
  },

  //////////////////////////////////////////////////////
  //////////////////////////////////////////////////////
  updateCustomer : async (params: any) => {
    const { args, auth} = params
    const {updates} = args
    const _id = auth.user._id

    if (updates.username){
      if (await db.collection(unauthenticated, 'Users').findOneByUnsafeSearch ( {_id: {$ne: ObjectId (_id)}, username : updates.username}))
        throw new Error('username is already taken')
    }
    
    if (updates && updates.password) {
      updates.password = await hashPassword(updates.password)
    }

    if (updates.email) 
      delete updates.email
    
    return db.collection(unauthenticated, 'Users').updateOneByID ( _id, { $set: updates})
  },



  //////////////////////////////////////////////////////
  //////////////////////////////////////////////////////  
  user : async (params: any) => {
    const { auth, publications } = params    
    auth.password=''
    return auth.user
  },


  
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////






  Admin_ActivateUser : async (params: any) =>{    
    const { _id } = params.args
    
    const activation = encryptString (_id)

    //return activateCustomer (_id, activation)    
  },


  Admin_ListUsers : (params: any) =>{
    const { auth } = params
    return db.collection(auth,'Users').findManyByUnsafeSearch({})
  }, 
  

  Admin_UpdateCustomer: async (params: any) => {
    const { args, auth} = params
    const {_id, updates} = args
    
    if (updates && updates.password) {
      updates.password = await hashPassword(updates.password)
    }

    if (updates.email) {
      //check for email conflict then update email, for now, just ignore the update request.
      delete updates.email
    }
    
    return db.collection(unauthenticated, 'Users').updateOneByID ( ObjectId (_id), { $set: updates})
  },

  Admin_ListCustomersDetails: (params: any)=>{
    const { args, auth} = params
    const {_id } = args

    return db.collection(unauthenticated, 'Users').findOneByID ( ObjectId (_id) )
  }

}
