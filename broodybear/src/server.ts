import * as express from 'express';
import * as graphqlHTTP from 'express-graphql';
import * as morgan from 'morgan';
import * as cors from 'cors';
import * as errorHandler from 'errorhandler';
import * as bodyParser from 'body-parser';
import { createServer } from 'http';

import { version } from './settings/version'

import { execute, subscribe } from 'graphql';
import { SubscriptionServer } from 'subscriptions-transport-ws';

//import { PubSub } from 'graphql-subscriptions';
import { RedisPubSub as PubSub} from 'graphql-redis-subscriptions';
import * as Redis from 'ioredis';
import * as razorpay from 'razorpay'


import { printSchema } from 'graphql/utilities/schemaPrinter';

import { initDB, db, unauthenticated } from './lib/mongo'
import { rsvp } from './lib/api'

import { initGraphQL } from './lib/graphql';


import { setupAuthStrategy } from './lib/authenticate';
import { fullyAuthorized, halfAuthorized,  qrGenerator } from './lib/authenticationTools';


import { serverPort, mongoDbUri, awsSESCallbackURL, qrGeneratorUrl, redisServer, redisPort, redisAuth, razorPayCallbackURL } from './settings/settings'
import { schema } from './schema'

import { graphqlFunctions } from './graphql'



declare const require: any

const EventEmitter = require('events');
const DEBUG_MODE = true;

const app = express();
const qlSchema = schema()


const redisOptions = {
    host: redisServer,
    port: redisPort,
    //password : redisAuth,
    retryStrategy: times => {
      // reconnect after
      return Math.min(times * 50, 2000);
    }
  }

export const publications = new PubSub({    
    publisher: new Redis(redisOptions),
    subscriber: new Redis(redisOptions)
  })

initDB (qlSchema, mongoDbUri, )
const graphqlSchema = initGraphQL (qlSchema, graphqlFunctions, publications)

app.use(morgan('combined'));
//app.use(bodyParser.urlencoded({ limit: '10mb',  extended: true }));
//app.use(bodyParser.json({ limit: '10mb'}));
app.use(errorHandler());

setupAuthStrategy(app)

app.get('/', (req, res) => {res.status(200).send(version) });
app.get('/logout', (req, res) => { req.logout();  res.redirect('/') })

app.use('/schema',  (req, res, _next) => {
    res.set('Content-Type', 'text/plain')
    res.send(printSchema(graphqlSchema))
})




//QR CODE Generateor
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////

app.get(`${qrGeneratorUrl}/:qr`, async (req, res) => {               
    const code = req.params.qr.slice(0,-3)
    const img = await qrGenerator(req.params.qr)    

   res.writeHead(200, {
     'Content-Type': 'image/png',
     'Content-Length': img.length
   })

   
   res.end(img);
})

//SES UPDATE
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
const SESDeliveryTypes = {
    'Pending' :0,
    'Delivery' :1,
    'Bounce' : 2,
    'Complaint' :3,
    'Open' :4,
}

app.post (awsSESCallbackURL, (req, res)=> {

    const chunks = [];
    req.on('data', function (chunk) {
        chunks.push(chunk);       
    });

    req.on('end', async () => {
        const message = JSON.parse(chunks.join(''));
        
        const messageData = JSON.parse(message.Message);
        const messageId = messageData.mail.messageId  
        const notificationType = SESDeliveryTypes[messageData.eventType]

        const now = new Date().toISOString()               
        
        
        if (messageData && messageId && notificationType ) {                 
            
            const detail = await db.collection(unauthenticated, 'GuestCommunicationDetails').updateOneByUnsafeSearch({ messageId: messageId }, { $set: {deliveryStatus : notificationType,  _updatedTime: now}})
                        
            const activity = await db.collection(unauthenticated, 'GuestCommunicationDetailsActivities').insertOne ({
                parentGuestCommunicationDetails : detail._id,
                _createdTime: now,
                activity : notificationType
            })

            db.collection(unauthenticated, 'GuestCommunicationDetails').updateOneByID( detail._id , { $push: {childrenGuestCommunicationDetailsActivities : activity._id}} )
            
            if (detail.parentEventGuest)
                db.collection(unauthenticated, 'eventGuests').updateOneByID( detail.parentEventGuest, { $set: {deliveryStatus : notificationType,  _updatedTime: now}})            
        }        
        
    });

    res.end();

})

//QL Endpoint
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
app.use('/ql',bodyParser.urlencoded({ limit: '10mb',  extended: true }), bodyParser.json({ limit: '10mb'}),
    cors(),
    halfAuthorized(),    
    graphqlHTTP(request => {
        const startTime = Date.now();

        return {
            schema: graphqlSchema,
            graphiql: true,  // <--- Make this true
            context: {
                user: request.user,
                authenticated: (request.user) ? true : false,
            },
            // formatError(err) {return errorCatcher(err);},
            extensions({ document, variables, operationName, result }) {  
                //console.log (result)
                return {  runTime: Date.now() - startTime };
            }
        };
    })
)

const server = createServer(app);
server.listen(serverPort, '0.0.0.0' as any, () => {
    console.log('Server started here -> http://0.0.0.0:' + serverPort);
  });



// Create the Server
// ****************************************************************************** */
// ****************************************************************************** */
// ****************************************************************************** */
// ****************************************************************************** */




//import { SubscriptionManager, PubSub } from 'graphql-subscriptions';

/*const schema = {}; // Replace with your GraphQL schema object
const pubsub = new PubSub();

const subscriptionManager = new SubscriptionManager({
  schema,
  pubsub
});*/



/** GraphQL Websocket definition **/
SubscriptionServer.create({
    execute,
    subscribe,
    schema : graphqlSchema,   
    onConnect: async (connectionParams: any, webSocket: any) => {

        if (connectionParams.headers && connectionParams.headers.authorization) {            
            const bus = new EventEmitter();
            let user = null

            const request = {
                headers: {
                    authorization: connectionParams.headers.authorization
                },
                user: null,
                logIn : ( result: any, webSocket: any, next: any ) => { return  next ( result ) }
            }

            console.log (`socket connection request`)
            halfAuthorized()( request , webSocket , (_request: any) => {                
                user = request.user
                bus.emit('loaded')
            } )

            await new Promise(resolve => bus.once('loaded', resolve))
            return ({ user: user, authenticated:true })
        }
        return ({ user: null, authenticated: false })
    }
},  { server: server,  path: '/ws'})



