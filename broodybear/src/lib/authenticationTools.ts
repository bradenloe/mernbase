import * as bcrypt from 'bcryptjs'
import * as passport from 'passport'

const Buffer = require('buffer').Buffer
const qrcode = require ('qrcode')

const AES = require("crypto-js/aes");
const crypto = require("crypto-js");

import * as jwt from 'jsonwebtoken';

import { encryptionKey , JWT_KEY } from '../settings/settings'

/////////////////////////////////////////////////////////////////////////////////////////////////
export const base64Encode = (str :string) => {
    return Buffer.from(str).toString('base64')    
}

/////////////////////////////////////////////////////////////////////////////////////////////////
export const base64Decode = function (str :string) {
    return Buffer.from(str, 'base64').toString('binary')
}

/////////////////////////////////////////////////////////////////////////////////////////////////
export const fullyAuthorized = () =>{
    return passport.authenticate('JWT', { session: false });        
}

/////////////////////////////////////////////////////////////////////////////////////////////////
export const halfAuthorized =  () => {    
    const auth = (req, res, _next) => {        
        req.authenticated= false
    
        const passportAuth = passport.authenticate('JWT', { session: false },(err, user, info) => {        
            if (user){
                req.user= user
                req.authenticated= true
            }

            if (err)  _next(err)    
            _next()
        });

        passportAuth (req, res, _next)        
    }
    return auth     
}

/////////////////////////////////////////////////////////////////////////////////////////////////
export function  onlyAuthorizedWs () {
    return passport.authenticate('JWT', { session: false })
}

/////////////////////////////////////////////////////////////////////////////////////////////////
export const encryptString = ( planString: string ) => {        
    const utf8 = crypto.enc.Utf8.parse (planString)
    const encrypted = AES.encrypt(utf8, encryptionKey);    
    const encoded = base64Encode(encrypted.toString())     
    return encoded
}

/////////////////////////////////////////////////////////////////////////////////////////////////
export const decryptString= ( codedString : string ) => {       
    const decoded = base64Decode (codedString)    
    const decrypted = AES.decrypt(decoded, encryptionKey);    
    return decrypted.toString(crypto.enc.Utf8)
}

/////////////////////////////////////////////////////////////////////////////////////////////////
export async function comparePassword(candidatePassword :string, dbPassword:string) {
    const isMatch = await bcrypt.compare(candidatePassword, dbPassword);
    return isMatch;
}


/////////////////////////////////////////////////////////////////////////////////////////////////
export const hashPassword = async (password) => {
    const saltRounds = 10;
  
    const hashedPassword = await new Promise((resolve, reject) => {
      bcrypt.hash(password, saltRounds, function(err, hash) {
        if (err) reject(err)
        resolve(hash)
      });
    })
  
    return hashedPassword
}

/////////////////////////////////////////////////////////////////////////////////////////////////
export const qrGenerator = async (qr :string) => {     
        
    const base64 =  await qrcode.toDataURL(qr)
    const base64_image =  base64.split(',')[1]

    return Buffer.from(base64_image, 'base64')    
}


export const qrGeneratorBase64= async (qr :string) => {     
    const base64 =  await qrcode.toDataURL(qr)
    //const base64_image =  base64.split(',')[1]

    //return Buffer.from(base64_image, 'base64')    
    return base64
}


export const authUser = (user: any, provider = null as any) => {
    if (user.banned == true)
        throw new Error ('account is disabled')


    const jwtPayload = {
        id: user._id.toString(),
        type: "USER",
        provider: provider
    }    

    user.password = ''
    user.auth = jwt.sign(jwtPayload, JWT_KEY)
    
    return ( user  )
}

