import { AWS_SES } from '../../../settings/settings'

const AWS = require('aws-sdk');

const ses = new AWS.SES(AWS_SES)

export  interface EmailMessage {
  id: String,
  to: any,
  from: String,
  message: String,
  subject: String
}

export  interface BulkTemplatedEmailMessage {
  id: String,
  to: any,
  from: String,
  template: String,
  defaultTemplateData: String
}


/*sendEmail ({
  id : message._id,
  to : message.to,
  from: message.from,
  subject: message.subject,
  message: message.body
}) */


 // TODO - Some of the feild mappings are not correct.
export const sendEmail = async ( mail: EmailMessage ) => {

    const params = {
      Destination: {ToAddresses: mail.to} ,
      Message: {
        Body: {
          Html: {
            Charset: 'UTF-8',
            Data: mail.message
          },
          /*Text: {
            Charset: "UTF-8",
            Data: textData
          }*/
        },
        Subject: {
          Charset: 'UTF-8',
          Data: mail.subject
        }
      },
      Source: mail.from,
      ReplyToAddresses: [mail.from] ,
       ConfigurationSetName: 'logs'
    };

    const result = await ses.sendEmail(params).promise()        
    return result
}

// TODO - Some of the feild mappings are not correct.
export const sendBulkTemplatedEmail = async ( mail: BulkTemplatedEmailMessage ) => {
  const params = {
    Source: mail.from,
    ReplyToAddresses: [mail.from] ,
    ConfigurationSetName: 'default',
    Destinations: mail.to ,
    Template: mail.template,
    DefaultTemplateData: mail.defaultTemplateData

  };

  return ses.sendBulkTemplatedEmail(params).promise();
}
