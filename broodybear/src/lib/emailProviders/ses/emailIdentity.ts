/**
 * This file is used to handle email Identity
 */
import { ses } from './simpleEmailService'

/**
 * The verifyGivenEmailIdentity function is used to add an email address to the list of identities for 
 * your Amazon SES account in the current AWS region and attempts to verity it. 
 * 
 * As a result of executing this operation, a verification email is sent to the specified address.
 * 
 * You can execute this operation no more than once per second.
 * @param email can be also a domain
 */
export function verifyEmailIdentity(email) {
    return ses.verifyEmailIdentity({ EmailAddress: email }).promise();
}

/**
 * The deleteIdentity function is used to delete a specified identity ( an email address or a domain )
 * from the list of verified identities.
 * 
 * You can execute this operation no more than once per second.
 * @param identity 
 */
export function deleteIdentity(identity) {
    return ses.deleteIdentity({Identity: identity}).promise();
}

/**
 * The listIdentities function return a list containing all of the identities ( email addresses and domains ) for your
 * AWS account in the current AWS Region, regardless of verification status.
 * 
 * You can execute this operation no more than once per second.
 * @param type --> EmailAddress | Domain
 * @param number 
 * @token used to pagination
 */
export function listIdentities(type, number, token = "") {
  return ses.listIdentities({
    IdentityType: type, 
    MaxItems: number, 
    NextToken: token
   }).promise();
}

/**
 * The verifyDomainIdentity function is used to add a domain to the list of identities for you Amazon SES account
 * in the current AWS Region and attempts to verify it.
 * 
 * You can execute this operatoin no more than once per second.
 * @param domain 
 */
export function verifyDomainIdentity(domain) {
    return ses.verifyDomainIdentity({Domain: domain}).promise();
}

/**
 * The verifyDomainDkim function returns a set of DKIM tokens for a domain identity.
 * When you execute the VerifyDomainDkim operation, the domain that you specify is added to the list of identities that
 * are associated with your account. However, you can't send email from the domain until you either successfully
 * verify it or you successfully set up DKIM for it.
 * 
 * @param domain 
 */
export function verifyDomainDKim(domain) {
    return ses.verifyDomainDKim({Domain: domain}).promise();
}