import { ses } from './simpleEmailService'


export  interface emailTemplate{
  id: String,
  name: String,
  html: String,
  subject: String,
  text: String
}

//TODO - Some of the feild mappings are not correct. 
export const createTemplate = async ( template: emailTemplate ) => {
    var params = {
        Template: { 
          TemplateName: template.name, /* required */
          HtmlPart: template.html,
          SubjectPart: template.subject,
          TextPart: template.text
        }
    };

    return ses.createTemplate(params).promise();
}

export const listTemplates = async ( maxItems: any ) => {
    return ses.listTemplates({MaxItems: maxItems}).promise();
}

/**
 * TODO add get/delete template
 * 
*/

