// using Twilio SendGrid's v3 Node.js Library
// https://github.com/sendgrid/sendgrid-nodejs

import sgMail = require("@sendgrid/mail");


export interface emailMessage {
  id: String,
  to: [String],
  from: String,
  message: String,
  subject: String
}



export const sendEmails = (message: emailMessage) => {
  const sgMail = require('@sendgrid/mail');
  sgMail.setApiKey(process.env.SENDGRID_API_KEY);
  const msg = {
    to: message.to,
    from: message.from,
    subject: message.subject,
    html: message.message
  };
  sgMail.send(msg);
}
console.log("Sending via SendGrid");
