
import {
    GraphQLSchema,
    GraphQLObjectType,
    GraphQLList,
    GraphQLNonNull,
    GraphQLString,
    GraphQLInt,
    GraphQLID,
    GraphQLInputObjectType,
    GraphQLBoolean,
    GraphQLInterfaceType
} from 'graphql';

import { GraphQLDateTime } from 'graphql-iso-date';
import { ObjectId } from './mongo'

import { withFilter } from 'graphql-subscriptions';


const registeredTypes = {
    'Int'    : GraphQLInt,
    'IntInput': GraphQLInt,

    'ID'     : GraphQLID,
    'id'     : GraphQLID,
    'string' : GraphQLString,
    '[string]' : new GraphQLList(GraphQLString),
    'String' : GraphQLString,
    '[String]' : new GraphQLList (GraphQLString),
    'int'    : GraphQLInt,
    'time'   : GraphQLDateTime,
    'Boolean'   : GraphQLBoolean,
    'boolean'   : GraphQLBoolean,

    'ID!'     : new GraphQLNonNull(GraphQLID),
    '[ID]'    : new GraphQLList (GraphQLID),
    '[ID!]'    : new GraphQLList (new GraphQLNonNull(GraphQLID)),
    'id!'     : new GraphQLNonNull(GraphQLID),
    'string!' : new GraphQLNonNull(GraphQLString),
    'String!' : new GraphQLNonNull(GraphQLString),    
    'int!'    : new GraphQLNonNull(GraphQLInt),
    'time!'   : new GraphQLNonNull(GraphQLDateTime),
    'Boolean!'   : new GraphQLNonNull(GraphQLBoolean),
    'boolean!'   : new GraphQLNonNull(GraphQLBoolean),

    'idInput'     : GraphQLID,
    'stringInput' : GraphQLString,
    'intInput'    : GraphQLInt,
    'timeInput'   : GraphQLDateTime,
    'BooleanInput'   : GraphQLBoolean,

    'id!Input'     : new GraphQLNonNull(GraphQLID),
    'string!Input' : new GraphQLNonNull(GraphQLString),
    'int!Input'    : new GraphQLNonNull(GraphQLInt),
    'time!Input'   : new GraphQLNonNull(GraphQLDateTime),
    'Boolean!Input'   : new GraphQLNonNull(GraphQLBoolean),
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const generateFieldList = ( template : any)=>{
    const inputFields = {      
    } as any 

    const outputFields ={
        _id : {type : GraphQLID},        
        _createdTime : {type : GraphQLDateTime},
        _updatedTime : {type : GraphQLDateTime},
    } as any

    for (var field in template) {
        const fieldType = template[field].type
        if (field[1] != '$' ) {            
            if (template[field].readOnly != true)
                inputFields[field]= {type: registeredTypes[fieldType]}
            if (template[field].writeOnly != true)
                outputFields[field]= {type: registeredTypes[fieldType]}
        }
    } 
    return [inputFields, outputFields] 
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const generateChildrenFields = (schema:any, template: any) =>{
    
    const onetomany = {...template._$children, ...template._$oneToMany}
    const onetoone = {...template._$child, ...template._$oneToOne}

    const children = {}
    

    const generate = (name:string, view: any, register: any, error: any)=>{
        const viewName = view.type ||  view
        const viewType = registeredTypes[viewName]           

        const args = {}
        const parameters = view.parameters || {}

        for (let name in parameters) {
            const type = parameters[name]
            args[name] = { type : registeredTypes[type] }                
        }

     
        return ({
            type: register(viewType),
            args : args,
            resolve: (root :any, args :any, auth: any)=>{
                if (!root[name])    
                    return (error)

                const parameters = {
                    _id: root[name],                    
                    args: args,     
                    auth: auth,
                    table: viewName,
                    search: {_id : {$in : root[name]} },
                    resolve: schema.lib.__childQuery,
                    root: root
                    
                }
                if (schema.lib[`__${viewName}__Query`]){
                    const resolver = schema.lib[`__${viewName}__Query`]
                    return resolver(parameters)
                }                
                return schema.lib.__childQuery(parameters)
            }

            
        })
    }

    if (onetomany) 
        for (let key in onetomany)
            children[key]=generate(key, onetomany[key], (t:any)=> {return new GraphQLList(t)},[])
        
    
    if (onetoone)    
        for (let key in onetoone)
            children[key]=generate(key, onetoone[key], (t:any)=> {return (t)},{})        
         
    

    return children
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


const registerType = ( schema :any, templateName :any )=> {

    const template = schema.types[templateName]

    console.log (`gql registering ${templateName}`)
    const [inputFields, output ] = generateFieldList (template)

    const outputFields = ()=>{
        return {...output, ...generateChildrenFields(schema,template)}
    }

    //Register the input type for the template
        const inputTypeName = `${templateName}Input`

        //Register the output type for the template
        
        registeredTypes[templateName] =  new GraphQLObjectType ({ name: templateName, description: templateName, fields: outputFields} )
        registeredTypes[`[${templateName}]`] = new GraphQLList (registeredTypes[templateName])
        registeredTypes[`${templateName}!`] = new GraphQLNonNull (registeredTypes[templateName])        

        registeredTypes[inputTypeName] =  new GraphQLInputObjectType ({ name: inputTypeName, fields: inputFields } )                
        registeredTypes[`[${inputTypeName}]`] = new GraphQLList (registeredTypes[inputTypeName])
        registeredTypes[`${inputTypeName}!`] = new GraphQLNonNull (registeredTypes[inputTypeName])
    
    
    return ( registeredTypes[templateName] )
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const registerFunction = (schema :any,publications: any, functionTemplate :any, handler :any ) => {
    const { parameters, returnType, authenticated } = functionTemplate

    const { userAdminstrator } = schema.const
    const registeredReturnType = registeredTypes[returnType]
    

    const registeredParams = {} as any
    for (var name in parameters) {
        const type = parameters[name]
        registeredParams[name] = { type : registeredTypes[type] }
    }

    const table = (functionTemplate.table!= null) ? schema.types[functionTemplate.table] :  null

    if ( functionTemplate.subscription ){
        return {
            type: registeredReturnType,
            args: registeredParams,
            resolve : (payload: any)=> {                
                return payload
            },
            subscribe : withFilter (() => publications.asyncIterator (functionTemplate.subscription), ( payload: any, variables: any, context:any , info: any) =>{
                return handler({
                    auth : context,
                    publication: payload, 
                    variables : variables,
                    info: info
                })
            })
        }
    }
    
    return {
        type: registeredReturnType,
        args: registeredParams,
        resolve:  (_: any, args: any, auth: any ) =>{
                       
            
            if (authenticated !== false && auth.authenticated !== true) {
                throw ('unauthorized')
            }
            
            if (authenticated == userAdminstrator && auth.user.administrator !== true) {
                throw ('unauthorized')
            }

            const params =  {
                args : args,
                schema : schema,
                
                table : table,
                auth: auth,             
                publications: publications
            }                     
            
            return handler(params)           
        }
    }
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const initGraphQL = ( schema: any, functions: any, publications:any) => {    
  
    const buildLibrary = (template: any) => {
        const lib = {}

        for (let functionName in template) {
            const funct = template[functionName]
            funct.name = functionName
            let handler = functions[functionName]
    
            let errorTable = functionName
            if (funct.handler != null){
                errorTable= `default - ${funct.handler}`
                handler = functions[funct.handler]
            }                    
    
            if (handler == null)
                console.log (`undefined function ${errorTable}`)
    
            lib[functionName] = registerFunction (schema, publications, funct, handler  )         
        }
        return lib
    }

    
    for (let table in schema.types) {
        registerType (schema, table)
    }

    schema.lib = functions

    const queries = buildLibrary (schema.queries)
    const mutations = buildLibrary (schema.mutations)    
    const subscriptions= buildLibrary (schema.subscriptions)        


    
    return new GraphQLSchema({
        query: new GraphQLObjectType ({
            name: 'Query',
            fields: queries
        }),
        mutation: new GraphQLObjectType ({
            name: 'Mutation',
            fields: mutations
        }),
        subscription: new GraphQLObjectType ({
            name: 'Subscription',
            fields: subscriptions
        }),
        
    })

}
