import { decryptString } from './authenticationTools'
import { db, ObjectId} from './mongo'
import { SendUnformatedMessage } from './mailTools'

export const rsvp = async (req, res) => {

    const [url, params] = req.url.split ("?")
    const decodedString = decryptString (params)


    const action= decodedString[0]
    const guestId = decodedString.slice (1)


    if (action == 'a' || action == 'd' ){
        const status= (action == 'a' ) ? 2 : 6

        const guestdb = await db.collection('eventGuests').findOneAndUpdate({ _id: ObjectId (guestId) }, {$set: {status: status}}, {returnOriginal: false, upsert: false})                                   
        const guest=guestdb.value
        const event = await db.collection('events').findOne ({_id : guest._eventId})
        
        const message = {
            id : 'message._id',
            to : guest.email, 
            from: 'me@here.com',
            subject: (action == 'a' ) ? 'RSVP Accepted' : 'RSVP Declined',
            body: (action == 'a' ) ? event.rsvpAcceptMessage : event.rsvpDeclineMessage
        }
        
        SendUnformatedMessage (message)
        return res.status(200).send( (action == 'a') ? 'Accepted'  : 'Declined' ) 
    }    

    res.status(200).send('Not sure what you are doing, but stop it.') 
}




