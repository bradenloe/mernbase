/**
 * This file is used to catch all the errors from the
 * graphQL endpoints.
 */

/**
 * The errorTables is a dictionary that allow me to find
 * directly the message to return back to the user.
 */
// FIXME: rethinking, sometimes is has problem!
const errorTables = {
    FIELD: {
        ORGANIZER: {
            NAME: "ORGANIZER_NAME_MISSED",
            PASSWORD: "ORGANIZER_PASSWORD_MISSED",
            EMAIL: "ORGANIZER_EMAIL_MISSED"
        },
        STAFF: {
            NAME: "STAFF_NAME_MISSED",
            PASSWORD: "STAFF_PASSWORD_MISSED",
            EMAIL: "STAFF_EMAIL_MISSED",
            GENRE: "STAFF_GENRE_MISSED"
        },
        EVENT: {
            NAME: "EVENT_NAME_MISSED",
            TYPOLOGY: "EVENT_TYPOLOGY_MISSED",
            GENRE: "EVENT_GENRE_MISSED",
            CATEGORY: "EVENT_CATEGORY_MISSED",
            LANG: "EVENT_LANG_MISSED",
            EVENTID: "EVENT_EVENTID_MISSED"
        },
        TICKET: {
            NAME: "TICKET_NAME_MISSED",
            EVENTID: "TICKET_EVENTID_MISSED",
            PRICE:  "TICKET_PRICE_MISSED",
            GENRE: "TICKET_GENRE_MISSED",
        },
    },
    VALIDATION: {
        ORGANIZER: {
            EMAIL: "ORGANIZER_EMAIL_INVALID", 
            PASSWORD: "ORGANIZER_PASSWORD_SHORT_OR_MISSING_SPECIAL_CHARACTER",
        },
        MEMBER: {
            EMAIL: "MEMBER_EMAIL_INVALID",
            PASSWORD: "MEMBER_PASSWORD_SHORT_OR_MISSING_SPECIAL_CHARACTER",
            TELEPHONE: "MEMBER_TELEPHONE_INVALID",
        },
        
    },
    DUPLICATE: {
        ORGANIZER: {
            NAME: "ORGANIZER_NAME_DUPLICATED",
        },
        STAFF: {
            NAME: "STAFF_NAME_DUPLICATED",
        },

    },
    SYNTAX: {
        ORGANIZER: {
            NAME: "GRAPHQL_SYNTAX_ORGANIZER_EXPECTED_NAME"
        }
    }
};

 /**
  * The errorCatcher function is used to catch the
  * error and returns back a custom error msg.
  * 
  * @param err 
  */
export function errorCatcher(err) {

    console.log(err);

    const listName = {
        // Type
        FIELD: 0, VALIDATION: 0, DUPLICATE: 0, SYNTAX: 0,
        // Who
        ORGANIZER: 0, STAFF: 0,EVENT: 0, MEMBER:0, 
        // --> Collections
        HEROSORGANIZERS: 0,
        // Field
        TICKET: 0, NAME: 0, PASSWORD: 0, EMAIL: 0, TELEPHONE: 0,
        GENRE: 0, TYPOLOGY: 0, CATEGORY: 0, EVENTID: 0, LANG: 0,
    };
    
    /**
     * BE AWARE that it works only with camelcase method!
     * MAYBE THERE ARE DIFFERENT PATTERN NOT CONSIDERED BEFORE: BE CAREFUL!
     * First replace all special symbol except space into "",
     * then split the String into subString with Uppercase word,
     * join them again adding a space, at the end all into UpperCase
     */
    const strings = err.message.replace(/[^a-zA-Z ]/g, "").split(/(?=[A-Z])/).join(" ").toUpperCase().split(" ");
    strings.forEach(string => {
        if(listName[string]!=null) listName[string] = 1;
    });
    console.log(strings);
    console.log(listName);
    const solution = {};
    let count = 0;
    for(let key in listName){
        if (listName[key] == 1 && count == 0) {
            solution["TYPE"] = key;
            count++;
            continue
        } 
        if (listName[key] == 1 && count == 1) {
            solution["WHO"] = key;
            count++;
            continue
        }
        if (listName[key] == 1 && count == 2) {
            solution["FIELD"] = key;
            count++;
            continue
        }
    }
    console.log(err.message);
    // If I didn't find one, rethrow the err
    if (count != 3) {
        return {
            message: err.message
        }
    }

    try { 
        const msg = errorTables[solution['TYPE']][solution['WHO']][solution['FIELD']];
        return { message: msg }
    } catch (error) {
        return { message: err.message }
    }
    
}