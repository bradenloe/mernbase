
import * as passport from 'passport';
//import * as jwt from 'jsonwebtoken';


import { db, ObjectId, unauthenticated  } from '../lib/mongo'

import { Application, Request } from 'express';
import { Strategy, StrategyOptions, ExtractJwt, VerifiedCallback } from 'passport-jwt';

import { JWT_KEY } from '../settings/settings'


/**
 * An interface that represents a JwtPayload
 * 
 * @interface
 */
interface JwtPayload {
    id: string;
    type: string;
    provider: string
}

/**
 * This constant is used as a key
 * for the passport-jwt
 */




/** 
 * Options for JWT
 * in this exemple we use fromAuthHeader, so the client need to
 * provide an "Authorization" request header token
 */
const jwtOptions: StrategyOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeader(),
    secretOrKey: JWT_KEY,
    passReqToCallback: true,
};


/**
 * Definition of the Strategy used by the passport to verify 
 * the token received.
 */
const jwtStategy = new Strategy(
    jwtOptions,
    async (request: Request, jwtPayload: JwtPayload, done: VerifiedCallback) => {                
        const {id, provider} = jwtPayload
        
        if ( id == null ) throw new Error('No Id in the JWT session token')

        console.log (jwtPayload)
        console.log (provider)
        

        const user =  await db.collection(unauthenticated, 'Customers', {identifier : provider}).findOneByID( ObjectId (id) )
        console.log (user)

        if (!user)
            return done('error logging in', false)
        if (user.banned == true)
            return done('account disabled', false);
        return done (null, user)        
    })



/**
 * If added to a express route, it will make the route require the auth token
 */

/**
 * Setup the Passport JWT for the given express App.
 * It will add the auth routes (auth, login, logout) to handle the token authorization
 * Set addDebugRoutes to true for adding a Auth Form for testing purpose
 * 
 * @param app
 * @param addDebugRoutes
 */
 
export const setupAuthStrategy = (app: Application) => {

    passport.use('JWT', jwtStategy);
    app.use(passport.initialize());

    app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept ");
        next();
      });

}








