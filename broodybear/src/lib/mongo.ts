const MongoClient = require('mongodb').MongoClient;
export const {ObjectId} = require('mongodb');

import { decryptString } from '../lib/authenticationTools'
import { dbName } from '../settings/settings'


export var unsafe = null
export const unauthenticated = '0xffffffff'  

var client = null

export const registeredTables = {
    global: { _$table: '__settings' }
} as any

export const initDB =  async (newSchema : any, mongoDbUri: string) => {
    for (var key in newSchema.types) {
        //const table = newSchema.types[key]._$table

        console.log (`db registering ${key}`)
        registeredTables[key] = newSchema.types[key]
    }

    client = await MongoClient.connect (mongoDbUri);
    unsafe = await client.db(dbName)    
}



export const db = {
    collection : (auth: any, tableName: any, params= {} as any) => {
        const { identifier } = params
        
        const postfix = identifier || ''
        const table = registeredTables[tableName]
        const collection = `${table._$table}${postfix}`

        const checkReadAccess = ( result: any) =>{
            if (result == null)
                return null
            if (auth && auth.user && auth.user.staff == true)
                return result
            if (auth == unauthenticated)
                return result
            if (auth == null)
                throw 'unauthorized'

            if ( result._auth.equals(auth.user._id) )                
                return result
            
            if ( result._auth.equals(auth.user.parentCustomer ))            
                return result
            throw 'unauthorized'
        }
        
        const secureUpdateOneByID = async ( _id: any, updates: any) =>{


            if (updates.$set ){
                delete updates.$set._id
                updates.$set._updatedTime = new Date (Date.now())
            }

            if (auth && auth.user && auth.user.staff == true )
                return unsafe.collection(collection).findOneAndUpdate( { _id: _id }, updates, {returnOriginal: false, upsert: false} )

            if (auth == unauthenticated)
                return unsafe.collection(collection).findOneAndUpdate( { _id: _id }, updates, {returnOriginal: false, upsert: false} )

            if (auth == null)
                return null

            let result = await unsafe.collection(collection).findOneAndUpdate( { _id: _id, _auth: auth.user._id }, updates, {returnOriginal: false, upsert: false} )

            if (result == null && auth.user.parentCustomer){
                result = await unsafe.collection(collection).findOneAndUpdate( { _id: _id, _auth: auth.user.parentCustomer }, updates, {returnOriginal: false, upsert: false} )
            }

            return result
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        return {         
            aggregate: async (pipline :any)=>{
                const cat = unsafe.collection(collection).aggregate (pipline)
                return (cat)
            },


            findManyByUnsafeSearchWithCount: async (search: any, skip: number = 0, limit: number = 0, sort :any = null)  =>{
                const x = await unsafe.collection(collection).aggregate([
                    { "$facet": {
                    "totalData": [
                        { "$match": search},
                        { "$skip": skip },
                        /*{ "$limit": limit }*/
                      ],
                      "totalCount": [
                        { "$count": "count" }
                    ]
                    }}
                ])
                
                
                return (x)
            },
            


            findManyByUnsafeSearch: async (search: any, skip: number = null, limit: number = null, sort :any = null)  =>{
                const _skip = skip || 0
                const _limit = limit || 0
                const _sort = sort || null

                const result = (_sort == null) ? await unsafe.collection(collection).find (search).skip(_skip).limit(_limit) : await unsafe.collection(collection).find (search).sort(_sort).skip(_skip).limit(_limit)
                return result.toArray()
            },
            
            findOneByUnsafeSearch: async (args :any) => {                
                const result= await unsafe.collection(collection).findOne (args)
                return checkReadAccess (result)
            },

                        
            findOneByID: async ( _id :any ) => {
                const result= await unsafe.collection(collection).findOne ({_id : _id})
                return checkReadAccess ( result)
            },

            findOneByEncryptedID: async (_id : string) => {
                let decryptedId = await decryptString(_id)                   
                
                if (decryptedId.indexOf(':') > 0) {
                    const [_prefix, id] =decryptedId.split(':')
                    decryptedId = id
                }


                const result = await unsafe.collection(collection).findOne ({_id : ObjectId (decryptedId)})
                
                result.__id = result._id
                result._id = _id 
                return checkReadAccess ( result)
            },                   
       
            insertOne: async ( create: any )=> {        
                for (let field in table){
                  const defaultValue = (table[field].default !== null) ? table[field].default : null
                  if (field[0] != '_' && create[field] == null && defaultValue != null  )
                    create[field] = defaultValue
                }
        
                if (auth != unauthenticated && create.__auth == null)
                    create._auth = auth.user._id
                /*if (auth && auth.user && auth.user.parentCustomer)
                    create._auth = ObjectId (auth.user.parentCustomer)*/
        
                for (let parent in table._$parents){
                    const parentId =  create[parent]
                    
                    if (create[parent]) 
                      create[parent] = parentId
                }        
                
                delete create._id
                create._createdTime = new Date (Date.now())
                const result = await unsafe.collection(collection).insertOne( create )
                if (result.insertedCount == 0)
                    throw new Error('error inserting data')                    
                
                
                create._id = result.insertedId
        
                for (var parent in table._$parents)
                {
                    if (!create[parent])    
                        continue
                    const [parentTableName, parentRow] = table._$parents[parent].split ('.')
                    const [parentId, provider] = `${create[parent]}`.split(':')                                        
                    
                    const providerName = provider || ''
                    const append = {}
                    append[parentRow] = create._id                        
    
                    const parentCollection = registeredTables[parentTableName]._$table+providerName                    
                    const safeID = ObjectId.isValid (create[parent] == true) ? create[parent] : ObjectId(parentId)
            
                    unsafe.collection(parentCollection).updateOne ( {_id : safeID} , {$push : append })
                
                }        
                return create
            },

            updateOneByUnsafeSearch: async (find:any, updates :any) => {
                if (updates.$set ){
                    delete updates.$set._id
                    updates.$set._updatedTime = new Date (Date.now())
                }

                const result = await unsafe.collection(collection).findOneAndUpdate( find, updates, {returnOriginal: false, upsert: false} )
                return result.value
            },

            updateOneByID: async ( _id: any, updates :any) => {
                           
                const result = await secureUpdateOneByID(  _id,  updates )
                return result.value
            },
            
            updateOneByEncryptedID: async (_id : string, updates: any) => {
                const  decryptedId = await decryptString(_id)   
                
                const result = await secureUpdateOneByID ( ObjectId (decryptedId) , updates)
                
                result.value.__id = result.value._id
                result.value._id = _id
                return result.value
            },

            deleteOneByID : async (_id: any) =>{                
                const result = await secureUpdateOneByID ( _id, {$set: {___deleted : true}} )
           
                            
                for (const parentTable in table._$parents) {
                        const [parentTableName, myRowInParent] = table._$parents[parentTable].split ('.')
                        const parentId =  result.value[parentTable]
            
                        if (parentId !=null )  {
            
                            const modify = {}
                            modify[myRowInParent] = result.value._id

                            const parentCollection = registeredTables[parentTableName]._$table
                            const del = await unsafe.collection(parentCollection).findOneAndUpdate ( {_id : parentId} , {$pull : modify })
                        }
            
                    }
                  return result.value
              }

        }
    }
}
