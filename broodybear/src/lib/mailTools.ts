
import { defaultEmailSender } from '../settings/settings'
import { sendEmail as _sendEmail } from './emailProviders/ses/sendEmail'


export const sendEmail = (message: any) =>{
    if (message.from == null)
        message.from = defaultEmailSender
    
    if (Array.isArray (message.to) == false) 
        message.to = [message.to]
    return _sendEmail (message)
}
