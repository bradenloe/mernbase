
import {  resetPasswordUrl, activationURL, appURL, manageURL, qrGeneratorFullUrl, rsvpURL, requestStaffUrl, userQRPrefix, lookurl, verifyurl   } from '../settings/settings'

import { encryptString } from './authenticationTools'

const activationLink = ( guestID: string, provider: string ) => {            
    const providerString = provider || ''
    const url = encryptString(`${guestID}:${providerString}`)
    return `<a ses:no-track href= '${activationURL}/${url}'>Activate</a>`
}    

const staffRequestLink  = (userID: string) => {
    const url = encryptString(`${userID}`)
    return `<a href= '${requestStaffUrl}/${url}'>Accept</a>`
}

const appLink  = (guestID: string) => {
    const url = encryptString(`${guestID}`)
    return `<a ses:no-track href= '${appURL}/${url}'>Accept</a>`
}

const manageLink = (guestID: string) => {
    const url = encryptString(`${guestID}`)    
    
    return `<a ses:no-track href= '${manageURL}/${url}'>Manage your ticket</a>`
}

const qrCode = ( id: string) => {    
    const url = encryptString(id)    
    return `<img  src = '${qrGeneratorFullUrl}/${url}' alt='qr' title='qr' style='display:block' width='150' height='150'/>`
}

const rsvpAccept = (guestID: string) => {
    const url = encryptString(`rsvpAccept:${guestID}`)
    return `<a ses:no-track href= '${rsvpURL}/${url}'>Accept</a>`
}

const rsvpDeny = ( guestID: string ) => {
    const url = encryptString(`rsvpDecline:${guestID}`)
    return `<a ses:no-track href= '${rsvpURL}/${url}'>Decline</a>`
}

const subscribeCustomer = (guestID: string) => {
    const url = encryptString(`s${guestID}`)
    return `<a href= '${rsvpURL}/${url}'>Subscribe</a>`
}



export const templateCustomer = (message: string, user: any) => {
    const _id = user.__id  || user._id

    const qr = userQRPrefix+':'+ _id 

    if (message == null || user ==null) return message
    message = message.replace(/{!userLastName!}/g, user.lastName)
    message = message.replace(/{!userFirstName!}/g, user.firstName)
    message = message.replace(/{!userCompany!}/g, user.company)
    message = message.replace(/{!userPhone!}/g, user.phoneNumber)
    message = message.replace(/{!userAddress!}/g, user.address)
    message = message.replace(/{!userEmail!}/g, user.email)
    message = message.replace(/{!userPostCode!}/g, user.postCode)
    message = message.replace(/{!userJob!}/g, user.job)
    message = message.replace(/{!userPhoneNumber!}/g, user.phoneNumber)
    message = message.replace(/{!userSite!}/g, user.site)
    message = message.replace(/{!userState!}/g, user.state)
    message = message.replace(/{!userQR!}/g, qrCode (qr ))
    message = message.replace(/{!appLink!}/g, appLink(user._id))    
    return message 
}

export const templateGuest = (message: string, guest: any) => {
    if (message == null || guest == null)   return message
    message = message.replace(/{!guestLastName!}/g, guest.lastName)
    message = message.replace(/{!guestFirstName!}/g, guest.firstName)
    message = message.replace(/{!guestEmail!}/g, guest.email)

    message = message.replace(/{!qrCode!}/g, qrCode(guest._id))        
    message = message.replace(/{!guestLink!}/g, manageLink(guest._id))    
    message = message.replace(/{!ticketLink!}/g, manageLink(guest._id))
    message = message.replace(/{!rsvpAccept!}/g, rsvpAccept(guest._id))
    message = message.replace(/{!rsvpDecline!}/g, rsvpDeny(guest._id))
    return message
}

export const templateEvent = (message: string, event: any) => {
    if (message == null || event == null)   return message
    message = message.replace(/{!eventName!}/g, event.name)
    message = message.replace(/{!eventDescription!}/g, event.description)
    message = message.replace(/{!eventAddress!}/g, event.description)
    message = message.replace(/{!eventCategory!}/g, event.category)
    message = message.replace(/{!eventCompany!}/g, event.company)
    message = message.replace(/{!eventEndTime!}/g, event.endTime)
    message = message.replace(/{!eventGenre!}/g, event.genre)
    message = message.replace(/{!eventImage!}/g, `<img src='${event.image}'/>`)
    message = message.replace(/{!eventMaxGuests!}/g, event.maxGuests)
    message = message.replace(/{!eventTypology!}/g, event.typology)
    message = message.replace(/{!eventVenue!}/g, event.venue)    
    return message
}




//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const templateActivationEmail = (message :string, user: any, provider: any) =>{
    message = templateCustomer (message, user)    
    message = message.replace(/{!activationLink!}/g, activationLink(user._id, provider))    
    return message
}

export const templateStaffRequest = (message :string, user: any) =>{
    message = templateCustomer (message, user)    
    message = message.replace(/{!staffRequestLink!}/g, staffRequestLink(user._id))    
    return message
}


export const templateRSVPMessage = (message: any, guest: any, user: any ) => {
    message = templateGuest (message, guest)
    message = templateCustomer (message, user)
    return (message)
    
}

export const templatePasswordResetEmail = (message: any, token: string) =>{
    const url =`<a href= "${resetPasswordUrl}/${token}">Reset</a>`
    return message.replace(/{!resetPasswordLink!}/g, url )
}