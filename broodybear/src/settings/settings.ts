export const serverURL = process.env.SERVER_URL || "http://localhost:9001"
export const clientURL = process.env.CLIENT_URL || "http://localhost:3000"
export const serverPort = process.env.PORT || 9001;

export const encryptionKey= process.env.SECRET || "4K7.da7<84EoT84" 
export const JWT_KEY = process.env.JWT_KEY || 'bluP803F.N76GFD1GK'

const dbUser = process.env.MONGODB_USERNAME || null
const dbPassword = process.env.MONGODB_PASSWORD || null
const dbServer = process.env.MONGODB_SERVER || 'localhost'
export const dbName = 'nordrassil'
const mongoAuth = process.env.MONGODB_CONNECTION_URI || `mongodb://${dbUser}:${dbPassword}@${dbServer}:27017/${dbName}`
const mongoUnath = process.env.MONGODB_CONNECTION_URI || `mongodb://${dbServer}:27017/${dbName}`
export const mongoDbUri = (dbUser === null) ? mongoUnath : mongoAuth


export const redisServer  = process.env.REDIS_SERVER || 'localhost'
export const redisPort  = process.env.REDIS_PORT || '6379'
export const redisAuth  = process.env.REDIS_AUTH || 'password'

//AWS Settings
const awsSESCallback = process.env.SES_ROUTE || 'aws' 
export const awsSESCallbackURL = `/${awsSESCallback}`

const razorPayCallback = process.env.RAZORPAY_ROUTE || 'razorpay'
export const razorPayCallbackURL = `/${razorPayCallback}`

//Default Email Sender
export const defaultEmailSender  = process.env.DEFAULT_EMAIL || 'support@directblu.com'


//Activation Settings
export const activationSender = process.env.ACCOUNT_RECOVERY_EMAIL || defaultEmailSender
export const activateionMessage='Activate your account, thank you {!activationLink!}'
export const activationSubject='Activate your account'
export const activationURL=`${clientURL}/activate`

export const recruiterURL=`${clientURL}/recruiterVerify`

//Password Reset Settings
export const resetPasswordMessage = 'Reset your password, thank you {!resetPasswordLink!} '
export const resetPasswordSender = defaultEmailSender
export const resetPasswordSubject = "Password Reset"
export const resetPasswordExpireTime = 600000
export const resetPasswordUrl = `${clientURL}/resetPassword`

//Staff Request and Activation
export const requestStaffMesage = 'Please here to accept {!staffRequestLink!}'
export const requestStaffSubject = 'Request to join a company'
export const requestStaffUrl = `${clientURL}/activatestaff`

//Subscribe, Rsvp Settings
export const appURL=`${clientURL}/activateGuest`
export const rsvpURL = `${clientURL}/rsvp`
export const manageURL=`${clientURL}/ticket`

//QR code settings
const qrGenerator = process.env.QRGEN_ROUTE || 'qr' 
export const qrGeneratorUrl = `/${qrGenerator}`
export const qrGeneratorFullUrl = serverURL + qrGeneratorUrl

export const userQRPrefix = 'cQR'
export const productQRPrefix = 'pQR'

export const lookurl = `${clientURL}/look`
export const verifyurl = `${clientURL}/verifyRecruiter`

export const AWS_SES = {
    apiVersion: '2010-12-01',
    accessKeyId: process.env.SES_ACCESSID || 'AKIA4BFVFLTDX2ULLVLO',
    secretAccessKey: process.env.SES_ACCESSKEY ||'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',
    region: process.env.SES_REGION || 'ap-southeast-1'
}

export const emailFromSeeker = "default looksend"

export const emailFromRecruiter = "default emailLookRequest"

export const emailVerifyRecruiter = "default emailVerify"