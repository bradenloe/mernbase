export declare const setValue: (key: string, value: string) => void;
export declare const getValue: (key: string) => Promise<string | null>;
export declare const deleteValue: (key: string) => void;
