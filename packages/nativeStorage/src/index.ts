import AsyncStorage from '@react-native-async-storage/async-storage'

export const setValue = (key: string, value: string) => {
	AsyncStorage.setItem(`@storage:${key}`, value)
}

export const getValue = async (key: string) => {
	const value = await AsyncStorage.getItem(key)
	return value
}

export const deleteValue = (key: string) => {
	AsyncStorage.removeItem(key)
}



const hexDecode = (str: any) =>{  
    const hexes = str.match(/.{1,4}/g) || [];
    let back = "";
    for(let j = 0; j<hexes.length; j++) {
        back += String.fromCharCode(parseInt(hexes[j], 16));
    }
  
    return back;
  }

const hexEncode = (str: any) => {
    let result = "";
    for (let i=0; i<str.length; i++) {
        const hex = str.charCodeAt(i).toString(16);
        result += ("000"+hex).slice(-4);
    }
    return result
}

export const setValueJson =( key: string, value: string) => {
    setValueHex (key, JSON.stringify(value))
}

export const getValueJson = async (key: string) =>{
    const value = await getValueHex (key)
    if (value)
        return (JSON.parse (value)) as any
    return null
}


export const setValueHex = ( key: string, value: string) => {
    const encoded = hexEncode (value)
    return setValue (key, encoded)
}


export const getValueHex = async (key: string) => {
    const value = await getValue (key)
    if (value)
        return hexDecode (value)
    return null
}


/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////

export const setCookieJson = setValueJson
export const getCookieJson = getValueJson
export const setCookieHex = setValueHex
export const getCookieHex = getValueHex
export const setCookie = setValue
export const getCookie = getValue
export const deleteCookie = deleteValue

