
const hexDecode = (str: any) =>{  
    const hexes = str.match(/.{1,4}/g) || [];
    let back = "";
    for(let j = 0; j<hexes.length; j++) {
        back += String.fromCharCode(parseInt(hexes[j], 16));
    }
  
    return back;
  }

const hexEncode = (str: any) => {
    let result = "";
    for (let i=0; i<str.length; i++) {
        const hex = str.charCodeAt(i).toString(16);
        result += ("000"+hex).slice(-4);
    }
    return result
}

export const setValueJson =( key: string, value: string) => {
    setValueHex (key, JSON.stringify(value))
}

export const getValueJson = (key: string) =>{
    const value = getValueHex (key)
    if (value)
        return (JSON.parse (value)) as any
    return null
}


export const setValueHex = ( key: string, value: string) => {
    const encoded = hexEncode (value)
    return setValue (key, encoded)
}


export const getValueHex = (key: string) => {
    const value = getValue (key)
    if (value)
        return hexDecode (value)
    return null
}

const localStorage = window.localStorage;

export const setValue = (key: string, value: string)=>{
    return localStorage.setItem(key, value);
}
export const deleteValue = (key: string)=>{
    return localStorage.removeItem (key)
}

export const getValue = (key: string)=>{
    return localStorage.getItem (key)
}









export const setCookieJson =( key: string, value: string) => {
    setCookieHex (key, JSON.stringify(value))
}

export const getCookieJson = (key: string) =>{
    const value = getCookieHex (key)
    if (value)
        return (JSON.parse (value)) as any
    return null
}


export const setCookieHex = ( key: string, value: string) => {
    const encoded = hexEncode (value)
    return setCookie (key, encoded)
}


export const getCookieHex = (key: string) => {
    const value = getCookie (key)
    if (value)
        return hexDecode (value)
    return null
}



export const setCookie = ( key: string, value: string) => {
    const d = new Date()  
    document.cookie = `${key}=${value}; path=/ ; expires=Thu, 18 Dec ${(d.getFullYear() + 1)} 12:00:00 UTC` 
}

export const getCookie = (key: string) => {
    const cookie = document.cookie.match('(^|[^;]+)\\s*' + key + '\\s*=\\s*([^;]+)');
  
    if ( cookie )  
    {     
      const tasty = cookie.pop()
      return ( tasty === 'null') ? null: tasty
    } 
    return null
}

export const deleteCookie = (key: string) => {
    document.cookie = `${key}=null; expires = Thu, 01 Jan 1970 00:00:00 GMT"; path=/`
}

