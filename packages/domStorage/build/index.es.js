var hexDecode = function (str) {
    var hexes = str.match(/.{1,4}/g) || [];
    var back = "";
    for (var j = 0; j < hexes.length; j++) {
        back += String.fromCharCode(parseInt(hexes[j], 16));
    }
    return back;
};
var hexEncode = function (str) {
    var result = "";
    for (var i = 0; i < str.length; i++) {
        var hex = str.charCodeAt(i).toString(16);
        result += ("000" + hex).slice(-4);
    }
    return result;
};
var setValueJson = function (key, value) {
    setValueHex(key, JSON.stringify(value));
};
var getValueJson = function (key) {
    var value = getValueHex(key);
    if (value)
        return (JSON.parse(value));
    return null;
};
var setValueHex = function (key, value) {
    var encoded = hexEncode(value);
    return setValue(key, encoded);
};
var getValueHex = function (key) {
    var value = getValue(key);
    if (value)
        return hexDecode(value);
    return null;
};
var localStorage = window.localStorage;
var setValue = function (key, value) {
    return localStorage.setItem(key, value);
};
var deleteValue = function (key) {
    return localStorage.removeItem(key);
};
var getValue = function (key) {
    return localStorage.getItem(key);
};
var setCookieJson = function (key, value) {
    setCookieHex(key, JSON.stringify(value));
};
var getCookieJson = function (key) {
    var value = getCookieHex(key);
    if (value)
        return (JSON.parse(value));
    return null;
};
var setCookieHex = function (key, value) {
    var encoded = hexEncode(value);
    return setCookie(key, encoded);
};
var getCookieHex = function (key) {
    var value = getCookie(key);
    if (value)
        return hexDecode(value);
    return null;
};
var setCookie = function (key, value) {
    var d = new Date();
    document.cookie = key + "=" + value + "; path=/ ; expires=Thu, 18 Dec " + (d.getFullYear() + 1) + " 12:00:00 UTC";
};
var getCookie = function (key) {
    var cookie = document.cookie.match('(^|[^;]+)\\s*' + key + '\\s*=\\s*([^;]+)');
    if (cookie) {
        var tasty = cookie.pop();
        return (tasty === 'null') ? null : tasty;
    }
    return null;
};
var deleteCookie = function (key) {
    document.cookie = key + "=null; expires = Thu, 01 Jan 1970 00:00:00 GMT\"; path=/";
};

export { deleteCookie, deleteValue, getCookie, getCookieHex, getCookieJson, getValue, getValueHex, getValueJson, setCookie, setCookieHex, setCookieJson, setValue, setValueHex, setValueJson };
//# sourceMappingURL=index.es.js.map
