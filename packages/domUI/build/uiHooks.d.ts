/// <reference types="react" />
export declare const useStyles: (func?: any, state?: any) => any;
export declare const useAlert: () => {
    alertMessage: any;
    alert: (x: any) => boolean;
};
export declare const useTheme: () => any[];
export declare const useUser: () => {
    user: any;
    login: any;
    userQuery: any;
};
export declare const Modal: (params: any) => JSX.Element;
