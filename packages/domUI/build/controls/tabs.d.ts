import React from 'react';
import { TabsProps as _TabsProps } from '@material-ui/core/Tabs';
import { TabProps as _TabProps } from '@material-ui/core/Tab';
export interface TabsProps extends _TabsProps {
    children: any;
    init?: number;
}
export interface TabProps extends _TabProps {
    children: React.ReactNode;
}
export declare const TabContainer: (props: TabsProps) => JSX.Element;
export declare const TabsContainer: (props: TabsProps) => JSX.Element;
export declare const Tab: (props: TabProps) => JSX.Element;
