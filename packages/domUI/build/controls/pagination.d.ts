/// <reference types="react" />
interface PaginationProps {
    totalRecords: number;
    pageLimit: number;
    pageNeighbours: 0 | 1 | 2;
    onPageChange(currentPageNumber: number): void;
}
export declare const Pagination: (props: PaginationProps) => JSX.Element;
export {};
