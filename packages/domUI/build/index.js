'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var React = require('react');
var styles = require('@material-ui/core/styles');
var CssBaseline = require('@material-ui/core/CssBaseline');
var core = require('@material-ui/core');
var MuiTabs = require('@material-ui/core/Tabs');
var MuiTab = require('@material-ui/core/Tab');
var EditIcon = require('@material-ui/icons/EditOutlined');
var ChevronLeftIcon = require('@material-ui/icons/ChevronLeft');
var ChevronRightIcon = require('@material-ui/icons/ChevronRight');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

function _interopNamespace(e) {
    if (e && e.__esModule) return e;
    var n = Object.create(null);
    if (e) {
        Object.keys(e).forEach(function (k) {
            if (k !== 'default') {
                var d = Object.getOwnPropertyDescriptor(e, k);
                Object.defineProperty(n, k, d.get ? d : {
                    enumerable: true,
                    get: function () { return e[k]; }
                });
            }
        });
    }
    n["default"] = e;
    return Object.freeze(n);
}

var React__namespace = /*#__PURE__*/_interopNamespace(React);
var React__default = /*#__PURE__*/_interopDefaultLegacy(React);
var CssBaseline__default = /*#__PURE__*/_interopDefaultLegacy(CssBaseline);
var MuiTabs__default = /*#__PURE__*/_interopDefaultLegacy(MuiTabs);
var MuiTab__default = /*#__PURE__*/_interopDefaultLegacy(MuiTab);
var EditIcon__default = /*#__PURE__*/_interopDefaultLegacy(EditIcon);
var ChevronLeftIcon__default = /*#__PURE__*/_interopDefaultLegacy(ChevronLeftIcon);
var ChevronRightIcon__default = /*#__PURE__*/_interopDefaultLegacy(ChevronRightIcon);

const Mui = (params) => {
    const { theme } = params;
    return (React__namespace.createElement(styles.ThemeProvider, { theme: core.createTheme(theme) },
        React__namespace.createElement(CssBaseline__default["default"], null),
        params.children));
};
///////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////
const UIContext = React__namespace.createContext(null);
const UI = (params) => {
    const { modal, styles, themes, language, theme: initTheme, alertTimeOut, userQuery, children } = params;
    const [auth, authSet] = React__namespace.useState(null);
    const [lang, langSet] = React__namespace.useState(language);
    const [theme, themeSet] = React__namespace.useState(initTheme);
    const [alert, alertSet] = React__namespace.useState(null);
    const timeOut = alertTimeOut || 2;
    const alertSetTimer = (value) => {
        alertSet(value);
        setTimeout(() => {
            alertSet(null);
        }, timeOut * 1000);
    };
    const value = {
        auth: auth,
        authSet: authSet,
        lang: lang,
        langSet: langSet,
        theme: theme,
        themeSet: themeSet,
        alert: alert,
        alertSet: alertSetTimer,
        userQuery: userQuery,
        styles: styles || {},
        modal: modal,
    };
    const muitheme = themes[initTheme];
    return (React__namespace.createElement(UIContext.Provider, { value: value },
        React__namespace.createElement(Mui, { theme: muitheme }, children)));
};

const useStyles = (func = {}, state = null) => {
    const { theme, styles } = React__namespace.useContext(UIContext);
    const vw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);
    const vh = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0);
    if (typeof func === 'function') {
        return ({
            theme: theme,
            state: state,
            vw: vw,
            vh: vh,
            styles: Object.assign({ styles }, func({
                theme: theme,
                vw: vw,
                vh: vh,
                styles: styles
            }))
        });
    }
    if (typeof func === 'object') {
        return ({
            styles: Object.assign(Object.assign({}, styles), func),
            theme: theme,
            vw: vw,
            vh: vh
        });
    }
    return ({ styles: styles });
};
const useAlert = () => {
    const { alert, alertSet } = React__namespace.useContext(UIContext);
    const set = (x) => {
        alertSet(x);
        return (x !== null);
    };
    return ({ alertMessage: alert, alert: set });
};
const useTheme = () => {
    const { theme, themeSet } = React__namespace.useContext(UIContext);
    return ([theme, themeSet]);
};
const useUser = () => {
    const { auth, authSet, userQuery } = React__namespace.useContext(UIContext);
    return ({ user: auth, login: authSet, userQuery: userQuery });
};
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
const Modal = (params) => {
    const { modal: UserModal } = React__namespace.useContext(UIContext);
    if (UserModal == null)
        return React__namespace.createElement(React__namespace.Fragment, null);
    return (React__namespace.createElement(UserModal, Object.assign({}, params)));
};

const LabeledInput = (params) => {
    const { value, label, onChange } = params;
    return (React__namespace.createElement(React__namespace.Fragment, null,
        React__namespace.createElement(core.Box, null,
            React__namespace.createElement(core.Typography, null, label),
            React__namespace.createElement(core.Input, { value: value, onChange: onChange }))));
};

/*! *****************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

const pageStyles$1 = {
    root: {
        minWidth: '0',
        paddingLeft: '30px',
        paddingRight: '30px'
    }
};
const TabContainer = (props) => {
    const { children, init } = props, more = __rest(props, ["children", "init"]);
    const initTab = init || 0;
    const [tab, SetTab] = React__default["default"].useState(initTab);
    const { styles } = useStyles(pageStyles$1);
    const handleTabClick = (tabIndex, onClick) => {
        SetTab(tabIndex);
        if (onClick)
            onClick();
    };
    return (React__default["default"].createElement(React__default["default"].Fragment, null,
        React__default["default"].createElement(MuiTabs__default["default"], Object.assign({ value: tab }, more, { TabIndicatorProps: { children: React__default["default"].createElement("span", null) } }), React__default["default"].Children.map(children, (child, index) => {
            const { props } = child;
            const { children, onClick } = props, more = __rest(props, ["children", "onClick"]);
            return (React__default["default"].createElement(MuiTab__default["default"], Object.assign({ sx: styles.root, onClick: () => handleTabClick(index, onClick && onClick) }, more)));
        })),
        React__default["default"].createElement(React__default["default"].Fragment, null, React__default["default"].Children.map(children, (child, index) => {
            if (index === tab)
                return child;
            return (React__default["default"].createElement(React__default["default"].Fragment, null));
        }))));
};
const Tab = (props) => {
    const { children } = props;
    return (React__default["default"].createElement(React__default["default"].Fragment, null, children));
};

const pageStyles = (() => ({
    image: {
        border: `2px dashed `,
        boxSizing: 'border-box',
        borderRadius: '16px'
    },
    checkIcon: {
        backgroundColor: ``,
    },
    addIcon: {
        fontSize: '2rem',
        color: 'grey'
    },
    imageInput: {
        opacity: 0,
        width: '100%',
        cursor: 'pointer',
        height: '100%',
        position: 'absolute'
    },
    checkIconColor: {
        color: '#ffffff'
    },
    root: {
        display: 'flex',
        position: 'relative',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        width: '35vw',
        height: '162px',
        border: `2px dashed`,
        boxSizing: 'border-box',
        borderRadius: '16px',
        background: 'white'
    },
}));
const ImageUploader = (props) => {
    const { onChange, value, label } = props;
    const [imageString, setImageString] = React__default["default"].useState(value);
    const { styles } = useStyles(pageStyles);
    React__default["default"].useEffect(() => {
        setImageString(value);
    }, [value]);
    const onfileupload = (e) => {
        const reader = new FileReader();
        reader.onloadend = () => {
            const image = reader.result;
            setImageString(image);
            onChange(image);
        };
        reader.readAsDataURL(e.target.files[0]);
    };
    const handleEditImage = () => {
        setImageString('');
    };
    return (React__default["default"].createElement(React__default["default"].Fragment, null,
        (!imageString) && React__default["default"].createElement(core.Box, { display: 'none', flexDirection: 'row' }),
        (imageString) &&
            React__default["default"].createElement(core.Box, { display: 'flex', flexDirection: 'row' },
                React__default["default"].createElement("img", { width: "236px", alt: 'todo-item', height: "162px", className: styles.image, src: imageString }),
                React__default["default"].createElement(core.Box, { display: 'flex', ml: 5, alignItems: 'center', justifyContent: 'center', flexDirection: 'column' },
                    React__default["default"].createElement(core.Box, { display: 'flex', alignItems: 'center', justifyContent: 'center' },
                        React__default["default"].createElement(core.IconButton, { "aria-label": "edit", onClick: handleEditImage },
                            React__default["default"].createElement(EditIcon__default["default"], { color: 'primary' }))),
                    React__default["default"].createElement(core.Typography, null, "Edit Image"))),
        React__default["default"].createElement(core.Box, null,
            React__default["default"].createElement(core.Box, null, "+"),
            React__default["default"].createElement(core.Typography, null,
                "Drag and Drop/Insert\u00A0",
                label),
            React__default["default"].createElement("input", { type: "file", className: styles.imageInput, onChange: onfileupload }))));
};

const HtmlView = (params) => {
    const { value } = params;
    if (value === '' || value === null)
        return (React__namespace.createElement(React__namespace.Fragment, null));
    try {
        const { html } = JSON.parse(value);
        return (React__namespace.createElement("div", { dangerouslySetInnerHTML: { __html: html } }));
    }
    catch (_a) {
        return (React__namespace.createElement("div", { dangerouslySetInnerHTML: { __html: value } }));
    }
};

const LEFT_PAGE = 'LEFT';
const RIGHT_PAGE = 'RIGHT';
/**
 * Helper function for creating a range of numbers
 * range(1, 5) => [1, 2, 3, 4, 5]
 */
const range = (from, to, step = 1) => {
    let i = from;
    const range = [];
    while (i <= to) {
        range.push(i);
        i += step;
    }
    return range;
};
const Pagination = (props) => {
    const { totalRecords = 0, pageLimit, pageNeighbours = 0, onPageChange } = props;
    const [currentPage, currentPageSet] = React__default["default"].useState(1);
    const totalPages = Math.ceil(totalRecords / pageLimit);
    const Neighbours = pageNeighbours
        ? Math.max(0, Math.min(pageNeighbours, 2))
        : 0;
    const pageNumbers = () => {
        /**
         * totalNumbers: the total page numbers to show on the control
         * totalBlocks: totalNumbers + 2 to cover for the left(<) and right(>) controls
         */
        const totalNumbers = Neighbours * 2 + 3;
        const totalBlocks = totalNumbers + 2;
        if (totalPages > totalBlocks) {
            const startPage = Math.max(2, currentPage - pageNeighbours);
            const endPage = Math.min(totalPages - 1, currentPage + pageNeighbours);
            let pages = range(startPage, endPage);
            /**
             * hasLeftSpill: has hidden pages to the left
             * hasRightSpill: has hidden pages to the right
             * spillOffset: number of hidden pages either to the left or to the right
             */
            const hasLeftSpill = startPage > 2;
            const hasRightSpill = totalPages - endPage > 1;
            const spillOffset = totalNumbers - (pages.length + 1);
            switch (true) {
                // handle: (1) < {5 6} [7] {8 9} (10)
                case hasLeftSpill && !hasRightSpill: {
                    const extraPages = range(startPage - spillOffset, startPage - 1);
                    pages = [LEFT_PAGE, ...extraPages, ...pages];
                    break;
                }
                // handle: (1) {2 3} [4] {5 6} > (10)
                case !hasLeftSpill && hasRightSpill: {
                    const extraPages = range(endPage + 1, endPage + spillOffset);
                    pages = [...pages, ...extraPages, RIGHT_PAGE];
                    break;
                }
                // handle: (1) < {4 5} [6] {7 8} > (10)
                case hasLeftSpill && hasRightSpill:
                default: {
                    pages = [LEFT_PAGE, ...pages, RIGHT_PAGE];
                    break;
                }
            }
            return [1, ...pages, totalPages];
        }
        return range(1, totalPages);
    };
    const pages = pageNumbers();
    const gotoPage = (page) => {
        const currentPage = Math.max(0, Math.min(page, totalPages));
        currentPageSet(currentPage);
        onPageChange(page);
    };
    const handlePageClick = (page) => {
        gotoPage(page);
    };
    const handleMoveLeft = () => {
        gotoPage(currentPage - pageNeighbours * 2 - 1);
    };
    const handleMoveRight = () => {
        gotoPage(currentPage + pageNeighbours * 2 + 1);
    };
    return (React__default["default"].createElement(core.Grid, { direction: 'row' }, pages.map((page, index) => {
        if (page === LEFT_PAGE)
            return (React__default["default"].createElement(core.IconButton, { "aria-label": 'edit', onClick: handleMoveLeft, key: index },
                React__default["default"].createElement(ChevronLeftIcon__default["default"], { color: 'secondary' })));
        if (page === RIGHT_PAGE)
            return (React__default["default"].createElement(core.IconButton, { "aria-label": 'edit', onClick: handleMoveRight, key: index },
                React__default["default"].createElement(ChevronRightIcon__default["default"], { color: 'secondary' })));
        return (React__default["default"].createElement(core.Button, { color: page === currentPage ? 'primary' : 'secondary', onClick: () => handlePageClick(page), key: index, id: `pagination-number-${index + 1}-button` }, page));
    })));
};

const w = window;
const useIFrame = (initstate) => {
    const [id] = React__namespace.useState(`${Date.now()}${Math.random()}`);
    const [value, valueSet] = React__namespace.useState(initstate);
    const hook = (event) => {
        if (event.data && event.data.value && event.data.auth == id)
            valueSet(event.data.value);
    };
    React__namespace.useEffect(() => {
        w.addEventListener("message", hook, false);
        w.parent.postMessage({
            init: true,
            auth: id
        }, '*');
        return (() => {
            w.removeEventListener("message", hook, false);
        });
    }, []);
    const onchange = (value) => {
        w.parent.postMessage({
            onChange: value
        }, '*');
        valueSet(value);
    };
    return ([value, onchange]);
};
const IFrame = (params) => {
    const { value, onChange } = params, more = __rest(params, ["value", "onChange"]);
    const [{ auth, handle }, setFrame] = React__namespace.useState({ auth: null, handle: null });
    const hook = (event) => {
        if (event && event.data && event.data.init == true && event.data.auth) {
            setFrame({ auth: auth, handle: event.source });
            event.source.postMessage({ value: value, auth: event.data.auth });
        }
        if (event.data && event.data.onChange)
            onChange(event.data.onChange);
    };
    React__namespace.useEffect(() => {
        if (auth && handle) {
            handle.postMessage({
                value: value,
                auth: auth
            });
        }
    }, [value]);
    React__namespace.useEffect(() => {
        w.addEventListener("message", hook, false);
        return (() => {
            w.removeEventListener("message", hook, false);
        });
    }, []);
    return React__namespace.createElement("iframe", Object.assign({}, more));
};

exports.HtmlView = HtmlView;
exports.IFrame = IFrame;
exports.ImageUploader = ImageUploader;
exports.LabeledInput = LabeledInput;
exports.Modal = Modal;
exports.Pagination = Pagination;
exports.Tab = Tab;
exports.TabContainer = TabContainer;
exports.UI = UI;
exports.useAlert = useAlert;
exports.useIFrame = useIFrame;
exports.useStyles = useStyles;
exports.useTheme = useTheme;
exports.useUser = useUser;
//# sourceMappingURL=index.js.map
