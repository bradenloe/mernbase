import * as React from 'react';
export { createSvgIcon } from '@material-ui/core';
export declare const Mui: (params: any) => JSX.Element;
export declare const UIContext: React.Context<any>;
export declare const UI: (params: any) => JSX.Element;
