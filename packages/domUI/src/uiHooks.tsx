import * as React from 'react';
import { UIContext } from './uiProvider'



export const useStyles= ( func = {} as any, state = null as any)=>{
    const {theme, styles} =  React.useContext(UIContext) as any	
    const vw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0)
    const vh = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0)

    if (typeof func === 'function') {
        return ({ 
            theme: theme,
            state: state,
            vw : vw,
            vh:  vh,
            styles: { styles, ...func({
                theme: theme,
                vw : vw,
                vh:  vh,
                styles: styles
            })}
        })
    }
    
    if (typeof func === 'object') {
        return ({
            styles: {...styles, ...func},
            theme : theme,
            vw : vw,
            vh:  vh
        }) as any
    }

    return ({styles: styles})
    
}

export const useAlert = ()=> {
    const {alert, alertSet} =  React.useContext(UIContext) as any

    const set = (x: any)=>{
        alertSet (x)
        return (x !== null)
    }
    
    return ({alertMessage: alert, alert: set })
}


export const useTheme = ()=>{
    const {theme, themeSet} =  React.useContext(UIContext) as any	
    
    return ([theme, themeSet])
}

export const useUser = ()=>{
    const {auth, authSet, userQuery} =  React.useContext(UIContext) as any	
    
    return ({user: auth, login:authSet, userQuery : userQuery})
}


////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

export const Modal = (params: any) => {     
    const {modal: UserModal} =  React.useContext(UIContext) as any	
    
    if (UserModal == null) 
        return <React.Fragment/>
    
    return (        
        <UserModal {...params} />        
    )                
}



