import * as React from 'react';



///////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////
import {ThemeProvider} from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline'
import {createTheme} from '@material-ui/core'

// @ts-ignore
export {createSvgIcon} from '@material-ui/core'





export const Mui = (params: any) =>{
    
    const {theme} = params
    return (
        <ThemeProvider theme={ createTheme( theme ) }>
            <CssBaseline />
            {params.children}
        </ThemeProvider>
    )
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////


export const UIContext = React.createContext (null as any)


export const UI = (params:any)=>{
    const {modal, styles, themes, language, theme: initTheme, alertTimeOut, userQuery, children} = params    

    const [ auth, authSet ] = React.useState (null)
    const [ lang, langSet ] = React.useState (language)
    const [ theme, themeSet ] = React.useState ( initTheme )
    const [ alert, alertSet ] = React.useState ( null as any) 
    

    const timeOut = alertTimeOut || 2

    const alertSetTimer = (value: string)=>{
        alertSet (value)
        setTimeout (()=>{
            alertSet (null)
        }, timeOut * 1000)
    }

    const value = {
        auth: auth,
        authSet: authSet,
        lang: lang, 
        langSet: langSet,
        theme: theme, 
        themeSet: themeSet,
        alert : alert,
        alertSet : alertSetTimer,
        userQuery : userQuery,
        styles: styles||{},
        modal : modal,        
    }

    const muitheme = themes[initTheme]
    return (
            <UIContext.Provider value={value}>                
                <Mui theme={muitheme} >
                    {children}
                </Mui>
            </UIContext.Provider>
        )
}

