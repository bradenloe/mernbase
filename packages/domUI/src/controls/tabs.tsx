import React from 'react';


import MuiTabs  from '@material-ui/core/Tabs'
import MuiTab  from '@material-ui/core/Tab'
import { TabsProps as _TabsProps }  from '@material-ui/core/Tabs';
import { TabProps as _TabProps } from '@material-ui/core/Tab';
import { useStyles } from '../uiHooks'

export interface  TabsProps extends _TabsProps{
    children: any,
    init?: number
}

export interface  TabProps extends _TabProps{
  children: React.ReactNode,
}

const pageStyles = {  
  root: {
    minWidth: '0', 
    paddingLeft: '30px',
    paddingRight: '30px'
    }
}
  

export const TabContainer = (props: TabsProps) => {
    const {children, init, ...more} = props
    const initTab = init || 0
    const [tab, SetTab] = React.useState(initTab)
    const {styles} = useStyles (pageStyles)
    
    const handleTabClick = (tabIndex: number, onClick: any) => {
      SetTab(tabIndex)
      if(onClick) onClick()
      
    }
   
    return(
      <React.Fragment>
        <MuiTabs value={tab} {...more} TabIndicatorProps={{ children: <span /> }}>   
          { React.Children.map(children, (child: React.ReactElement, index: number) => {
              const { props } = child
              const { children, onClick, ...more } = props
              return (
                <MuiTab 
                  sx={styles.root} 
                  onClick ={() => handleTabClick(index, onClick && onClick )} 
                  {...more} 
                />
              )
          })}
        </MuiTabs>
        <>
          { React.Children.map(children, (child: React.ReactNode, index: number) => {
              if(index === tab) return child
              return (<React.Fragment/>)
          })}
        </>
      </React.Fragment>
  )
}

export const TabsContainer = TabContainer

export const Tab = (props: TabProps) => {
  const { children } = props

  return(
    <>
      {children}
    </>
  )
}