import * as React from 'react'

const w = window as any

export const useIFrame = (initstate: any)=>{
    const [id] = React.useState (`${Date.now()}${Math.random()}`)
    const [value, valueSet] = React.useState(initstate)
    
    const hook= (event:any) => {      
        if (event.data && event.data.value && event.data.auth == id)
            valueSet (event.data.value)
    }

    React.useEffect (()=>{        
        w.addEventListener("message", hook, false);

        w.parent.postMessage ({
            init: true,
            auth : id
        },'*')

        return (()=>{
            w.removeEventListener("message", hook, false);
          })
    },[])

    const onchange = (value:any) =>{        
        w.parent.postMessage ({
            onChange: value
        },'*')        
        valueSet (value)
    }

    return ([value, onchange])    
}

export const IFrame = (params:any)=>{
    const {value, onChange, ...more} = params
    
    const [ {auth, handle}, setFrame] = React.useState ({auth: null, handle: null as any})
    const hook = (event:any) => {      
      if (event && event.data && event.data.init==true && event.data.auth){
        setFrame ({auth: auth, handle: event.source})
        event.source.postMessage ({ value: value, auth : event.data.auth})
      }
  
      if (event.data && event.data.onChange)
        onChange (event.data.onChange)
    }
  
    React.useEffect (()=>{
      
      if (auth && handle){
        handle.postMessage ({
          value: value,
          auth : auth
        })
      }
    }, [value])
  
    React.useEffect (()=>{     
      w.addEventListener("message", hook, false);
      return (()=>{
        w.removeEventListener("message", hook, false);
      })
    },[])
  
    return <iframe {...more}/>
  }