import React from 'react'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
import ChevronRightIcon from '@material-ui/icons/ChevronRight'
import {IconButton, Button, Grid} from '@material-ui/core'

interface PaginationProps {
  totalRecords: number;
  pageLimit: number;
  pageNeighbours: 0 | 1 | 2;
  onPageChange(currentPageNumber: number): void;
}

const LEFT_PAGE = 'LEFT'
const RIGHT_PAGE = 'RIGHT'

/**
 * Helper function for creating a range of numbers
 * range(1, 5) => [1, 2, 3, 4, 5]
 */
const range = (from: any, to: any, step = 1) => {
  let i = from
  const range = []
  while (i <= to) {
    range.push(i)
    i += step
  }
  return range
}

export const Pagination = (props: PaginationProps) => {
  const {totalRecords = 0, pageLimit, pageNeighbours = 0, onPageChange} = props
  const [currentPage, currentPageSet] = React.useState(1)

  const totalPages = Math.ceil(totalRecords / pageLimit)
  const Neighbours = pageNeighbours
    ? Math.max(0, Math.min(pageNeighbours, 2))
    : 0

  const pageNumbers = () => {
    /**
     * totalNumbers: the total page numbers to show on the control
     * totalBlocks: totalNumbers + 2 to cover for the left(<) and right(>) controls
     */
    const totalNumbers = Neighbours * 2 + 3
    const totalBlocks = totalNumbers + 2
    if (totalPages > totalBlocks) {
      const startPage = Math.max(2, currentPage - pageNeighbours)
      const endPage = Math.min(totalPages - 1, currentPage + pageNeighbours)
      let pages = range(startPage, endPage)
      /**
       * hasLeftSpill: has hidden pages to the left
       * hasRightSpill: has hidden pages to the right
       * spillOffset: number of hidden pages either to the left or to the right
       */
      const hasLeftSpill = startPage > 2
      const hasRightSpill = totalPages - endPage > 1
      const spillOffset = totalNumbers - (pages.length + 1)

      switch (true) {
        // handle: (1) < {5 6} [7] {8 9} (10)
        case hasLeftSpill && !hasRightSpill: {
          const extraPages = range(startPage - spillOffset, startPage - 1)
          pages = [LEFT_PAGE, ...extraPages, ...pages]
          break
        }

        // handle: (1) {2 3} [4] {5 6} > (10)
        case !hasLeftSpill && hasRightSpill: {
          const extraPages = range(endPage + 1, endPage + spillOffset)
          pages = [...pages, ...extraPages, RIGHT_PAGE]
          break
        }

        // handle: (1) < {4 5} [6] {7 8} > (10)
        case hasLeftSpill && hasRightSpill:
        default: {
          pages = [LEFT_PAGE, ...pages, RIGHT_PAGE]
          break
        }
      }

      return [1, ...pages, totalPages]
    }

    return range(1, totalPages)
  }

  const pages = pageNumbers()

  const gotoPage = (page: any) => {
    const currentPage = Math.max(0, Math.min(page, totalPages))
    currentPageSet(currentPage)
    onPageChange(page)
  }

  const handlePageClick = (page: number) => {
    gotoPage(page)
  }

  const handleMoveLeft = () => {
    gotoPage(currentPage - pageNeighbours * 2 - 1)
  }

  const handleMoveRight = () => {
    gotoPage(currentPage + pageNeighbours * 2 + 1)
  }

  return (
    <Grid direction='row'>
      {pages.map((page: any, index: number) => {
        if (page === LEFT_PAGE)
          return (
            <IconButton aria-label='edit' onClick={handleMoveLeft} key={index}>
              <ChevronLeftIcon color='secondary' />
            </IconButton>
          )
        if (page === RIGHT_PAGE)
          return (
            <IconButton aria-label='edit' onClick={handleMoveRight} key={index}>
              <ChevronRightIcon color='secondary' />
            </IconButton>
          )
        return (
          <Button
            color={page === currentPage ? 'primary' : 'secondary'}
            onClick={() => handlePageClick(page)}
            key={index}
            id={`pagination-number-${index + 1}-button`}>
            {page}
          </Button>
        )
      })}
    </Grid>
  )
}

