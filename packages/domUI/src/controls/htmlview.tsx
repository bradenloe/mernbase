import * as React from 'react'

export const HtmlView = (params: any) => {
    const {value} = params
    
    if (value === '' || value === null)
      return (<React.Fragment/>)
    
    try {    
      const { html }  = JSON.parse(value)          
      return (<div dangerouslySetInnerHTML={{__html: html }} />)    
    }
    catch {
      return (<div dangerouslySetInnerHTML={{__html: value }} />)    
    }  
  }