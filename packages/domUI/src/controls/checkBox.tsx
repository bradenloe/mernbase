import React from 'react';
import { Checkbox as _CheckBox  } from '@material-ui/core'
import CircleCheckedFilled from '@material-ui/icons/CheckCircle'
import CircleUnchecked from '@material-ui/icons/RadioButtonUnchecked'

export const CheckBox = (params: any)=>{
    const {onChange, value, options } = params
    const _true = (options && options.true)  ? options.true: true
    const _false = (options && options.false !== undefined)  ? options.false: false
  
  
    const checked = (value ===_true ) ? true : false
    const onchange = (e: any) =>{
      
      if (e && e.target &&e.target.checked )
        return onChange (_true)
      onChange (_false)
    }
  
    return( <_CheckBox  
        icon={<CircleUnchecked />}
        checkedIcon={<CircleCheckedFilled />}
        color='primary' 
        checked={checked} 
        onChange={onchange} />)
  }