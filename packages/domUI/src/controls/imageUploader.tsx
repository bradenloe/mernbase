import React from 'react';
import  { Box, Typography, IconButton } from '@material-ui/core'
import EditIcon from '@material-ui/icons/EditOutlined'


import { useStyles } from '../uiHooks'

const pageStyles = (() => ({
  image: {
    border: `2px dashed `,
    boxSizing: 'border-box',
    borderRadius: '16px'
  },
  checkIcon: {
    backgroundColor: ``,
  },
  addIcon: {
    fontSize: '2rem', 
    color: 'grey'
  },
  imageInput:  {
    opacity: 0,
    width: '100%',
    cursor: 'pointer',
    height: '100%',
    position: 'absolute'
  },
  checkIconColor:{
    color: '#ffffff'
  },
  root: {
    display: 'flex',  
    position: 'relative', 
    flexDirection: 'column', 
    alignItems: 'center', 
    justifyContent: 'center',
    width: '35vw', 
    height: '162px', 
    border: `2px dashed`, 
    boxSizing: 'border-box', 
    borderRadius: '16px', 
    background: 'white'
  }, 
}));

  
  export const ImageUploader = (props: any) => {
    const {  onChange, value, label } = props
    const [ imageString, setImageString ] = React.useState(value)
    
    const { styles } = useStyles(pageStyles)    

    React.useEffect (()=>{
      setImageString (value)
    },[value])
    
    const onfileupload = ( e:any ) => {
        const reader = new FileReader();
        
        reader.onloadend =  () => {
           const image = reader.result
           setImageString(image)
           onChange (image)
        }        
        reader.readAsDataURL(e.target.files[0])
     }    
    

    const handleEditImage = () => {
      setImageString('')
    }
    

    return (        
        <>
            {( !imageString ) && <Box display='none' flexDirection='row' ></Box>} 

            {( imageString ) &&
              <Box display='flex' flexDirection='row' >
                <img width="236px" alt='todo-item' height="162px" className={styles.image} src={imageString} />
                <Box display='flex' ml={5} alignItems='center' justifyContent='center' flexDirection='column'>
                    <Box display='flex' alignItems='center' justifyContent='center'>
                    <IconButton aria-label="edit" onClick={handleEditImage}>
                      <EditIcon color='primary' />
                    </IconButton>
                  </Box>
                  <Typography>Edit Image</Typography>
                </Box>
                </Box>
            }
          
          
          <Box >
            <Box >&#43;</Box>
            <Typography>Drag and Drop/Insert&nbsp;{label}</Typography>
            <input type="file" className={styles.imageInput} onChange={onfileupload}  />
          </Box>
        </>        
      ) 
  }