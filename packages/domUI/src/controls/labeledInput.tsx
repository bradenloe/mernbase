import * as React from 'react'
import { Input, Box, Typography } from '@material-ui/core';

export const LabeledInput = (params: any)=>{
	const { value, label, onChange } = params
	return (
        <React.Fragment>
		    <Box>
			    <Typography>{label}</Typography>
			    <Input value={value} onChange={onChange} />
		    </Box>
        </React.Fragment>
		)
}