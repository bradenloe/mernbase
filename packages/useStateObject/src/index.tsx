import * as React from 'react'


const getvalue = (e: any) => {
  return (e && e.target && e.target.value !=null) ? e.target.value : e
}

  
  
const useState= React.useState
const useEffect = React.useEffect

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////



export const useStateHash = ( result: any, watch : any = null) =>{
  if (Array.isArray (result) != false) 
      throw new Error('use useReactArray')
  
  const [data, dataSet] = useState (result || [])
  const [changes] = useState ({} as any)

  useEffect (()=>{
    dataSet (result || [])
  }, watch || [result])
    
      const change = (field: any) =>{      
        return (e: any)=>{
          const value = getvalue (e)
                              
          
          if (changes[field] == null)
              changes[field] =  true
          
          const hash={} as any

          hash[field] = value
          dataSet ({...data, ...hash})
        }      
      } 


      const changedItems = () =>{       
        if (changes == null)
            return null          
        const newrow = {} as any
        
        for (let key in changes){            
            newrow[key]=data[key]
        }
        return newrow        
      }        
    
    const register = (field: string, defaultvalue = null as any)=>{
      return {id: field, name: field, value: data[field] || defaultvalue, onChange: change(field)}
    }

    const registerString = (field: string, defaultvalue = '' as any)=>{
      return {id: field, name: field, value: data[field] || defaultvalue, onChange: change(field)}
    }    

    const registerInt = (field: string, defaultvalue = 0 as any)=>{
      return {id: field, name: field, value: data[field] || defaultvalue, onChange: change(field)}
    }    

    

    return {register: register, integer: registerInt, string: registerString, state: data, update: change, setState: dataSet ,changes: changedItems} as any
    
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const useStateArray = ( result: any =null, watch : any =null )=>{
  if (Array.isArray (result) == false) 
      throw new Error('use useReactHash')

  
  
  const [state, dataSet] = useState (result || [] as any)
  const [changes] = useState ([] as any)

  const data = [...state]


  useEffect (()=>{
    dataSet (result)
  }, watch || [result])
  
    Object.defineProperty(data, "push", { enumerable: false,  configurable: true,  writable: false,  value: (newitem: any) =>{
        dataSet ([...data, newitem])
        return (Array.prototype.push.call(data, newitem))
      }
    })


    Object.defineProperty(data, "render", { enumerable: false, configurable: true, writable: false, value: (callback: any) =>{

        return ( Array.prototype.map.call (data, (item: any, index: number)=>{            

            const change = (field: any) =>{      
              return (e: any)=>{
                const value = getvalue (e)

                if (changes[index] == null)
                  changes[index] = {}                            
                  
                if (changes[index][field] == null)
                    changes[index][field] =  true

                                
                data[index][field] = value
                dataSet ([...data])
              }      
            } 

            const register = (field: string, defaultvalue = null as any)=>{
              const value = (item && item[field]) || defaultvalue
              return {id: field, name: field, value: value, onChange: change(field)}
            }            

            const registerInt = (field: string, defaultvalue = 0 as any)=>{
              const value = (item && item[field]) || defaultvalue
              return {id: field, name: field, value: value, onChange: change(field)}
            }            

            const registerString = (field: string, defaultvalue = '' as any)=>{
              const value = (item && item[field]) || defaultvalue
              return {id: field, name: field, value: value, onChange: change(field)}
            }            

            return (callback({element: item, index: index, update : change, register: register, integer: registerInt, string: registerString}) )            
          })          
        )
      }
    })


    const changedItems = (row: any, index: number) =>{       
      if (changes.length == 0 || changes[index]==null || row == null)
          return null          
      const newrow = {} as any
      
      for (let key in changes[index]){            
          newrow[key]=row[key]
      }
      return newrow        
  }

    Object.defineProperty(data, "changes", { enumerable: false, configurable: true, writable: false, value: () =>{            
          if (data == null || data.length == 0 )   return null

          const changes=Array()
          Array.prototype.map.call (data, (item: any, index: number)=>{          
              const changed = changedItems(item, index)

              if (changed)
                changes.push (changed)
            
          })

          return changes
      }
    })   

    return [data, dataSet] as any
}




const updateAt = (index: number, updateFn :any, set: any) =>
  set((l:any) => {
    const copy = l.slice(0);
    const item = copy[index];
    copy[index] = updateFn(item);
    return copy;
})

export const useArray = (initialList: any) => {
  const [list, set] = useState(initialList);
  return [
    list,
    {
      set,
      empty: () => set([]),
      replace: (list: any) => set(list),
      push: (item: any) => set( (l: any) => [...l, item]),
      updateAt: (index :number, updateFn: any) => updateAt(index, updateFn, set),
      setAt: (index: number, value: any) => set( (l: any) =>
        [...l.slice(0, index), value, ...l.slice(index + 1)]
      ),
      removeAt: (index: number) => set((l: any) => [...l.slice(0, index), ...l.slice(index + 1)]),
      filter: (filterFn : any) => set((l : any) => l.filter(filterFn)),
      map: (mapFn: any) => set((l: any) => [...l].map(mapFn)),
      sort: (sortFn: any ) => set((l: any) => [...l].sort(sortFn)),
      reverse: () => set((l:any) => [...l].reverse()),
      mergeBefore: (arr: any) => set((l: any) => [...arr].concat([...l])),
      mergeAfter: (arr: any) => set((l: any) => [...l].concat([...arr])),
    }
  ];
}