export declare const useStateHash: (result: any, watch?: any) => any;
export declare const useStateArray: (result?: any, watch?: any) => any;
export declare const useArray: (initialList: any) => any[];
