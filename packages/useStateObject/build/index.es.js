import * as React from 'react';

const getvalue = (e) => {
    return (e && e.target && e.target.value != null) ? e.target.value : e;
};
const useState = React.useState;
const useEffect = React.useEffect;
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
const useStateHash = (result, watch = null) => {
    if (Array.isArray(result) != false)
        throw new Error('use useReactArray');
    const [data, dataSet] = useState(result || []);
    const [changes] = useState({});
    useEffect(() => {
        dataSet(result || []);
    }, watch || [result]);
    const change = (field) => {
        return (e) => {
            const value = getvalue(e);
            if (changes[field] == null)
                changes[field] = true;
            const hash = {};
            hash[field] = value;
            dataSet(Object.assign(Object.assign({}, data), hash));
        };
    };
    const changedItems = () => {
        if (changes == null)
            return null;
        const newrow = {};
        for (let key in changes) {
            newrow[key] = data[key];
        }
        return newrow;
    };
    const register = (field, defaultvalue = null) => {
        return { id: field, name: field, value: data[field] || defaultvalue, onChange: change(field) };
    };
    const registerString = (field, defaultvalue = '') => {
        return { id: field, name: field, value: data[field] || defaultvalue, onChange: change(field) };
    };
    const registerInt = (field, defaultvalue = 0) => {
        return { id: field, name: field, value: data[field] || defaultvalue, onChange: change(field) };
    };
    return { register: register, integer: registerInt, string: registerString, state: data, update: change, setState: dataSet, changes: changedItems };
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const useStateArray = (result = null, watch = null) => {
    if (Array.isArray(result) == false)
        throw new Error('use useReactHash');
    const [state, dataSet] = useState(result || []);
    const [changes] = useState([]);
    const data = [...state];
    useEffect(() => {
        dataSet(result);
    }, watch || [result]);
    Object.defineProperty(data, "push", { enumerable: false, configurable: true, writable: false, value: (newitem) => {
            dataSet([...data, newitem]);
            return (Array.prototype.push.call(data, newitem));
        }
    });
    Object.defineProperty(data, "render", { enumerable: false, configurable: true, writable: false, value: (callback) => {
            return (Array.prototype.map.call(data, (item, index) => {
                const change = (field) => {
                    return (e) => {
                        const value = getvalue(e);
                        if (changes[index] == null)
                            changes[index] = {};
                        if (changes[index][field] == null)
                            changes[index][field] = true;
                        data[index][field] = value;
                        dataSet([...data]);
                    };
                };
                const register = (field, defaultvalue = null) => {
                    const value = (item && item[field]) || defaultvalue;
                    return { id: field, name: field, value: value, onChange: change(field) };
                };
                const registerInt = (field, defaultvalue = 0) => {
                    const value = (item && item[field]) || defaultvalue;
                    return { id: field, name: field, value: value, onChange: change(field) };
                };
                const registerString = (field, defaultvalue = '') => {
                    const value = (item && item[field]) || defaultvalue;
                    return { id: field, name: field, value: value, onChange: change(field) };
                };
                return (callback({ element: item, index: index, update: change, register: register, integer: registerInt, string: registerString }));
            }));
        }
    });
    const changedItems = (row, index) => {
        if (changes.length == 0 || changes[index] == null || row == null)
            return null;
        const newrow = {};
        for (let key in changes[index]) {
            newrow[key] = row[key];
        }
        return newrow;
    };
    Object.defineProperty(data, "changes", { enumerable: false, configurable: true, writable: false, value: () => {
            if (data == null || data.length == 0)
                return null;
            const changes = Array();
            Array.prototype.map.call(data, (item, index) => {
                const changed = changedItems(item, index);
                if (changed)
                    changes.push(changed);
            });
            return changes;
        }
    });
    return [data, dataSet];
};
const updateAt = (index, updateFn, set) => set((l) => {
    const copy = l.slice(0);
    const item = copy[index];
    copy[index] = updateFn(item);
    return copy;
});
const useArray = (initialList) => {
    const [list, set] = useState(initialList);
    return [
        list,
        {
            set,
            empty: () => set([]),
            replace: (list) => set(list),
            push: (item) => set((l) => [...l, item]),
            updateAt: (index, updateFn) => updateAt(index, updateFn, set),
            setAt: (index, value) => set((l) => [...l.slice(0, index), value, ...l.slice(index + 1)]),
            removeAt: (index) => set((l) => [...l.slice(0, index), ...l.slice(index + 1)]),
            filter: (filterFn) => set((l) => l.filter(filterFn)),
            map: (mapFn) => set((l) => [...l].map(mapFn)),
            sort: (sortFn) => set((l) => [...l].sort(sortFn)),
            reverse: () => set((l) => [...l].reverse()),
            mergeBefore: (arr) => set((l) => [...arr].concat([...l])),
            mergeAfter: (arr) => set((l) => [...l].concat([...arr])),
        }
    ];
};

export { useArray, useStateArray, useStateHash };
//# sourceMappingURL=index.es.js.map
