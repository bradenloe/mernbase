export declare const useApollo: () => {
    login: any;
};
export declare const useQuery: (query: any, params: any) => import("@apollo/client").QueryResult<any, import("@apollo/client").OperationVariables> | {
    client: import("@apollo/client").ApolloClient<any>;
    previousData?: any;
    error?: import("@apollo/client").ApolloError | undefined;
    loading: boolean;
    networkStatus: import("@apollo/client").NetworkStatus;
    called: true;
    variables: import("@apollo/client").OperationVariables | undefined;
    startPolling: (pollInterval: number) => void;
    stopPolling: () => void;
    subscribeToMore: <TSubscriptionData = any, TSubscriptionVariables = import("@apollo/client").OperationVariables>(options: import("@apollo/client").SubscribeToMoreOptions<any, TSubscriptionVariables, TSubscriptionData>) => () => void;
    refetch: (variables?: Partial<import("@apollo/client").OperationVariables> | undefined) => Promise<import("@apollo/client").ApolloQueryResult<any>>;
    fetchMore: ((fetchMoreOptions: import("@apollo/client").FetchMoreQueryOptions<import("@apollo/client").OperationVariables, any> & import("@apollo/client").FetchMoreOptions<any, import("@apollo/client").OperationVariables>) => Promise<import("@apollo/client").ApolloQueryResult<any>>) & (<TData2, TVariables2>(fetchMoreOptions: {
        query?: any;
    } & import("@apollo/client").FetchMoreQueryOptions<TVariables2, any> & import("@apollo/client").FetchMoreOptions<TData2, TVariables2>) => Promise<import("@apollo/client").ApolloQueryResult<TData2>>);
    updateQuery: (values: any) => void;
    data: any;
    raw: import("@apollo/client").QueryResult<any, import("@apollo/client").OperationVariables>;
};
export declare const useMutationApollo: (mutation: any, params: any) => import("@apollo/client").MutationTuple<any, import("@apollo/client").OperationVariables, import("@apollo/client").DefaultContext, import("@apollo/client").ApolloCache<any>>;
export declare const useMutation: (mutation: any, params: any) => any[];
export declare const useSubscription: (query: any, params: any) => import("@apollo/client").SubscriptionResult<any, any> | {
    loading: boolean;
    error?: import("@apollo/client").ApolloError | undefined;
    variables?: any;
    data: any;
    raw: import("@apollo/client").SubscriptionResult<any, any>;
};
