'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var React = require('react');
var client = require('@apollo/client');
var utilities = require('@apollo/client/utilities');
var ws = require('@apollo/client/link/ws');

function _interopNamespace(e) {
  if (e && e.__esModule) return e;
  var n = Object.create(null);
  if (e) {
    Object.keys(e).forEach(function (k) {
      if (k !== 'default') {
        var d = Object.getOwnPropertyDescriptor(e, k);
        Object.defineProperty(n, k, d.get ? d : {
          enumerable: true,
          get: function () { return e[k]; }
        });
      }
    });
  }
  n["default"] = e;
  return Object.freeze(n);
}

var React__namespace = /*#__PURE__*/_interopNamespace(React);

const createWebConnection = (server, auth) => {
    const httpLink = new client.HttpLink({
        uri: server.http,
        headers: (!auth) ? undefined : {
            authorization: auth
        }
    });
    const wsLink = new ws.WebSocketLink({
        uri: server.ws,
        options: {
            reconnect: true,
            connectionParams: (!auth) ? undefined : {
                headers: { authorization: auth }
            }
        }
    });
    const splitLink = client.split(({ query }) => {
        const definition = utilities.getMainDefinition(query);
        return (definition.kind === 'OperationDefinition' &&
            definition.operation === 'subscription');
    }, wsLink, httpLink);
    return splitLink;
};
const ApolloAuthContext = React__namespace.createContext(null);
const Apollo = (params) => {
    const { children, server, schema } = params;
    const [client$1, setClient] = React__namespace.useState(null);
    React__namespace.useEffect(() => {
        const client$1 = new client.ApolloClient({
            uri: server.http,
            cache: new client.InMemoryCache()
        });
        setClient(client$1);
    }, []);
    const login = (auth) => {
        const client$1 = new client.ApolloClient({
            cache: new client.InMemoryCache(),
            link: createWebConnection(server, `JWT ${auth}`),
        });
        setClient(client$1);
    };
    const value = {
        schema: schema,
        client: client$1,
        login: login,
    };
    if (client$1 == null)
        return (React__namespace.createElement(React__namespace.Fragment, null));
    return (React__namespace.createElement(client.ApolloProvider, { client: client$1 },
        React__namespace.createElement(ApolloAuthContext.Provider, { value: value }, children)));
};

/*! *****************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

const table = {
    'queries': 'query',
    'mutations': 'mutation',
    'subscriptions': 'subscription'
};
const useApollo = () => {
    const context = React__namespace.useContext(ApolloAuthContext);
    return { login: context.login };
};
const formatQLCall = (schema, functionType, functionName, request, params) => {
    const functionLib = schema[functionType];
    const functionCall = functionLib[functionName];
    const type = table[functionType];
    if (functionCall == null) {
        throw new Error(`${type} ${functionName} not found`);
    }
    let paramsHeader = '';
    let paramsBody = '';
    let variables = {};
    for (let key in functionCall.parameters) {
        if (params[key] != null) {
            const type = functionCall.parameters[key];
            paramsHeader += `$${key} : ${type},`;
            paramsBody += `${key} : $${key},`;
            variables[key] = params[key];
        }
    }
    if (paramsHeader !== '')
        paramsHeader = `(${paramsHeader.slice(0, -1)})`;
    if (paramsBody !== '')
        paramsBody = `(${paramsBody.slice(0, -1)})`;
    const query = `${type} ${functionName} ${paramsHeader} {${functionName} ${paramsBody} { ${request} } }`;
    return [query, variables];
};
const useQuery = (query, params) => {
    const { request: Request, variables: Variables, $ } = params, more = __rest(params, ["request", "variables", "$"]);
    const context = React__namespace.useContext(ApolloAuthContext);
    const schema = context && context.schema;
    const variables = Variables || {};
    const request = (Request) ? Request : ($) ? $ : '_id';
    const [formatedQuery, formatedVariables] = formatQLCall(schema, 'queries', query, request, variables);
    const raw = client.useQuery(client.gql(formatedQuery), Object.assign({ variables: formatedVariables }, more));
    if (raw && raw.data && raw.data[query]) {
        const { updateQuery, data } = raw, more = __rest(raw, ["updateQuery", "data"]);
        const update = (values) => {
            if (values) {
                updateQuery(() => {
                    const result = {};
                    result[query] = values;
                    return result;
                });
            }
        };
        return Object.assign({ updateQuery: update, data: raw.data[query], raw: raw }, more);
    }
    return raw;
};
const useMutation = (mutation, params) => {
    const { request: Request, onCompleted } = params;
    const { client: client$1, schema } = React__namespace.useContext(ApolloAuthContext);
    const request = Request || '_id';
    const [state, setState] = React__namespace.useState({ loading: 0, error: null, data: null });
    const mutate = (params) => __awaiter(void 0, void 0, void 0, function* () {
        const { variables } = params;
        const [formatedQuery, formatedVariables] = formatQLCall(schema, 'mutations', mutation, request, variables);
        setState({
            loading: 1,
            error: null,
            data: null
        });
        const result = yield client$1.mutate({ mutation: client.gql(formatedQuery), variables: formatedVariables });
        const { data } = result;
        if (data && data[mutation]) {
            setState({
                loading: 0,
                error: null,
                data: data[mutation]
            });
            if (onCompleted)
                onCompleted(data[mutation], null);
            return { data: data[mutation], raw: data, error: null };
        }
        if (onCompleted)
            onCompleted(data, null);
        return { data: data, error: null };
    });
    return [mutate, state];
};
const useSubscription = (query, params) => {
    const { request: Request, variables: Variables, $ } = params, more = __rest(params, ["request", "variables", "$"]);
    const context = React__namespace.useContext(ApolloAuthContext);
    const schema = context && context.schema;
    const variables = Variables || {};
    const request = (Request) ? Request : ($) ? $ : '_id';
    const [formatedQuery, formatedVariables] = formatQLCall(schema, 'subscriptions', query, request, variables);
    const raw = client.useSubscription(client.gql(formatedQuery), Object.assign({ variables: formatedVariables }, more));
    if (raw && raw.data && raw.data[query]) {
        const more = __rest(raw
        /*const update = (values :any)=>{
            if (values){
                updateQuery (()=>{
                    const result = {}
                    result[query] = values
                    return result
                })
            }
        }*/
        , ["data"]);
        /*const update = (values :any)=>{
            if (values){
                updateQuery (()=>{
                    const result = {}
                    result[query] = values
                    return result
                })
            }
        }*/
        return Object.assign({ data: raw.data[query], raw: raw }, more);
    }
    return raw;
};

exports.Apollo = Apollo;
exports.ApolloAuthContext = ApolloAuthContext;
exports.useApollo = useApollo;
exports.useMutation = useMutation;
exports.useQuery = useQuery;
exports.useSubscription = useSubscription;
//# sourceMappingURL=index.js.map
