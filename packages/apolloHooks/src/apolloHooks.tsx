import * as React from 'react'

import { gql, useQuery as useQueryApollo, useSubscription as useSubscriptionApollo, useMutation as useMutationAPOLLO} from '@apollo/client';

import {ApolloAuthContext} from './apollo'


const table = {
    'queries' : 'query',
    'mutations' : 'mutation',
    'subscriptions' : 'subscription'
} as any

export const useApollo = ()=>{
    const context = React.useContext (ApolloAuthContext) as any   
    return {login : context.login}
}

const formatQLCall = (schema: any, functionType:string, functionName: string,  request :string, params :any ) => {  
    
    const functionLib = schema[functionType]    
    const functionCall = functionLib[functionName]
    const type = table[functionType]    
    
    if (functionCall == null){  
      throw new Error (`${type} ${functionName} not found`)
    }
  
    let paramsHeader: any = ''
    let paramsBody: any = ''
    let variables: any = {}
  
    for (let key in functionCall.parameters) {
  
      if ( params[key] != null ) {        
        const type = functionCall.parameters[key]
  
        paramsHeader += `$${key} : ${type},`
        paramsBody += `${key} : $${key},`
  
        variables[key] = params[key]
      }
    }
  
    if (paramsHeader !== '')  paramsHeader = `(${paramsHeader.slice(0, -1) })`
    if (paramsBody !== '')    paramsBody = `(${paramsBody.slice(0, -1) })`
  
    const query = `${type} ${functionName} ${paramsHeader} {${functionName} ${paramsBody} { ${request} } }`
    return [query, variables]
}


export const useQuery = (query: any, params:any) => {
    const {request : Request, variables: Variables, $, ...more} = params
    const context = React.useContext (ApolloAuthContext) as any
    const schema = context && context.schema
    const variables = Variables || {}
    
    const request = ( Request ) ? Request : ($) ? $ : '_id'
    const [formatedQuery, formatedVariables] = formatQLCall (schema,'queries', query, request, variables)
    const raw = useQueryApollo (gql(formatedQuery), {variables: formatedVariables, ...more})
    

    if (raw && raw.data && raw.data[query]){
        const {updateQuery, data, ...more} = raw
        
        const update = (values :any)=>{
            if (values){
                updateQuery (()=>{
                    const result = {}
                    result[query] = values
                    return result                    
                })
            }
        }

        return {updateQuery: update, data: raw.data[query], raw: raw, ...more}
    }
    return raw
}



export const useMutationApollo = (mutation: any, params: any)=>{
    const {request : Request, variables, onCompleted, ...more} = params
    const context = React.useContext (ApolloAuthContext) as any
    const schema = context && context.schema
    const request = Request || '_id'
    const [formatedQuery, formatedVariables] = formatQLCall (schema, 'mutations', mutation, request, variables)

    const complete = (data: any, error: any) =>{
        if (!onCompleted)   
            return

        if ( data && data[mutation]) {            
            return onCompleted (data[mutation], error)
        }
        onCompleted(data, error)
    }
    
    const raw = useMutationAPOLLO (gql(formatedQuery), {variables: formatedVariables, onCompleted: complete, ...more} )
    if (raw && raw[1] && raw[1].data && raw[1].data[mutation]){
        raw[1].data = raw[1].data[mutation]        
    }
    return raw
}

export const useMutation = (mutation: any, params: any)=>{
    const {request : Request, onCompleted} = params
    const {client, schema} = React.useContext (ApolloAuthContext) as any
    
    const request = Request || '_id'

    const [state, setState] = React.useState ({loading: 0, error: null, data: null} as any )
    
    const mutate = async (params: any)=>{
        const {variables} = params
        const [formatedQuery, formatedVariables] = formatQLCall (schema, 'mutations', mutation, request, variables)
        setState ({
            loading : 1,
            error : null,
            data : null
        })

        const result = await client.mutate ({mutation: gql(formatedQuery), variables: formatedVariables})
        const {data} = result
        
        if ( data && data[mutation]) {
            setState ({
                loading : 0,
                error : null,
                data :data[mutation]
            })
            if (onCompleted) 
                onCompleted (data[mutation], null)
            return {data: data[mutation], raw: data, error: null}
        }
    
        if (onCompleted) 
                onCompleted (data, null)

        return {data: data, error : null};
    }

    return [mutate, state]
}

export const useSubscription = (query: any, params: any) =>{
    
        const {request : Request, variables: Variables, $, ...more} = params
        const context = React.useContext (ApolloAuthContext) as any
        const schema = context && context.schema
        const variables = Variables || {}
        
        const request = ( Request ) ? Request : ($) ? $ : '_id'
        const [formatedQuery, formatedVariables] = formatQLCall (schema,'subscriptions', query, request, variables)
        const raw = useSubscriptionApollo (gql(formatedQuery), {variables: formatedVariables, ...more})
        
    
        if (raw && raw.data && raw.data[query]){
            const { data, ...more} = raw
            
            /*const update = (values :any)=>{
                if (values){
                    updateQuery (()=>{
                        const result = {}
                        result[query] = values
                        return result                    
                    })
                }
            }*/
    
            return {data: raw.data[query], raw: raw, ...more}
        }
        return raw
    }
    