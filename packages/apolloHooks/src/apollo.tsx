import * as React from 'react';


import {
    ApolloClient,
    InMemoryCache,
    ApolloProvider,    
    split,
    HttpLink
} from "@apollo/client";

import { getMainDefinition } from '@apollo/client/utilities'
import { WebSocketLink } from '@apollo/client/link/ws'



const createWebConnection = (server: any, auth: any) =>{

  const httpLink = new HttpLink({
    uri: server.http,
    headers: (!auth) ? undefined: {
      authorization: auth
    }
  });

  const wsLink = new WebSocketLink({
    uri: server.ws,
    options: {
      reconnect: true,
      connectionParams: (!auth) ? undefined : {
        headers : {authorization: auth}
      }
    }
  });
  
  const splitLink = split(
    ({ query }) => {
      const definition = getMainDefinition(query);
      return (
        definition.kind === 'OperationDefinition' &&
        definition.operation === 'subscription'
      );
    },
    wsLink,
    httpLink,
  );
  
  
  return splitLink
}



export const ApolloAuthContext = React.createContext (null) as any

export const Apollo = (params: any) =>{
    const {children, server, schema} = params    
    const [client, setClient] = React.useState(null) as any
    
    React.useEffect (()=>{
      
      const client = new ApolloClient  ({
            uri: server.http,
            cache: new InMemoryCache()
      })

      setClient(client)      
    },[])

    const login = (auth: any)=>{

      const client = new ApolloClient({  
        cache: new InMemoryCache(),
        link: createWebConnection(server, `JWT ${auth}`),
      })            
      
      setClient(client)
    }

    const value = {
      schema : schema,
      client : client,
      login : login,      
    }

    if (client==null)
      return (<></>)

    return (
        <ApolloProvider client={client}>
            <ApolloAuthContext.Provider value={value}>
              {children}
            </ApolloAuthContext.Provider>
        </ApolloProvider>
    )
}

