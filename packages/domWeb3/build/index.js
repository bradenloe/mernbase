'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var React = require('react');
var Web3Provider = require('web3');
var core = require('@web3-react/core');
var networkConnector = require('@web3-react/network-connector');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

function _interopNamespace(e) {
    if (e && e.__esModule) return e;
    var n = Object.create(null);
    if (e) {
        Object.keys(e).forEach(function (k) {
            if (k !== 'default') {
                var d = Object.getOwnPropertyDescriptor(e, k);
                Object.defineProperty(n, k, d.get ? d : {
                    enumerable: true,
                    get: function () { return e[k]; }
                });
            }
        });
    }
    n["default"] = e;
    return Object.freeze(n);
}

var React__namespace = /*#__PURE__*/_interopNamespace(React);
var Web3Provider__default = /*#__PURE__*/_interopDefaultLegacy(Web3Provider);

/*! *****************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};

var Init = function (params) {
    var networks = params.networks;
    var activate = core.useWeb3React().activate;
    React__namespace.useEffect(function () {
        var network = new networkConnector.NetworkConnector({
            urls: networks
        });
        activate(network);
    }, []);
    return (React__namespace.createElement(React__namespace.Fragment, null, params.children));
};
var Web3 = function (params) {
    var getLibrary = function (provider) {
        return new Web3Provider__default["default"](provider);
    };
    return (React__namespace.createElement(core.Web3ReactProvider, { getLibrary: getLibrary },
        React__namespace.createElement(Init, __assign({}, params))));
};

Object.defineProperty(exports, 'useWeb3React', {
    enumerable: true,
    get: function () { return core.useWeb3React; }
});
exports.Web3 = Web3;
//# sourceMappingURL=index.js.map
