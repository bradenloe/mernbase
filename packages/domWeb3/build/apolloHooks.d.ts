export declare const useQuery: (query: any, params: any) => import("@apollo/client").QueryResult<any, import("@apollo/client").OperationVariables> | {
    client: import("@apollo/client").ApolloClient<any>;
    previousData?: any;
    error?: import("@apollo/client").ApolloError | undefined;
    loading: boolean;
    networkStatus: import("@apollo/client").NetworkStatus;
    called: true;
    variables: import("@apollo/client").OperationVariables | undefined;
    startPolling: (pollInterval: number) => void;
    stopPolling: () => void;
    subscribeToMore: <TSubscriptionData = any, TSubscriptionVariables = import("@apollo/client").OperationVariables>(options: import("@apollo/client").SubscribeToMoreOptions<any, TSubscriptionVariables, TSubscriptionData>) => () => void;
    updateQuery: <TVars = import("@apollo/client").OperationVariables>(mapFn: (previousQueryResult: any, options: Pick<import("@apollo/client").WatchQueryOptions<TVars, any>, "variables">) => any) => void;
    refetch: (variables?: Partial<import("@apollo/client").OperationVariables> | undefined) => Promise<import("@apollo/client").ApolloQueryResult<any>>;
    fetchMore: ((fetchMoreOptions: import("@apollo/client").FetchMoreQueryOptions<import("@apollo/client").OperationVariables, any> & import("@apollo/client").FetchMoreOptions<any, import("@apollo/client").OperationVariables>) => Promise<import("@apollo/client").ApolloQueryResult<any>>) & (<TData2, TVariables2>(fetchMoreOptions: {
        query?: import("@apollo/client").DocumentNode | import("@graphql-typed-document-node/core").TypedDocumentNode<any, import("@apollo/client").OperationVariables> | undefined;
    } & import("@apollo/client").FetchMoreQueryOptions<TVariables2, any> & import("@apollo/client").FetchMoreOptions<TData2, TVariables2>) => Promise<import("@apollo/client").ApolloQueryResult<TData2>>);
    data: any;
    raw: import("@apollo/client").QueryResult<any, import("@apollo/client").OperationVariables>;
};
export declare const useMutation: (mutation: any, params: any) => import("@apollo/client").MutationTuple<any, import("@apollo/client").OperationVariables, import("@apollo/client").DefaultContext, import("@apollo/client").ApolloCache<any>>;
