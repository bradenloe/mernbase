import * as React from 'react';
import Web3Provider from 'web3';
import { Web3ReactProvider, useWeb3React } from '@web3-react/core';
export { useWeb3React } from '@web3-react/core';
import { NetworkConnector } from '@web3-react/network-connector';

/*! *****************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};

var Init = function (params) {
    var networks = params.networks;
    var activate = useWeb3React().activate;
    React.useEffect(function () {
        var network = new NetworkConnector({
            urls: networks
        });
        activate(network);
    }, []);
    return (React.createElement(React.Fragment, null, params.children));
};
var Web3 = function (params) {
    var getLibrary = function (provider) {
        return new Web3Provider(provider);
    };
    return (React.createElement(Web3ReactProvider, { getLibrary: getLibrary },
        React.createElement(Init, __assign({}, params))));
};

export { Web3 };
//# sourceMappingURL=index.es.js.map
