import * as React from 'react';
import Web3Provider from "web3";
import { useWeb3React, Web3ReactProvider } from '@web3-react/core'
import { NetworkConnector } from "@web3-react/network-connector"

export { useWeb3React } from '@web3-react/core'


const Init = (params: any)=>{
  const { networks } = params
  const { activate } = useWeb3React()

  React.useEffect (()=>{
    const network = new NetworkConnector({
        urls: networks
      });
    activate (network)
  },[])

  return (<>{params.children}</>)
}

export const Web3 = (params: any) => {
  
  const getLibrary = (provider :any)=> {
    return new Web3Provider(provider) 
  }

  return (
    <Web3ReactProvider getLibrary={getLibrary}>      
      <Init {...params}/>
    </Web3ReactProvider>
  )
}