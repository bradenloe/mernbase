export declare const withKeys: (array: [], hash: any) => any[];
export declare const withoutKeys: (array: any, keys: any) => any;
