'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var React = require('react');

function _interopNamespace(e) {
    if (e && e.__esModule) return e;
    var n = Object.create(null);
    if (e) {
        Object.keys(e).forEach(function (k) {
            if (k !== 'default') {
                var d = Object.getOwnPropertyDescriptor(e, k);
                Object.defineProperty(n, k, d.get ? d : {
                    enumerable: true,
                    get: function () { return e[k]; }
                });
            }
        });
    }
    n["default"] = e;
    return Object.freeze(n);
}

var React__namespace = /*#__PURE__*/_interopNamespace(React);

var hashmap = function (hash, callback) {
    var ret = [];
    for (var i in hash) {
        ret.push(callback({ key: i, value: hash[i] }));
    }
    return ret;
};

var useControlState = function (initvalue) {
    var _a = React__namespace.useState(initvalue), value = _a[0], valueset = _a[1];
    var set = function (e) {
        return (valueset((e && e.target && e.target.value != null) ? e.target.value : e));
    };
    return [value, set];
};

/*! *****************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};

var withKeys = function (array, hash) {
    if (array == null || hash == null)
        return array;
    return array.map(function (item) {
        return __assign(__assign({}, item), hash);
    });
};
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
var withoutKeys = function (array, keys) {
    if (array == null || keys == null || keys.length == 0)
        return array;
    var without = function (item) {
        var create = {};
        for (var key in item) {
            if (!keys.includes(key))
                create[key] = item[key];
        }
        return create;
    };
    return array.map(function (item) {
        return without(item);
    });
};

exports.hashmap = hashmap;
exports.useControlState = useControlState;
exports.withKeys = withKeys;
exports.withoutKeys = withoutKeys;
//# sourceMappingURL=index.js.map
