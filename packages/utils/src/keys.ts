export const withKeys = (array: [], hash: any) => {
	if (array == null || hash == null) return array

	return array.map((item: any) => {
		return {...item, ...hash}
	})
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

export const withoutKeys = (array: any, keys: any) => {
	if (array == null || keys == null || keys.length == 0) return array

	const without = (item: any) => {
		const create = {} as any
		for (let key in item) {
			if (!keys.includes(key)) create[key] = item[key]
		}
		return create
	}

	return array.map((item: any) => {
		return without(item)
	})
}
