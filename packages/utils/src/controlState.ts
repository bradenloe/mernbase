import * as React from 'react';

export const useControlState = (initvalue: any)=>{
    const [value, valueset] =  React.useState(initvalue)
  
    const set=(e: any)=>{
        return ( valueset ( (e && e.target && e.target.value !=null) ? e.target.value : e ) )
    }
    return [value, set]
}